<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Type extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    public function all(){
    	return $this->db->get('types')->result();
    }

    public function find($type_id){
        $query = 'SELECT * FROM types WHERE id = ?';
        $type = $this->db->query($query, $type_id);        
        return $type->row();
    }

    public function edit($params){
        $this->db->where('id', $params['type_id']);
        unset($params['type_id']);
        $type = $this->db->update('types', $params);
        return $type;
    }

    public function create($type){
        $type = $this->db->insert('types', $type);        
        return $type;
    }

    public function delete($type_id){
        $type = $this->db->delete('types', array('id' => $type_id)); 
        return $type;
    }
}
/* End of file model_name.php */
/* Location: ./application/models/model_name.php */