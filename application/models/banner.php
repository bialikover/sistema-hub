<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banner extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function all(){
        $query = "SELECT banners.*, profiles.first_name, 
                         profiles.last_name, users.name as username
                  FROM banners
                  JOIN profiles ON profiles.id = banners.profile_id                  
                  JOIN users  ON profiles.uid = users.uid";
    	$banners = $this->db->query($query);
    	return $banners->result();
    }

    public function active(){
        $query = "SELECT banners.*
                  FROM banners 
                  WHERE banners.active = 1";
        $banners = $this->db->query($query);
        return $banners->result();        
    }

    public function find($banner_id){
        $query = 'SELECT * FROM banners WHERE id = ?';
        $banner = $this->db->query($query, $banner_id);        
        return $banner->row();
    }

    public function edit($params){

        $this->db->where('id', $params['banner_id']);        
        unset($params['banner_id']);
        if(isset($params['active'])){
            $params['active'] = TRUE;
        }
        else{
            $params['active'] = FALSE;   
        }
        $banner = $this->db->update('banners', $params);
        return $banner;
    }

    public function create($banner){        
        $banner = $this->db->insert('banners', $banner);        
        return $banner;
    }

    public function delete($banner_id){
        $banner = $this->db->delete('banners', array('id' => $banner_id)); 
        return $banner;
    }

    public function search($criteria){        
        $this->db->like('name', $criteria);
        $this->db->or_like('link', $criteria);
        $this->db->or_like('image_url', $criteria);
        $banners = $this->db->get("banners");
        return $banners->result();
    }

}
/* End of file banner.php */
/* Location: ./application/models/banner.php */