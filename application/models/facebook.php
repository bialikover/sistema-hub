<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name:  Simple Facebook Codeigniter API for TelmexHub
 *
 * Author: Adán Galván
 *         bialikoer@gmail.com
 *         @bialikover
 * Created:  03.11.2013
 *
 * Description:  An easy way to use Facebook to get info and likes
 *
 * Requirements: PHP5 or above
 *
 */
class Facebook extends CI_Model {

    public $appid;
    public $apisecret;

    public function __construct()
    {
        parent::__construct();
        // replace these with Application ID and Application Secret.
        $this->appid = '311397385570136';
        $this->apisecret = 'af0798ea531efcf3c2e06126b71af6e0';
        $this->load->model('user');
    }

    /**
     * Get tokens from FB then exchanges them for the User login tokens
     */
    public function getfbInfo($user_id)
    {
            $user = $this->user->find($user_id);
            // now you can save the token to a database, and use it to access the user's graph
            // for example, this will return all their basic info.  check the FB Dev docs for more.
            $infourl = "https://graph.facebook.com/me?access_token=$user->access_token";            
            $return = $this->__getpage($infourl);                       
            if (!$return)
                die('Error getting info');            
            
            //get likes
            $likesurl = "https://graph.facebook.com/me/likes?access_token=$user->access_token";
            $likesuser = $this->__getpage($likesurl); 
            if (!$likesurl)
                die('Error getting likes');
            $info['basic_info'] = json_decode($return);            
            $info['likes'] = $this->parse_likes($likesuser); 
            return $info;                    
    }


    private function parse_likes($likesuser){
        $likes = json_decode($likesuser)->data;         
        $user_likes = "";
        foreach($likes as $like){
            $user_likes .= $like->name.", ";
        }
        return htmlentities($user_likes, ENT_QUOTES);
    }

    /**
     * CURL method to interface with FB API
     * @param string $url
     * @return json 
     */
    private function __getpage($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // check if it returns 200, or else return false
        if ($http_code === 200)
        {
            curl_close($ch);
            return $return;
        }
        else
        {
            // store the error. I may want to return this instead of FALSE later
            $error = curl_error($ch);
            curl_close($ch);
            return FALSE;
        }
    }

}