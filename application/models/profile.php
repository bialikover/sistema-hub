<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    /**
     * undocumented function
     * @params array of params ( user_id, first_name, middle_name, last_name, gender, likes );
     * @return void
     * @author 
     **/
    public function create_by_oauth($params)
    {
    	$query = "INSERT INTO profiles (uid, first_name, middle_name, last_name, gender, likes) VALUES (?, ?, ?, ?, ?, ?)";
    	$this->db->query($query, $params);
    	$profile = $this->db->insert_id();
    	return $profile;
    }
    public function create($profile){
        $profile['birthday'] = strtotime(date($profile['birthday']));
        if($profile['thumbnail']){
            $profile['picture_url'] = $profile['thumbnail'];
         }
        unset($profile['thumbnail']);
        $event = $this->db->insert('profiles', $profile);        
        return $event;
    }
    public function find_by_user_id($user_id){
        $query = "SELECT profiles.*, users.name, users.mail FROM profiles 
                  JOIN users ON users.uid = profiles.uid 
                  WHERE users.uid = ?";
        $profile = $this->db->query($query, $user_id)->row();        
        return $profile;
    }

    
    public function all(){
        $query = "SELECT * FROM profiles";
        $profiles = $this->db->query($query);
        return $profiles->result();
    }

    public function edit($params){        
        $this->db->where('id', $params['profile_id']);
        unset($params['profile_id']);
        $params['birthday'] = strtotime(date($params['birthday']));
        $profile = $this->db->update('profiles', $params);
        return $profile;
    }


    public function prepare_all(){
        $profiles_raw = $this->all();
        $profiles = array();
        foreach($profiles_raw as $profile){
            $profiles[] = $profile->first_name." ".$profile->last_name."-".$profile->id;
        }
        return $profiles;

    }
}
/* End of file profile.php */
/* Location: ./application/models/profile.php */