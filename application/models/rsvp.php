<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rsvp extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('profile');
        $this->load->model('user');
    }

    public function find_by_event($event_id){
    	$query = "SELECT rsvps.* , profiles.picture_url as 'image_url' , users.name, users.uid
                  FROM rsvps JOIN profiles
                  ON profiles.id = rsvps.profile_id JOIN users 
                  ON users.uid = profiles.uid 
                  WHERE rsvps.event_id = ? 
                  AND rsvps.status = TRUE ";
    	$rsvps = $this->db->query($query, $event_id)->result();
    	return $rsvps;
    }
    public function save($profile_id, $event_id){
        $rsvp = $this->find_by_profile_and_event($profile_id, $event_id);
        if(!empty($rsvp)){
            $status = $rsvp->status;
            $rsvp_updated = $this->update($rsvp, $status);
            return $rsvp_updated;
        }
        else{
            $rsvp = $this->create($profile_id, $event_id);
            return $rsvp;
        }
    }

    public function find_by_profile_and_event($profile_id, $event_id){
        $query = "SELECT * FROM rsvps WHERE profile_id = ? AND  event_id = ?";
        $rsvp = $this->db->query($query, array($profile_id, $event_id))->row();
        return $rsvp;   
    }

    public function create($profile_id, $event_id){
        $rsvp_data = array( "event_id"        => $event_id,
                            "profile_id"      => $profile_id,
                            "status"          => TRUE, 
                            "registered_time" => time());

        $rsvp = $this->db->insert('rsvps', $rsvp_data);        
        if($rsvp){
            $rsvp = $this->find($this->db->insert_id()); 
            return $rsvp;
        }
        else{
            return $rsvp;
        }
    }

    public function find($rsvp_id){
        $query = "SELECT * from rsvps WHERE id = ?";
        $rsvp = $this->db->query($query, $rsvp_id)->row();
        return $rsvp;   
    }

    public function update($rsvp, $status){
        $params['status'] = !$status;
        $this->db->where('id', $rsvp->id);
        $this->db->update('rsvps', $params);
        $rsvp = $this->find($rsvp->id); 
        return $rsvp;
    }

    /**
     * 
     * Find if the current user is attending to an event
     * @return boolean
     * @author @bialikover
     **/
    
    public function is_attending($user_id, $event_id){
        $profile = $this->profile->find_by_user_id($user_id);
        if($profile){
            $query = "SELECT * from rsvps WHERE profile_id = ? AND event_id = ? AND status = 1";
            return $this->db->query($query, array($profile->id, $event_id))->row();
        }
        else{
            return false;
        }
    }


}
/* End of file rsvp.php */
/* Location: ./application/models/rsvp.php */