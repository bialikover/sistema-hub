<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();     
        $this->load->library('ion_auth');
    }


    public function find_by_email_or_username($email, $username){
    	$query = "SELECT * FROM users WHERE users.mail = ? OR users.name = ?";
    	$user = $this->db->query($query, array($email, $username))->row();
    	return $user;
    }


    /**
     * undocumented function
     * @params array of params ( username, email, fbid, access_token created_on );
     * @return user id
     * @author 
     **/
    
    public function create_with_access_token($params){
    	$query = "INSERT INTO users (name, mail, fbid, access_token, created_on) VALUES (?, ?, ?, ?, ?)";
    	$this->db->query($query, $params);
    	$user = $this->db->insert_id();
    	return $user;
    }


    public function all()
    {   
        $users_object = $this->db->get('users', 100, 0)->result();        
        foreach ($users_object as $user) {
            $user->roles = $this->_get_user_roles($user->uid);
        }
        return $users_object;
    }

    private function _get_user_roles($user_id){
        $query = "SELECT * FROM role
                  JOIN users_roles ON users_roles.rid = role.rid
                  WHERE users_roles.uid = ?";
        $roles = $this->db->query($query, $user_id)->result();
        return $roles;
    }


    private function _set_or_delete_roles($user_id, $groups){
        $roles = $this->_get_user_roles($user_id);
        $roles_it_has = array();
        $roles_to_set = array();
        foreach($roles as $role){            
            $roles_it_has[] = $role->rid;          
        }
        foreach($groups as $group){             
            
            if(!in_array($group, $roles_it_has)){
                $roles_to_set[] = $group; 
            }
        }
        $roles_to_delete = array_diff($roles_it_has, $groups);
        $this->_set_roles($user_id, $roles_to_set);
        $this->_delete_roles($user_id, $roles_to_delete);
    }

    private function _set_roles($user_id, $roles_to_set){
        if(!empty($roles_to_set)){
            foreach($roles_to_set as $role_id){
                $user_role = array(
                            "uid" => $user_id,
                            "rid" => $role_id
                    );
                $this->db->insert('users_roles', $user_role);        
            }
        }
    }

    private function _delete_roles($user_id, $roles_to_delete){
        if(!empty($roles_to_delete)){
            $query = "DELETE FROM users_roles WHERE uid = ? AND rid = ?";
            foreach($roles_to_delete as $role_id){
                $user_role = array(
                            "uid" => $user_id,
                            "rid" => $role_id
                    );
                //echo var_dump($user_role);
                $this->db->query($query, $user_role);
            }
        }
    }


    public function find($user_id){
        $query = 'SELECT users.*  FROM users WHERE users.uid = ?';
        $user = $this->db->query($query, $user_id);        
        $user_object = $user->row();
        $user_object->roles = $this->_get_user_roles($user_object->uid);
        return $user_object;
    }

    public function edit($params){
        if(!isset($params['active'])){
            $params['active'] = 0;
        }        
        $this->_set_or_delete_roles($params['user_id'], $params['group']);        
        $user_id = $params['user_id'];
        unset($params['user_id']);
        unset($params['group']);
        $user = $this->ion_auth->update($user_id, $params);
        return $user;
    }

    public function edit_password($params){
        $data['password'] = $params['password'];
        unset($params['user_id']);
        $user_id = $this->session->userdata('user_id');        
        $user = $this->ion_auth->update($user_id, $data);
        return $user;
    }


    public function create($user){
        $user = $this->db->insert('users', $user);        
        return $user;
    }

    public function delete($user_id){
        $user = $this->db->delete('users', array('uid' => $user_id)); 
        return $user;
    }


    public function search($criteria){        
        $query = "SELECT * FROM users WHERE name LIKE '%".$criteria."%' OR mail LIKE '%".$criteria."%'";
        //$users_object = $this->db->get('users')->result(); 
        $users_object = $this->db->query($query)->result();
        foreach ($users_object as $user) {
            $user->roles = $this->_get_user_roles($user->uid);
        }
        return $users_object;
    }
}
/* End of file user.php */
/* Location: ./application/models/user.php */