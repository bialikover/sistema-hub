<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Role extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    public function all(){
    	return $this->db->get('role')->result();
    }

    public function find($role_id){
        $query = 'SELECT * FROM role WHERE rid = ?';
        $role = $this->db->query($query, $role_id);        
        return $role->row();
    }

    public function edit($params){
        $this->db->where('rid', $params['role_id']);
        unset($params['role_id']);
        $role = $this->db->update('role', $params);
        return $role;
    }

    public function create($role){
        $role = $this->db->insert('role', $role);        
        return $role;
    }

    public function delete($role_id){
        $role = $this->db->delete('role', array('rid' => $role_id)); 
        return $role;
    }
}
/* End of file model_name.php */
/* Location: ./application/models/model_name.php */