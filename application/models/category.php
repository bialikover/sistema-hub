<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function all(){
    	$categories = $this->db->get('categories')->result();
    	return $categories;
    }

    public function find($category_id){
        $query = 'SELECT * FROM categories WHERE id = ?';
        $category = $this->db->query($query, $category_id);        
        return $category->row();
    }

    public function edit($params){
        $this->db->where('id', $params['category_id']);
        unset($params['category_id']);
        $category = $this->db->update('categories', $params);
        return $category;
    }

    public function create($category){
        $category = $this->db->insert('categories', $category);        
        return $category;
    }

    public function delete($category_id){
        $category = $this->db->delete('categories', array('id' => $category_id)); 
        return $category;
    }

    public function search($criteria){        
        $this->db->like('name', $criteria);
        $this->db->or_like('description', $criteria);        
        $categories = $this->db->get("categories");
        return $categories->result();
    }
}
/* End of file category.php */
/* Location: ./application/models/category.php */