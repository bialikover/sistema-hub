<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function all()
    {   
        $query = 'SELECT posts.*, categories.name AS "category", profiles.first_name, 
                         profiles.last_name, users.name  AS "username"
                  FROM posts
                  JOIN categories ON categories.id = posts.category_id
                  JOIN profiles   ON profiles.id = posts.profile_id
                  JOIN users  ON profiles.uid = users.uid';
        $posts = $this->db->query($query);
    	return array_reverse($posts->result());

    }

    public function find($post_id){
        $query = 'SELECT posts.* , categories.name AS "category", 
                  profiles.first_name, profiles.last_name, profiles.id AS "profile_id"
                  FROM posts 
                  JOIN profiles ON profiles.id = posts.profile_id
                  JOIN categories ON categories.id = posts.category_id 
                  WHERE posts.id =?';
        $query2 = 'SELECT tags.name AS "tag_name" FROM tags
                   JOIN posts_tags
                   ON posts_tags.tag_id = tags.id
                   WHERE posts_tags.post_id = ?';
        $tags = $this->db->query($query2, $post_id);
        $post = $this->db->query($query, $post_id);        
        $post_object = $post->row();
        $post_object->profile = $post_object->first_name." ".$post_object->last_name."-".$post_object->profile_id;
        $post_object->tags = $tags->result_array();
        return $post_object;
    }


    public function find_by_slug($slug){
        $query = 'SELECT posts.* , categories.name AS "category", 
                  profiles.first_name, profiles.last_name, profiles.id AS "profile_id"
                  FROM posts 
                  JOIN profiles ON profiles.id = posts.profile_id
                  JOIN categories ON categories.id = posts.category_id 
                  WHERE posts.slug =?';

        $post = $this->db->query($query, $slug);
        if($post){
            $query2 = 'SELECT tags.name AS "tag_name" FROM tags
                   JOIN posts_tags
                   ON posts_tags.tag_id = tags.id
                   WHERE posts_tags.post_id = ?';
            $tags = $this->db->query($query2, $post->row()->id);               
            $post_object = $post->row();
            $post_object->profile = $post_object->first_name." ".$post_object->last_name."-".$post_object->profile_id;
            $post_object->tags = $tags->result_array();
            return $post_object;
        }
        else{
            return false;
        }

    }

    public function edit($params){
        $this->db->where('id', $params['post_id']);
        unset($params['post_id']);
        $post['published_on'] = strtotime(date($post['published_on']));
        $post = $this->db->update('posts', $params);
        return $post;
    }

    public function create($post){

        $tag_ids = $this->get_tag_ids($post['tags']);
        unset($post['tags']);
        unset($post['profile']);
        $post['published_on'] = strtotime(date($post['published_on']));
        $post = $this->db->insert('posts', $post);
        $post_id = $this->db->insert_id();
        if(!empty($tag_ids)){
            $this->insert_tags($post_id, $tag_ids);
        }
        return $post;
    }

    public function delete($post_id){
        $post = $this->db->delete('posts', array('id' => $post_id)); 
        return $post;
    }

    public function search($criteria){        
        $query = 'SELECT posts.* , categories.name AS "category", 
                  profiles.first_name, profiles.last_name, profiles.id AS "profile_id", users.name AS "username"
                  FROM posts 
                  JOIN profiles ON profiles.id = posts.profile_id
                  JOIN categories ON categories.id = posts.category_id
                  JOIN users  ON profiles.uid = users.uid                   
                  WHERE posts.name LIKE "%'.$criteria.'%" OR posts.description LIKE "%'.$criteria.'%"';
        $posts = $this->db->query($query);
        return $posts->result();
    }


    public function insert_tags($post_id, $tag_ids){

        foreach($tag_ids as $tag_id){
            $post_tag = array('post_id' => $post_id, 'tag_id' => $tag_id);
            $this->db->insert('posts_tags', $post_tag);
        }
    
    }



    public function get_tag_ids($tags){

        $raw_tags = explode(",", $tags);
        $tag_ids = array();
        foreach($raw_tags as $tag){
            $tag_ids[] = $this->find_or_create_tag($tag);
        }
        return $tag_ids;
    }

    public function find_or_create_tag($tag){
        $tag_downcase = strtolower($tag);
        $query = 'SELECT * from tags WHERE name = ?';
        $tag = $this->db->query($query, $tag_downcase)->row();
        if(!empty($tag)){
            return $tag->id;
        }
        else{
            $tag_data = array('name' => $tag_downcase);   
            $this->db->insert('tags', $tag_data);        
            $tag = $this->db->insert_id();
            return $tag;
        }
    }

}
/* End of file posts.php */
/* Location: ./application/models/posts.php */