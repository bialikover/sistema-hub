<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->model('rsvp');
    }

    public function all()
    {   
        $query = 'SELECT events.* , types.name AS "type" FROM events JOIN types ON types.id = events.type_id';
        $events = $this->db->query($query);
    	return array_reverse($events->result());

    }

    public function find($event_id){
        $query = 'SELECT events.* , types.name AS "type" FROM events JOIN types ON types.id = events.type_id WHERE events.id = ?';
        $event = $this->db->query($query, $event_id);        
        return $event->row();
    }

    public function find_by_day($time){
        $date = date('Y-m-d H:i:s', $time);
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $day = substr($date, 8, 2);    
        $begin = mktime(0, 0, 0, $month, $day, $year);
        $end = mktime(0, 0, 0, $month, $day + 1, $year);

        $query = 'SELECT events.* , types.name 
                            AS "type" 
                            FROM events JOIN types 
                            ON types.id = events.type_id
                            WHERE events.date_start
                            BETWEEN ? AND ? ORDER BY events.date_start ';

        $events = $this->db->query($query, array($begin, $end));
        return $this->with_rsvp($events->result());
    }


    public function with_rsvp($events){
        foreach($events as $event){
            $event->rsvps = $this->rsvp->find_by_event($event->id);
        }
        return $events;
    }

    public function find_by_year_and_month($year, $month){
        $first = mktime(0, 0, 0, $month, 1, $year);
        $last = mktime(0, 0, 0, $month+1, 0, $year);
        $query = 'SELECT events.* , types.name 
                            AS "type" 
                            FROM events JOIN types 
                            ON types.id = events.type_id
                            WHERE events.date_start
                            BETWEEN ? AND ? ';

        $events = $this->db->query($query, array($first, $last));
        return $this->_prepare_events_to_calendar($events->result());
    }

    public function find_this_month($time){

        $date = date('Y-m-d H:i:s', $time);
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);    
        $first = mktime(0, 0, 0, $month, 1, $year);
        $last = mktime(0, 0, 0, $month+1, 0, $year);
        $query = "SELECT events.* , types.name 
                            AS 'type' 
                            FROM events JOIN types 
                            ON types.id = events.type_id
                            WHERE events.date_start
                            BETWEEN ? AND ? ";

        $events = $this->db->query($query, array($first, $last));
        return $this->_prepare_events_to_calendar($events->result());
    }

    private function _prepare_events_to_calendar($events){
       
        $array = array();
        foreach($events as $event){
            $date = date('Y-m-d H:i:s', $event->date_start);
            $day = substr($date, 8, 2);
            $day = (int)$day;
            $array[$day] = $this->_encode_event_html($event);
        }

        //var_dump($array);
        //die();
        return $array;
    }

    private function _encode_event_html($event){

        $html = 
        '
            <div id="'.$event->date_start.'">
                <img src="'.base_url("assets/events/".$event->image_url).'" width="146" height="150">
            </div>
        ';
        return $html;

    }

    public function edit($params){
        $this->db->where('id', $params['event_id']);
        unset($params['event_id']);
        $params['date_start'] = strtotime(date($params['date_start']));
        $params['date_end'] = strtotime(date($params['date_end']));
        $event = $this->db->update('events', $params);
        return $event;
    }

    public function create($event){
        $event['date_start'] = strtotime(date($event['date_start']));
        $event['date_end'] = strtotime(date($event['date_end']));
        $event = $this->db->insert('events', $event);        
        return $event;
    }

    public function delete($event_id){
        $event = $this->db->delete('events', array('id' => $event_id)); 
        return $event;
    }
    public function search($criteria){        
        $query = 'SELECT events.* , types.name AS "type" 
                  FROM events JOIN types ON types.id = events.type_id 
                  WHERE events.name LIKE "%'.$criteria.'%" OR events.description LIKE "%'.$criteria.'%" OR events.coach LIKE "%'.$criteria.'%"';
        $events = $this->db->query($query);
        return $events->result();
    }

}
/* End of file events.php */
/* Location: ./application/models/events.php */