<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class tag extends CI_Model {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function all(){
    	$tags = $this->db->get('tags')->result();
    	return $tags;
    }

    public function all_for_post(){
        $tags_object = $this->db->get('tags')->result();
        $tags = array();
        foreach($tags_object as $tag){
            $tags[] = $tag->name;
        }
        return $tags;
    }

    public function find($tag_id){
        $query = 'SELECT * FROM tags WHERE id = ?';
        $tag = $this->db->query($query, $tag_id);        
        return $tag->row();
    }

    public function edit($params){
        $this->db->where('id', $params['tag_id']);
        unset($params['tag_id']);
        $params['name'] = strtolower($params['name']);
        $tag = $this->db->update('tags', $params);
        return $tag;
    }

    public function create($tag){
        $tag['name'] = strtolower($tag['name']);
        $tag = $this->db->insert('tags', $tag);        
        return $tag;
    }

    public function delete($tag_id){
        $tag = $this->db->delete('tags', array('id' => $tag_id)); 
        return $tag;
    }
    public function search($criteria){        
        $this->db->like('name', $criteria);
        $tags = $this->db->get("tags");
        return $tags->result();
    }
}
/* End of file tag.php */
/* Location: ./application/models/tag.php */