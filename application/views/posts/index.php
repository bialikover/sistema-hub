<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>Posts del Blog</h1>
<?php echo form_open('posts/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar posts">
<?php echo form_close();?>
<?php echo anchor("posts/create", "Crear un post", array('class' => 'btn btn-primary pull-right'));?>
<?php if($posts):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>
      <th>Publicado por: </th>
      <th>Imagen Destacada</th>
      <th>Nombre</th>
      <th>Fecha de publicación</th>      
      <th>Categoria</th>      
      <th>¿Esta Publicado?</th>
      <th>url</th>            
    </tr>
  </thead>
  <tbody>
    <?php foreach($posts as $post) :?>
      <tr>
        <td><?php echo $post->id;?></td>
        <td><?php echo $post->first_name." ".$post->last_name." - nombre de usuario: - ".$post->username;?></td>
        <td><img src="<?php echo base_url('assets/posts/'.$post->image_url)?>" class="img-rounded" width="50" height="50"/></td>
        <td><?php echo $post->name;?></td>      
		    <td><?php echo $post->published_on;?></td>
		    <td><?php echo $post->category;?></td>
		    <td><?php echo $post->published?"si":"no";?></td>		
		    <td><?php echo $post->slug;?></td>        
        <td>          
          <?php echo anchor("posts/edit/".$post->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("posts/destroy/".$post->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>