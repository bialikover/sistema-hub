<h1>Nuevo post</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('posts/save');?>


    

<div class="row-fluid">	
	<div class="span4">
		<div class="control-group">  	
			<label class="control-label">Nombre del post:</label>
			
    		<div class="controls">
        		<input type="text" name="name" value="<?php echo set_value('name'); ?>" size="70" />
    		</div>
  		</div>
        <div class="control-group">
            <label class="control-label">Mostrar con fecha de publicación del dia</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo set_value('published_on'); ?>" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" id="dtp_input1" value="<?php echo set_value('published_on'); ?>" name ="published_on"/><br/>
        </div>  
        
  	</div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Publicado Por:</label>            
            <div class="controls">
                <input type="text" id="profiles" name="profile" value="<?php echo set_value('profile'); ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Publicar ahora mismo:</label>            
            <div class="controls">
                <input type="checkbox" name="published" value="1">  Si deseo publicarla ahora mismo<br>
            </div>
        </div>
    </div>
    <div class="span8">
    <div class="control-group">     
            <label class="control-label">Extracto del post( brief de lo que se trata..):</label>            
        <div class="controls">
                <textarea class="span8" name="excerpt" value="<?php echo set_value('excerpt'); ?>" size="70" /></textarea>
        </div>  
    </div>    
    </div>
</div>    

<div class="row-fluid"> 
    <div class="span8">
        <div class="control-group">
            <label class="control-label">Descripción del post:</label>
            <textarea  name="description" value="" class="textarea span12" rows="15" id="description" placeholder="Enter text ..."><?php echo set_value('description'); ?></textarea>            
        </div>
    </div>
    <div class="span4">
      
    </br>
    </br>
    
        <div class="control-group ">
            <label class="control-label">Imagen Destacada:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="http://www.placehold.it/300x200/EFEFEF/AAAAAA&text=no+image+300x200" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">categoria de post:</label>
            
            <div class="controls">            
            <select name="category_id">
                <?php if (isset($categories)):?>
                <?php foreach ($categories as $category):?>
                <option value="<?php echo $category->id;?>"><?php echo $category->name;?> </option>                
                <?php endforeach;?>
                <?php endif;?>
            </select>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Etiquetas:</label>
            
            <div class="controls">
                <input type="text" id="singleFieldTags" name="tags" value="<?php echo set_value('tags'); ?>" size="70" />
            </div>
        </div>
    </div>    
</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Crear posto');?>
    <?php echo anchor('posts/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">	
$(document).ready(function() {
    var sampleTags = <?php echo json_encode($tags);?>;
    var profiles = <?php echo json_encode($profiles);?>;
    $('.form_datetime').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
    });

    CKEDITOR.replace( 'description',{
        toolbar: "post",
        height: '400px',  
        extraPlugins: 'autogrow', removePlugins: 'resize'
        // Remove the Resize plugin as it does not make sense to use it in conjunction with the AutoGrow plugin.        
    });
    $('#singleFieldTags').tagit({
                availableTags: sampleTags,
                // This will make Tag-it submit a single form value, as a comma-delimited field.
                singleField: true,
                singleFieldNode: $('#mySingleField')
            });
    $('#profiles').tagit({
                availableTags: profiles,
                // This will make Tag-it submit a single form value, as a comma-delimited field.
                singleField: true,
                singleFieldNode: $('#mySingleField')
            });    
    


    
});
</script>