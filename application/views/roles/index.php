<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>rol de usuarios</h1>
<?php echo anchor("roles/create", "Crear un rol de usuario", array('class' => 'btn btn-primary pull-right'));?>
<?php if($roles):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>rid</th>      
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($roles as $role) :?>
      <tr>
        <td><?php echo $role->rid;?></td>        
        <td><?php echo $role->name;?></td>      
    		<td><?php echo $role->description;?></td>
        <td>          
          <?php echo anchor("roles/edit/".$role->rid, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("roles/destroy/".$role->rid, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>