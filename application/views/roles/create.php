<h1>Nuevo rol de usuario</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('roles/save');?>

<div class="row-fluid">	
	<div class="span4">
		<div class="control-group">  	
			<label class="control-label">Nombre del rol de usuario:</label>
			
    		<div class="controls">
        		<input type="text" name="name" value="<?php echo set_value('name'); ?>" size="70" />
    		</div>
  		</div>
  	</div>
</div>    

<div class="row-fluid"> 
    <div class="span8">
        <div class="control-group">
            <label class="control-label">Descripción del rol de usuario:</label>
            <textarea  name="description" value="" class="textarea span12" rows="12" id="description" placeholder="Enter text ..."><?php echo set_value('description'); ?></textarea>            
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Crear rol de usuario');?>
    <?php echo anchor('roles/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>