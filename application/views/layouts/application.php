<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width">
	<title><?php echo ( isset($pagetitle) ) ? pagetitle($pagetitle) : pagetitle(); ?></title>  
	<!--Inician Facebook OG Headers=-->
	<?php if (isset($product)): ?>
	<meta property="og:title" content="<?php echo $product->name;?>" />
	<meta property="og:description" content="<?php echo $product->description;?>" />
	<meta property="og:url" content="<?php echo base_url().$product->name_url; ?>" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:site_name" content="Nouian" />
	<meta property="og:image" content="<?php echo base_url()."images/product/370x370".$product->images[0]->image_url; ?>" />
	<?php elseif (isset($shop)): ?>
	<meta property="og:title" content="<?php echo $shop->name;?>" />
	<meta property="og:description" content="<?php echo $shop->description;?>" />
	<meta property="og:url" content="<?php echo base_url().$shop->name_url; ?>" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:site_name" content="Nouian" />
	<?php endif; ?>
	<!--Terminan Facebook OG Headers=-->
	<link href="<?php echo base_url('assets/css/bootstrap-front.min.css') ?>" rel="stylesheet">
  	<link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
   	<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/telmexhub.css') ?>" rel="stylesheet">
   	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
   	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
	<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/thub.js') ?>"></script>
</head>
<body>
   <!-- FB SDK w/ XFBML -->
   <div id="fb-root"></div>
   <script>(function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1&appId=226437797487663";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));</script>
   
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
<header id="menu">
<nav id="menu_head">
  <ul class="top">
    <a href="<?php echo base_url('index.php/')?>"><li class="logo">Logo</li></a>
    <a href="<?php echo base_url('index.php/calendario')?>"><li>calendario</li></a>
    <a href="<?php echo base_url('index.php/blog')?>"><li>blog</li></a>
    <a href="<?php echo base_url('index.php/tv')?>"><li>tv</li></a>
    <a href="<?php echo base_url('index.php/ubicacion')?>"><li>ubicación</li></a>
    <a href="<?php echo base_url('index.php/acerca')?>"><li>acerca</li></a>
  </ul>
  <ul class="bottom">
    <li class="social">
     <!-- <ul>
        <li>
          <a href="http://twitter.com/telmexhub">
            <div class="twitter">twitter</div>
          </a>
        </li>
        <li>
          <a href="http://facebook.com/telmexhub">
            <div class="facebook">facebook</div>
          </a>
        </li>
        <li>
          <a href="http://plus.google.com/telmexhub">
           <div class="google"> google+</div>
          </a>
        </li>
        <li>          
          <a href="http://www.linkedin.com/company/inttelmex-it-telmexhub">
            <div class="linkedin">linkedin</div>
          </a>
        </li>
      </ul>-->
    </li>
    <li class="add_event"><a href="<?php echo base_url('index.php/eventos/proponer');?>"><div class="add-icon"></div>proponer un evento</a></li>
    <li class="search"><input type="search" placeholder="¿Qué estas buscando?"/></li>
    <?php if(current_user()):?>      
      <li class="login dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="<?php echo user_image()?> " width="24px" height="24px" alt="<?php echo current_user();?>" title="<?php echo current_user();?>"/>
          <?php echo current_user()?>
          <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">                                    
                  <li><a href="#">Modificar perfil</a></li>
                  <li><a href= "<?php echo base_url(); ?>index.php/auth/logout">Cerrar sesión</a></li>                  
        </ul>
      </li>          
    <?php else:?>
      <li class="login"><a href="<?php echo base_url('index.php/auth/login');?>">Inicia Sesión</a></li>    
    <?php endif;?>
  </ul>
</nav>
</header>
  <div role="main">
     <div id="content">
        <?php echo $yield;?>
     </div>
  </div>
  <footer>
     	<div id="footer">
			<div id="menufooter">
			<ul>						
						<li><a href="<?php echo base_url();?>index.php/faq/ayuda">Aviso de privacidad</a></li>
						<li><a href="<?php echo base_url();?>index.php/faq/privacidad">Aviso de privacidad</a></li>
            <li><a href="<?php echo base_url();?>index.php/faq/reglamento-interno">Reglamento Interno</a></li>
						<li><a href="<?php echo base_url();?>index.php/faq/reglamento-comunidades">Reglamento para comunidades</a></li>
            <li><a href="<?php echo base_url();?>index.php/faq">Preguntas Frecuentes</a></li>
            <li><a href="<?php echo base_url();?>index.php/comunidades">Comunidades en Telmex Hub</a></li>
            <li><a href="<?php echo base_url();?>index.php/Alianzas">Alianzas del Grupo</a></li>
			</ul>
			</div><span id="spanfoot" >TelmexHub © 2013</span>
			
			</div> <!--end footer=-->
  </footer>
  <script>
    var _gaq=[['_setAccount',''],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
  
</body>
</html>
