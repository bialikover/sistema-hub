<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width">
  <title><?php echo ( isset($pagetitle) ) ? pagetitle($pagetitle) : pagetitle(); ?></title>  
  <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/datetimepicker.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/jquery.tagit.css');?>" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url('assets/css/tagit.ui-zendesk.css');?>" rel="stylesheet" type="text/css">

    
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
  <script src="<?php echo base_url('assets/js/locales/bootstrap-datetimepicker.es.js')?>"></script>
  <script src="<?php echo base_url('assets/ckeditor/ckeditor.js')?>"></script>  
  <script src="<?php echo base_url('assets/js/tag-it.min.js')?>" type="text/javascript" charset="utf-8"></script>
  
  <style type="text/css">

     

      /* Custom page CSS
      -------------------------------------------------- */
      /* Not required for template or sticky footer method. */

      .container {
        width: auto;
        max-width: 960px;

      }
      .container .credit {
        margin: 20px 0;
      }

      #content{
        margin-top: 60px;

      }

    </style>

</head>

<body>   
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <header>       
  <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">TelmexHub Panel</a>
          <div class="nav-collapse collapse">
            <ul class="nav">              
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Usuarios
                    <b class="caret"></b>
                  </a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor("/users", "Gestión de usuarios")?></li>
                  <li><?php echo anchor("/roles", "Roles y permisos")?></li>
                </ul>
              </li>              
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Eventos
                    <b class="caret"></b>
                  </a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor("/events", "Gestión de eventos")?></li>
                  <li><?php echo anchor("/types", "Tipos de evento")?></li>
                </ul>
              </li>
              <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    Blog
                    <b class="caret"></b>
                  </a>
                <ul class="dropdown-menu">
                  <li><?php echo anchor("/posts", "Publicaciones")?></li>
                  <li><?php echo anchor("/categories", "Categorias del blog")?></li>
                  <li><?php echo anchor("/tags", "Etiquetas del blog")?></li>
                </ul>
              </li>
              
              <li><?php echo anchor("/banners", "Banners")?></li>              
            </ul>
              <?php //if ($this->ion_auth->logged_in()):?>
            <ul class="nav pull-right">  
              <li id="fat-menu" class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <?php echo current_user()?>
                  <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li class="divider"></li>
                  <li class="nav-header">Mi cuenta</li>
                  <li><a href="#">Modificar perfil</a></li>
                  <li><a href= "<?php echo base_url(); ?>index.php/auth/logout">Cerrar sesión</a></li>
                  
                </ul>
              </li>
            </ul>
              <?php //else:?>
              <!--<form class="navbar-form pull-right" action="<?php echo base_url();?>login/process" method="post" >
                <input class="span2" type="text" placeholder="Email">
                <input class="span2" type="password" placeholder="Password">
                <button type="submit" class="btn">Sign in</button>
              </form>-->
              <?php //endif;?>
            
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
  </header>
  <div role="main">
     <div id="content" class="container">
        <?php echo $yield;?>         
     </div>
  </div>
  <footer>
  <div id="footer">
      <div class="container">
        <p class="muted credit">Telmex Hub © 2013.</p>
      </div>
  </div>
  </footer>

  <script>
    var _gaq=[['_setAccount',''],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>  

  
  
</body>
</html>
