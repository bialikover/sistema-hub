<h1>Editar banner</h1><hr>

<?php if ($this->session->flashdata('error')):?>
         <div class="alert alert-notice">           
             <h4>Muy mal!</h4>
           <?php echo $this->session->flashdata('error');?>
         </div>
<?php endif; ?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('banners/update');?>


<input type="hidden" name="banner_id" value="<?php echo $banner->id; ?>"/> 

<div class="row-fluid">	
    <div class="span4">
	   <div class="control-group">     
            <label class="control-label">Nombre del banner(aparecerá sobre el banner):</label>
            
            <div class="controls">
                <input type="text" name="name" value="<?php echo $banner->name; ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Descripción del banner,(aparecerá sobre el banner debe tener menos de 120 caracteres):</label>
            
            <div class="controls">
                <textarea name="description" value="" size="70" /><?php echo $banner->description; ?></textarea>
            </div>
        </div>
    </div>    
  	<div class="span4">
  	    <div class="control-group">     
            <label class="control-label">Link del banner:</label>
            
            <div class="controls">
                <input type="text" name="link" value="<?php echo $banner->link; ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Publicar ahora mismo:</label>            
            <div class="controls">
                <input type="checkbox" name="active" <?php echo $banner->active?'checked':'';?>>  Si deseo publicarlo ahora mismo<br>
            </div>
        </div>
    </div>
</div>    

<div class="row-fluid"> 
    <div class="span4">
    </br>
    </br>
    
        <div class="control-group ">
            <label class="control-label">Imagen del bannero:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo base_url("assets/banners/".$banner->image_url);?>" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-widthtth: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>
    </div>

</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Editar banner');?>
    <?php echo anchor('banners/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>