<h1>Nuevo banner</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('banners/save');?>


    

<div class="row-fluid">	
	<div class="span4">
		<div class="control-group">  	
			<label class="control-label">Nombre del banner(aparecerá sobre el banner):</label>
			
    		<div class="controls">
        		<input type="text" name="name" value="<?php echo set_value('name'); ?>" size="70" />
    		</div>
  		</div>
  	</div>
        <div class="span4">
        <div class="control-group">     
            <label class="control-label">Descripción del banner,(aparecerá sobre el banner debe tener menos de 120 caracteres):</label>
            
            <div class="controls">
                <textarea name="description" value="" size="70" /><?php echo set_value('description'); ?></textarea>
            </div>
        </div>
    </div>
  	<div class="span4">
        <div class="control-group">     
            <label class="control-label">Link del banner:</label>
            
            <div class="controls">
                <input type="text" name="link" value="<?php echo set_value('link'); ?>" size="100" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Publicar ahora mismo:</label>            
            <div class="controls">
                <input type="checkbox" name="active" value="1">  Si deseo publicarlo ahora mismo<br>
            </div>
        </div>
    </div>
</div>    

<div class="row-fluid"> 
    <div class="span4">
    </br>
    </br>
    
        <div class="control-group ">
            <label class="control-label">Imagen del bannero:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="http://www.placehold.it/300x200/EFEFEF/AAAAAA&text=no+image+300x200" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>
    </div>

</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Crear bannero');?>
    <?php echo anchor('banners/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>