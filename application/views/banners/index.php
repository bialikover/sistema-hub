<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>banners</h1>
<div class="row-fluid"> 

<?php echo form_open('banners/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar baner">
<?php echo form_close();?>
<?php echo anchor("banners/create", "Crear un banner", array('class' => 'btn btn-primary pull-right'));?>
</div>
<div class="row-fluid"> 
  <div class="span 12">
<?php if($banners):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>
      <th>Imagen</th>
      <th>subido por</th>
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Link</th>
      <th>esta Activo?</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($banners as $banner) :?>
      <tr>
        <td><?php echo $banner->id;?></td>
        <td><img src="<?php echo base_url('assets/banners/'.$banner->image_url)?>" class="img-rounded" width="50" height="50"/></td>
        <td><?php echo $banner->first_name." ".$banner->first_name." - usuario - ".$banner->username;?></td>
        <td><?php echo $banner->name;?></td>      
        <td><?php echo $banner->description;?></td>      
		    <td><?php echo $banner->link;?></td>		    
		    <td><?php echo $banner->active? "si":"no";?></td>		
        <td>
          <?php echo anchor("banners/edit/".$banner->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("banners/destroy/".$banner->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>
</div>
</div>