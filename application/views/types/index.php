<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>tipo de eventos</h1>
<?php echo anchor("types/create", "Crear un tipo de evento", array('class' => 'btn btn-primary pull-right'));?>
<?php if($types):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>      
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($types as $type) :?>
      <tr>
        <td><?php echo $type->id;?></td>        
        <td><?php echo $type->name;?></td>      
    		<td><?php echo $type->description;?></td>
        <td>          
          <?php echo anchor("types/edit/".$type->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("types/destroy/".$type->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>