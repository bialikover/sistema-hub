<h1>Edita tu perfil</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('profiles/update');?>


<div class="row-fluid">	
	<div class="span4">        
		<div class="control-group">  	
			<label class="control-label">Nombre:</label>
			
    		<div class="controls">
        		<input type="text" name="first_name" value="<?php echo $profile->first_name; ?>" size="70" />
    		</div>
  		</div>       
        <div class="control-group">     
            <label class="control-label">Apellido Paterno:</label>
            
            <div class="controls">
                <input type="text" name="last_name" value="<?php echo $profile->last_name; ?>" size="70" />
            </div>
        </div>
         <div class="control-group">     
            <label class="control-label">Apellido Materno:</label>
            
            <div class="controls">
                <input type="text" name="middle_name" value="<?php echo $profile->middle_name; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Fecha de Nacimiento</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo $profile->birthday; ?>" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" id="dtp_input1" value="<?php echo $profile->birthday; ?>" name ="birthday"/><br/>
        </div>
        <div class="control-group">     
            <label class="control-label">Genero:</label>
            
            <div class="controls">            
            <select name="gender">                
                <option value="female">Femenino</option>
                <option value="male">Masculino</option>                
            </select>
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Nivel de estudios:</label>            
            <div class="controls">
                <input type="text" name="education" value="<?php echo $profile->education; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Mi Especialidad es:</label>            
            <div class="controls">
                <input type="text" name="skill" value="<?php echo $profile->skill; ?>" size="70" />
            </div>
        </div>
  	</div>
  	<div class="span4">
        <div class="control-group">     
            <label class="control-label">Teléfono:</label>            
            <div class="controls">
                <input type="text" name="telephone" value="<?php echo $profile->telephone; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Dirección:</label>            
            <div class="controls">
                <input type="text" name="address" value="<?php echo $profile->address; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Ciudad:</label>            
            <div class="controls">
                <input type="text" name="city" value="<?php echo $profile->city; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Pais:</label>            
            <div class="controls">
                <input type="text" name="country" value="<?php echo $profile->country; ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Intereses:</label>            
            <div class="controls">
                <textarea name="interested_in" value="" ><?php echo $profile->interested_in; ?></textarea>
            </div>
        </div>                          		        
    </div> 
    <div class="span4">
            <div class="control-group ">
            <label class="control-label">Mi foto:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo base_url("assets/users/pics/$profile->uid/".$profile->picture_url);?>" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>   
    </div>
</div>    

<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Completar perfil');?>    
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">	
$(document).ready(function() {
    $('.form_datetime').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
    });
});
</script>