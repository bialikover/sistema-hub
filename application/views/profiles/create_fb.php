<!--<section id="complete-profile">
    <?php echo validation_errors(); ?>
    <?php echo form_open_multipart('profiles/save');?>
    <header id="">
        <h2>Completa tus datos</h2>
        <div id="submit" class="span6">
            <?php echo form_submit('submit', 'Crear cuenta');?>
        </div>        
    </header> 
    <div class="fixed">
    <?php if (isset($error)):?>
    <div class="alert alert-notice">                   
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
    <?php endif;?>
</div>
    <div id="picture-section">
        <div class="span4">
            <div class="control-group ">
            <label class="control-label">Mi foto:</label>
            <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo set_value('thumbnail');?>" /></div>
            <input type="hidden" name="thumbnail" id="thumbnail" value="<?php echo set_value('thumbnail'); ;?>"/>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
            </div>
            </div>   
        </div>
    </div>
    <div id="form">
            <div class="span4">        
        <div class="control-group">     
            <div class="controls">
                <input type="text" name="first_name" placeholder="Nombre" value="<?php echo set_value('first_name'); ?>" size="70" />
            </div>
        </div>       
        <div class="control-group">     
            <div class="controls">
                <input type="text" name="last_name" placeholder="Apellido paterno" value="<?php echo set_value('last_name'); ?>" size="70" />
            </div>
        </div>
         <div class="control-group">                             
            <div class="controls">
                <input type="text" name="middle_name" placeholder="Apellido materno" value="<?php echo set_value('middle_name'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Fecha de Nacimiento</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo set_value('birthday'); ?>" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" id="dtp_input1" value="<?php echo set_value('birthday'); ?>" name ="birthday"/><br/>
        </div>
        <div class="control-group">     
                        
            <div class="controls">            
            <select name="gender">                
                <option value="">Genero</option>
                <option value="female">Femenino</option>
                <option value="male">Masculino</option>                
            </select>
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="education"  placeholder="Nivel de estudios" value="<?php echo set_value('education'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="skill"  placeholder="Mi especialidad" value="<?php echo set_value('skill'); ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="telephone" placeholder="Teléfono" value="<?php echo set_value('telephone'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="address" placeholder="Dirección" value="<?php echo set_value('address'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="city" placeholder="Ciudad" value="<?php echo set_value('city'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <input type="text" name="country"  placeholder="Pais" value="<?php echo set_value('country'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">                 
            <div class="controls">
                <textarea name="interested_in" value="" placeholder="Intereses" ><?php echo set_value('interested_in'); ?></textarea>
            </div>
        </div> 
    </div>
    

</section>-->

<div class="fixed">
    <?php if (isset($error)):?>
    <div class="alert alert-notice">                   
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
    <?php endif;?>
</div>
<div class="row-fluid">
<h1>Completa tu perfil</h1><hr>

<?php echo validation_errors(); ?>
<?php echo form_open_multipart('profiles/save');?>

<?php if (isset($fbInfo)):?>
    <input type="hidden" name="likes" id="likes" value="<?php echo $fbInfo['likes'];?>" />
    <div class="row-fluid"> 
    <div class="span4">        
        <div class="control-group">     
            <label class="control-label">Nombre:</label>
            
            <div class="controls">
                <input type="text" name="first_name" value="<?php echo $fbInfo['basic_info']->first_name; ?>" size="70" />
            </div>
        </div>       
        <div class="control-group">     
            <label class="control-label">Apellido Paterno:</label>
            
            <div class="controls">
                <input type="text" name="last_name" value="<?php echo $fbInfo['basic_info']->last_name; ?>" size="70" />
            </div>
        </div>
         <div class="control-group">     
            <label class="control-label">Apellido Materno:</label>
            
            <div class="controls">
                <input type="text" name="middle_name" value="<?php echo set_value('middle_name'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Fecha de Nacimiento</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo set_value('birthday'); ?>" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" id="dtp_input1" value="<?php echo set_value('birthday'); ?>" name ="birthday"/><br/>
        </div>
        <div class="control-group">     
            <label class="control-label">Genero:</label>
            
            <div class="controls">            
            <select name="gender">                
                <option value="female">Femenino</option>
                <option value="male">Masculino</option>                
            </select>
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Nivel de estudios:</label>            
            <div class="controls">
                <input type="text" name="education" value="<?php echo set_value('education'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Mi Especialidad es:</label>            
            <div class="controls">
                <input type="text" name="skill" value="<?php echo set_value('skill'); ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Teléfono:</label>            
            <div class="controls">
                <input type="text" name="telephone" value="<?php echo set_value('telephone'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Dirección:</label>            
            <div class="controls">
                <input type="text" name="address" value="<?php echo set_value('address'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Ciudad:</label>            
            <div class="controls">
                <input type="text" name="city" value="<?php echo set_value('city'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Pais:</label>            
            <div class="controls">
                <input type="text" name="country" value="<?php echo set_value('country'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Intereses:</label>            
            <div class="controls">
                <textarea name="interested_in" value="" ><?php echo set_value('interested_in'); ?></textarea>
            </div>
        </div>                                          
    </div> 
    <div class="span4">
            <div class="control-group ">
            <label class="control-label">Mi foto:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo 'https://graph.facebook.com/'.$fbInfo['basic_info']->id.'/picture?type=large';?>" /></div>
            <input type="hidden" name="thumbnail" id="thumbnail" value="<?php echo 'https://graph.facebook.com/'.$fbInfo['basic_info']->id.'/picture?type=large';?>"/>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>   
    </div>
</div>    

<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Completar perfil');?>    
    </div>
</div>
<?php echo form_close();?>


<?php else:?>


<input type="hidden" name="likes" id="likes" value="<?php echo set_value('likes');?>" />
<div class="row-fluid">	
	<div class="span4">        
		<div class="control-group">  	
			<label class="control-label">Nombre:</label>
			
    		<div class="controls">
        		<input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" size="70" />
    		</div>
  		</div>       
        <div class="control-group">     
            <label class="control-label">Apellido Paterno:</label>
            
            <div class="controls">
                <input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" size="70" />
            </div>
        </div>
         <div class="control-group">     
            <label class="control-label">Apellido Materno:</label>
            
            <div class="controls">
                <input type="text" name="middle_name" value="<?php echo set_value('middle_name'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Fecha de Nacimiento</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo set_value('birthday'); ?>" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <input type="hidden" id="dtp_input1" value="<?php echo set_value('birthday'); ?>" name ="birthday"/><br/>
        </div>
        <div class="control-group">     
            <label class="control-label">Genero:</label>
            
            <div class="controls">            
            <select name="gender">                
                <option value="female">Femenino</option>
                <option value="male">Masculino</option>                
            </select>
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Nivel de estudios:</label>            
            <div class="controls">
                <input type="text" name="education" value="<?php echo set_value('education'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Mi Especialidad es:</label>            
            <div class="controls">
                <input type="text" name="skill" value="<?php echo set_value('skill'); ?>" size="70" />
            </div>
        </div>
  	</div>
  	<div class="span4">
        <div class="control-group">     
            <label class="control-label">Teléfono:</label>            
            <div class="controls">
                <input type="text" name="telephone" value="<?php echo set_value('telephone'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Dirección:</label>            
            <div class="controls">
                <input type="text" name="address" value="<?php echo set_value('address'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Ciudad:</label>            
            <div class="controls">
                <input type="text" name="city" value="<?php echo set_value('city'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Pais:</label>            
            <div class="controls">
                <input type="text" name="country" value="<?php echo set_value('country'); ?>" size="70" />
            </div>
        </div>
        <div class="control-group">     
            <label class="control-label">Intereses:</label>            
            <div class="controls">
                <textarea name="interested_in" value="" ><?php echo set_value('interested_in'); ?></textarea>
            </div>
        </div>                          		        
    </div> 
    <div class="span4">
            <div class="control-group ">
            <label class="control-label">Mi foto:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo set_value('thumbnail');?>" /></div>
            <input type="hidden" name="thumbnail" id="thumbnail" value="<?php echo set_value('thumbnail'); ;?>"/>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>   
    </div>
</div>    

<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Completar perfil');?>    
    </div>
</div>
<?php echo form_close();?>

<?php endif;?>
</div>
</div>
</div>
<script type="text/javascript">	
$(document).ready(function() {
    $('.form_datetime').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
    });
});
</script>