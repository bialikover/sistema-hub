

<section id="profile-content">
    <header id="profile">
        <h1><?php echo $profile->name?></h1>      
    </header>    
    <section id="profilecard" class="<?php echo $profile->gender=='male'?'male':'female';?>">
        <div id ="image-holder">
            <img src="<?php echo $profile->picture_url."?width=200&height=200";?>" alt="<?php echo $profile->name?>" title="<?php echo $profile->name?>"/>
        </div>
        <div class="card">
            <h2><?php echo $profile->name?></h2>
            <h3><?php echo $profile->first_name." ".$profile->last_name;?></h3>
            <h4><?php echo $profile->skill;?></h4>
        </div>
        <div class="clear"></div>
        <div class="stream">
            <h4>Intereses:</h4>
            <p><?php echo $profile->interested_in;?></p>
            <h4> Actividad Reciente: </h4>
        </div>
    </section>
    <aside class="<?php echo $profile->gender=='male'?'male':'female';?>">
            <h4>Cosas que le gustan a <?php echo $profile->gender=="male"?"este dude":"esta dudette";?>:</h4>
            <p><?php echo $profile->likes;?></p>
    </aside>
    <div class="clear"></div>
        
</section>
