<h1>Editar categoria</h1><hr>

<?php if ($this->session->flashdata('error')):?>
         <div class="alert alert-notice">           
             <h4>Muy mal!</h4>
           <?php echo $this->session->flashdata('error');?>
         </div>
<?php endif; ?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('categories/update');?>


<input type="hidden" name="category_id" value="<?php echo $category->id; ?>"/> 

<div class="row-fluid"> 
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Nombre de la categoria:</label>
            
            <div class="controls">
                <input type="text" name="name" value="<?php echo $category->name; ?>" size="70" />
            </div>
        </div>
    </div>
</div>    

<div class="row-fluid"> 
    <div class="span8">
        <div class="control-group">
            <label class="control-label">Descripción de la categoria:</label>
            <textarea  name="description" value="" class="textarea span12" rows="12" id="description" placeholder="Enter text ..."><?php echo $category->description; ?></textarea>            
        </div>
    </div>
</div>


<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Editar categoria');?>
    <?php echo anchor('categories/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>