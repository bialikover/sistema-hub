<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>Categorias</h1>
<?php echo form_open('categories/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar categoria">
<?php echo form_close();?>

<?php echo anchor("categories/create", "Crear una categoria", array('class' => 'btn btn-primary pull-right'));?>
<?php if($categories):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>      
      <th>Nombre</th>
      <th>Descripción</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($categories as $category) :?>
      <tr>
        <td><?php echo $category->id;?></td>        
        <td><?php echo $category->name;?></td>      
    		<td><?php echo $category->description;?></td>
        <td>          
          <?php echo anchor("categories/edit/".$category->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("categories/destroy/".$category->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>