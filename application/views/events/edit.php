<h1>Editar Evento</h1><hr>

<?php if ($this->session->flashdata('error')):?>
         <div class="alert alert-notice">           
             <h4>Muy mal!</h4>
           <?php echo $this->session->flashdata('error');?>
         </div>
<?php endif; ?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('events/update');?>


<input type="hidden" name="event_id" value="<?php echo $event->id; ?>"/> 

<div class="row-fluid">	
	<div class="span4">
		<div class="control-group">  	
			<label class="control-label">Nombre del evento:</label>
			
    		<div class="controls">
        		<input type="text" name="name" value="<?php echo $event->name; ?>" size="70" />
    		</div>
  		</div>
  	</div>
  	<div class="span4">
  		<div class="control-group">
            <label class="control-label">Fecha de Inicio</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text" value="<?php echo $event->date_start; ?>" readonly>
        	    <span class="add-on"><i class="icon-remove"></i></span>
				<span class="add-on"><i class="icon-th"></i></span>
            </div>
			<input type="hidden" id="dtp_input1" value="<?php echo $event->date_start; ?>" name ="date_start"/><br/>
        </div>
    </div>
    <div class="span4">
  		<div class="control-group">
            <label class="control-label">Fecha de Termino</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input2">
                <input size="16" type="text" value="<?php echo $event->date_end; ?>" readonly>
        	    <span class="add-on"><i class="icon-remove"></i></span>
				<span class="add-on"><i class="icon-th"></i></span>
            </div>
			<input type="hidden" id="dtp_input2" value="<?php echo $event->date_end; ?>" name="date_end"/><br/>
        </div>
    </div>
</div>    

<div class="row-fluid"> 
    <div class="span8">
        <div class="control-group">
            <label class="control-label">Descripción del evento:</label>
            <textarea  name="description" value="" class="textarea span12" rows="12" id="description"><?php echo htmlentities($event->description); ?></textarea>            
        </div>
    </div>
    <div class="span4">
    </br>
    </br>
    
        <div class="control-group ">
            <label class="control-label">Banner del evento:</label>
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new thumbnail" style="width: 300px; height: 200px;"><img src="<?php echo base_url("assets/events/".$event->image_url);?>" /></div>
            <div class="fileupload-preview fileupload-exists thumbnail" style="max-widthtth: 300px; max-height: 200px; line-height: 20px;"></div>
            <div>
            <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
            <input type="file" name="userfile"/>
            </span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Tipo de evento:</label>
            
            <div class="controls">            
            <select name="type_id">
                <?php if (isset($types)):?>
                <?php foreach ($types as $type):?>
                <option value="<?php echo $type->id;?>"><?php echo $type->name;?> </option>                
                <?php endforeach;?>
                <?php endif;?>
            </select>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Instructor:</label>
            
            <div class="controls">
                <input type="text" name="coach" value="<?php echo $event->coach; ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Sala de trabajo:</label>
            
            <div class="controls">
                <select name="workspace">
                <option value="1">Sala de trabajo 1</option>
                <option value="2">Sala de trabajo 2</option>
                <option value="3">Sala de trabajo 3</option>
                <option value="4">Sala de trabajo 4</option>
                <option value="5">Sala de trabajo 5</option>
                <option value="6">Sala de trabajo 6</option>                
                </select>
            </div>
        </div>
    </div>                    
</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Editar Evento');?>
    <?php echo anchor('events/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>

<script type="text/javascript">	

$(document).ready(function() {
    
    $('.form_datetime').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
    });

    CKEDITOR.replace( 'description',{
        toolbar: "event",
        height: '400px',  
        extraPlugins: 'autogrow', removePlugins: 'resize'
        // Remove the Resize plugin as it does not make sense to use it in conjunction with the AutoGrow plugin.        
    });    


    
});
</script>