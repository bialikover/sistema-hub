<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>Eventos</h1>
<?php echo form_open('events/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar evento">
<?php echo form_close();?>
<?php echo anchor("events/create", "Crear un evento", array('class' => 'btn btn-primary pull-right'));?>
<?php if($events):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>
      <th>Banner</th>
      <th>Nombre</th>
      <th>Fecha de inicio</th>
      <th>Fecha de termino</th>
      <th>Tipo de evento</th>      
      <th>Imparte</th>
      <th>Sala de trabajo</th>      
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($events as $event) :?>
      <tr>
        <td><?php echo $event->id;?></td>
        <td><img src="<?php echo base_url('assets/events/'.$event->image_url)?>" class="img-rounded" width="50" height="50"/></td>
        <td><?php echo $event->name;?></td>      
		<td><?php echo $event->date_start;?></td>
		<td><?php echo $event->date_end;?></td>
		<td><?php echo $event->type;?></td>
		
		<td><?php echo $event->coach;?></td>
        <td><?php echo $event->workspace;?></td>               
        <td>
          <?php echo anchor("events/rsvps/".$event->id, "RSVP", array('class' => 'btn btn-mini btn-info'));?>
          <?php echo anchor("events/edit/".$event->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("events/destroy/".$event->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>