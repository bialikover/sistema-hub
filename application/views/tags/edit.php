<h1>Editar etiqueta</h1><hr>

<?php if ($this->session->flashdata('error')):?>
         <div class="alert alert-notice">           
             <h4>Muy mal!</h4>
           <?php echo $this->session->flashdata('error');?>
         </div>
<?php endif; ?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('tags/update');?>


<input type="hidden" name="tag_id" value="<?php echo $tag->id; ?>"/> 

<div class="row-fluid"> 
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Nombre de la etiqueta:</label>
            
            <div class="controls">
                <input type="text" name="name" value="<?php echo $tag->name; ?>" size="70" />
            </div>
        </div>
    </div>
</div>    

<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Editar etiqueta');?>
    <?php echo anchor('tags/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>