<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>etiquetas</h1>
<?php echo form_open('tags/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar etiquetas">
<?php echo form_close();?>
<?php echo anchor("tags/create", "Crear una etiqueta", array('class' => 'btn btn-primary pull-right'));?>
<?php if($tags):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>id</th>      
      <th>Nombre</th>      
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($tags as $tag) :?>
      <tr>
        <td><?php echo $tag->id;?></td>        
        <td><?php echo $tag->name;?></td>          		
        <td>          
          <?php echo anchor("tags/edit/".$tag->id, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("tags/destroy/".$tag->id, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>