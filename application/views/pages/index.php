<section id ="index">
	<article id ="brief-index" class ="brief">
		<h1>TelmexHub es el epicentro de inovación técnologica</h1>
		<h2>Co-working, Emprendimiento y Creatividad.</h2>
		<?php echo $this->session->flashdata('message');?>		
		<div id="register">
		 <a href="<?php echo base_url('index.php/auth/login') ?>"><p>Regístrate</p></a>
		</div>
	</article>
</section>
<section>
	<div id="myCarousel" class="carousel slide">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <!-- Carousel items -->
  <div class="carousel-inner">
    <?php $first= TRUE;?>
    <?php foreach ($banners as $banner ):?>
    <?php if($first):?>
      <div class="active item">
      <?php $first = FALSE;?>
    <?php else:?>
      <div class="item">      
    <?php endif;?>
      <a href="<?php echo $banner->link;?>">
    	 <img src ="<?php echo base_url('assets/banners/'.$banner->image_url);?>"/>
      </a>      
    	<div class="carousel-caption">
        	<h4><?php echo !empty($banner->name)? $banner->name: "";?></h4>
            <p><?php echo !empty($banner->description)? $banner->description: "";?></p>
        </div>
    </div>
    <?php endforeach;?>    
  </div>
  <!-- Carousel nav -->
  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
	</div>
</section>
<script>
$('.carousel').carousel({
  interval: 5000
})
</script>