
<section id ="terms">
	<article class="brief">
		<h1 style="color:#000;text-align:center">Reglamento de comunidades</h1>		
		<p class="rtejustify">
	<style type="text/css">
<!--
		@page { margin: 2cm }
		P { margin-bottom: 0cm }
		P.western { so-language: es-ES; font-weight: bold }
		P.cjk { font-weight: bold }
		P.ctl { font-weight: bold }
	-->	</style>
</p>
<p class="rtecenter" lang="es-ES">&nbsp;</p>
<p align="CENTER"><br />
	<font color="#000000"><font face="Arial, sans-serif"><font size="2"><b>TELMEX-HUB</b></font></font></font></p>
<p align="CENTER"><font color="#000000"><font face="Arial, sans-serif"><font size="2"><b>REGLAMENTO INTERNO</b></font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El presente documento contiene las bases conforme las cuales INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), a trav&eacute;s de su programa Telmex Hub, proporcionar&aacute; al p&uacute;blico en general que acceda a las instalaciones, los servicios que a continuaci&oacute;n se describen.</font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol>
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>DEFINICIONES.</b></font>&nbsp;<font size="2" style="font-size: 9pt">Para los efectos del presente Reglamento se entiende por:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Programa &ldquo;Telmex Hub&rdquo;: es un proyecto que proporciona a sus usuarios, de forma gratuita, infraestructura tecnol&oacute;gica de &uacute;ltima generaci&oacute;n con el objetivo de fomentar la colaboraci&oacute;n, la innovaci&oacute;n, as&iacute; como la creaci&oacute;n y desarrollo de toda clase de proyectos.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Consolas de Videojuegos&rdquo;: significa el equipo electr&oacute;nico de entretenimiento, propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), y que a trav&eacute;s de su programa Telmex Hub, concede su uso temporal dentro de las instalaciones, a los usuarios registrados.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Equipo de C&oacute;mputo&rdquo;: Significa; i) el equipo de c&oacute;mputo port&aacute;til (Laptop y/o Netbook), ii) el equipo de computo de escritorio, y/o iii) cualquier otro aparato electr&oacute;nico, cableado y material did&aacute;ctico propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), que a trav&eacute;s de su programa Telmex Hub, conceda su uso temporal a los Usuarios Registrados.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Equipo de C&oacute;mputo de Propiedad Particular: Significa cualquier equipo de c&oacute;mputo y/o electr&oacute;nico propiedad de los Usuarios Registrados, (como pudiera ser de forma enunciativa, m&aacute;s no limitativa; laptops, netbooks, ipads, ipods, c&aacute;maras fotogr&aacute;ficas, tel&eacute;fonos celulares, etc&hellip;) que sea ingresado por dichos Usuarios Registrados a las Instalaciones para utilizar cualquiera de los Servicios prestados mediante el programa Telmex Hub.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Instalaciones&rdquo;: Significan todas las facilidades, mobiliario y dem&aacute;s equipamiento propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX) que se encuentren dentro del inmueble ubicado en la calle de Isabel la Cat&oacute;lica No. 51, Colonia Centro Hist&oacute;rico, CP. 06002, Delegaci&oacute;n Cuauhtemoc, M&eacute;xico, Distrito Federal.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Inttelmex&rdquo;: Significa el</font></font><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3">&nbsp;</font></font><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Representante del menor&rdquo;: Significa, en t&eacute;rminos del C&oacute;digo Civil para el Distrito Federal, cualquiera de los padres de los menores de edad que ingresen a las Instalaciones, su tutor o aquella persona quien ejerza la patria potestad sobre aquellos.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Software&rdquo;: Significa el conjunto de programas, instrucciones y reglas inform&aacute;ticas que permiten a los Usuarios Registrados ejecutar distintas tareas en cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Telmex&rdquo;: Tel&eacute;fonos de M&eacute;xico, S.A.B. de C.V.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Usuario&rdquo;: Significa cualquier persona f&iacute;sica, mayor de edad, que se encuentre en pleno goce y ejercicio de las facultades que le concede la Legislaci&oacute;n Mexicana vigente.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000">&ldquo;<font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Usuario Registrado&rdquo;: Significa aquel Usuario que ha completado su inscripci&oacute;n l Sistema del programa Telmex Hub.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="2">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>OBJETO.</b></font>&nbsp;<font size="2" style="font-size: 9pt">La finalidad del presente Reglamento es establecer los t&eacute;rminos y las condiciones aplicables a los Servicios proporcionados por INTTELMEX mediante el programa Telmex Hub.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<ol start="3">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>SERVICIOS.</b></font>&nbsp;<font size="2" style="font-size: 9pt">INTTELMEX, a trav&eacute;s de su programa Telmex Hub, proporcionar&aacute; de forma gratuita, a los Usuarios Registrados que as&iacute; lo soliciten, dentro del horario de servicio y sujeto a los t&eacute;rminos y condiciones aqu&iacute; establecidos, los siguientes servicios (en lo sucesivo, los &ldquo;Servicios&rdquo;):</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Pr&eacute;stamo de uso temporal, dentro de las Instalaciones, del &ldquo;Equipo de Computo&rdquo; con acceso a Internet de alta velocidad.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Pr&eacute;stamo de uso temporal, dentro de las Instalaciones, de las &ldquo;Consolas de Videojuegos&rdquo;.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Acceso a Internet inal&aacute;mbrico de alta velocidad dentro de las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Acceso a Internet al&aacute;mbrico mediante conexi&oacute;n v&iacute;a cable de Ethernet</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Uso de las instalaciones para organizar talleres, conferencias, charlas, encuentros, presentaciones y reuniones de trabajo.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">La prestaci&oacute;n de los servicios por parte de INTTELMEX estar&aacute; sujeta en todo momento a la disponibilidad que haya en las Instalaciones de los &ldquo;Equipos de Computo&rdquo;.</font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="4">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt"><b>CONDICIONES APLICABLES A LA PRESTACI&Oacute;N DE LOS SERVICIOS</b></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Todo Usuario Registrado que acuda a las Instalaciones podr&aacute; hacer uso de los Servicios s&oacute;lo si se encuentra registrado en el sistema y ha firmado el reglamento.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INTTELMEX proporcionar&aacute; a los Usuarios Registrados que as&iacute; lo soliciten, un &ldquo;Equipo de C&oacute;mputo&rdquo;, para que &eacute;ste sea utilizado dentro de las Instalaciones, conforme a los horarios de servicio y las pol&iacute;ticas de uso establecidas por el Programa Telmex Hub.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El Usuario Registrado deber&aacute; verificar el estado que guarda el &ldquo;Equipo de C&oacute;mputo&rdquo; al momento en que el mismo le sea entregado, toda vez que &eacute;ste ser&aacute; acreedor a las sanciones que se detallan m&aacute;s adelante en caso de encontrarse en el &ldquo;Equipo de Computo&rdquo; se&ntilde;ales de deterioro.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados s&oacute;lo podr&aacute; solicitar el pr&eacute;stamo de un &ldquo;Equipo de C&oacute;mputo&rdquo; o una &ldquo;Consola de Videojuegos&rdquo; por visita a las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El pr&eacute;stamo de uso de los &ldquo;Equipos de C&oacute;mputo&rdquo; y de las &ldquo;Consolas de Videojuegos&rdquo; estar&aacute; sujeto a disponibilidad de unidades.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El Usuario deber&aacute; devolver el &ldquo;Equipo de C&oacute;mputo&rdquo; en el mismo estado en que fue proporcionado, 30 minutos antes del cierre de las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El personal del Programa Telmex Hub levantar&aacute; un inventario al momento de la devoluci&oacute;n de los &ldquo;Equipos de C&oacute;mputo&rdquo; y de las &ldquo;Consolas de Videojuegos&rdquo;, el cual contendr&aacute; la descripci&oacute;n del estado general de &eacute;ste y, en su caso, el detalle de los accesorios del mismo.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Por ning&uacute;n motivo se podr&aacute; prestar m&aacute;s de un &ldquo;Equipo de C&oacute;mputo&rdquo; o &ldquo;Consola de Videojuegos&rdquo; a un s&oacute;lo Usuario Registrado de forma simult&aacute;nea.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados ser&aacute;n en todo momento los &uacute;nicos responsables del &ldquo;Equipo de Computo&rdquo; o &ldquo;Consola de Videojuegos&rdquo; que INTTELMEX les entregue, incluso ser&aacute;n responsables por el uso que hagan terceras personas de dichos equipos dentro de las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados ser&aacute;n en todo momento, los &uacute;nicos responsables de los contenidos digitales, informaci&oacute;n, proyectos, trabajos, obras, archivos, documentos, etc., que sean cargados, descargados, guardados, utilizados, modificados, alterados, desarrollados, dise&ntilde;ados, creados, o a los que tengan acceso mediante los &ldquo;Equipo de C&oacute;mputo&rdquo; y/o los Servicios, as&iacute; como, de las actividades, cursos y/o talleres organizadas, creados y/o desarrollados por los propios Usuarios Registrados, por lo que se obligan a mantener en paz y a salvo a &ldquo;TELMEX&rdquo; y/o a &ldquo;INTTELMEX&rdquo;, en caso de que cualquier tercero y/o autorizado inicie en contra de cualquiera de &eacute;stas &uacute;ltimas alguna denuncia, querella, juicio o procedimiento judicial o administrativo, relacionado de manera directa o indirecta con todo lo anterior.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Por ning&uacute;n motivo se autorizar&aacute; a los Usuarios Registrados el acceso a las Instalaciones si se encuentran bajo los efectos de alg&uacute;n estupefaciente, droga, estimulante o cualquier sustancia similar, o con aliento alcoh&oacute;lico.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="5">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>CONDICIONES DE USO DE LOS EQUIPOS.&nbsp;</b></font><font size="2" style="font-size: 9pt">Mientras los Usuarios Registrados utilicen los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o las &ldquo;Consolas de Videojuegos&rdquo; deber&aacute;n observar lo siguiente:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados deber&aacute;n utilizar los &ldquo;Equipo de C&oacute;mputo&rdquo; &uacute;nicamente para fines educativos, laborales, de esparcimiento u otros de naturaleza an&aacute;loga tendientes a propiciar la colaboraci&oacute;n e intercambio de conocimiento.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados deber&aacute;n utilizar las &ldquo;Consolas de Videojuegos&rdquo; conforme a los fines y a la naturaleza para la cual estos fueron dise&ntilde;ados.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Cualquier uso diverso que se le d&eacute; a los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o a las &ldquo;Consolas de Videojuegos&rdquo; por parte de los Usuarios Registrados, podr&aacute; ser sancionado por INTTELMEX, en t&eacute;rminos de lo previsto por el presente Reglamento.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados ser&aacute;n responsables de la custodia y el buen uso de los &ldquo;Equipos de C&oacute;mputo&rdquo; mientras los tengan en pr&eacute;stamo, por lo que deber&aacute;n abstenerse de copiar, reproducir, distribuir, aplicar ingenier&iacute;a inversa, descompilar o realizar obras derivativas, de cualquier programa o aplicaci&oacute;n de software que venga incluido en los &ldquo;Equipos de C&oacute;mputo&rdquo; otorgados en pr&eacute;stamo, por lo que, en caso de que INTTELMEX denote alguna falta, el Usuario Registrado en cuesti&oacute;n podr&aacute; ser acreedor a alguna de las penalizaciones establecidas en este Reglamento.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt">El Usuario deber&aacute; mantener la integridad del Hardware y Software de los &ldquo;Equipos de C&oacute;mputo&rdquo;, por lo que tiene prohibido modificar la configuraci&oacute;n del equipo que se le asigne, incluyendo en forma enunciativa, mas no limitativa: la implementaci&oacute;n de cualquier modificaci&oacute;n a la configuraci&oacute;n de los sistemas operativos, el cambio de&nbsp;</font><font size="2" style="font-size: 9pt"><i>passwords</i></font>&nbsp;<font size="2" style="font-size: 9pt">o contrase&ntilde;as, la instalaci&oacute;n y/o descarga de software, programas, aplicaciones y widgets maliciosos, o visitar sitios en Internet con contenido contrario a la moral y las buenas costumbres.</font></font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados deber&aacute;n abstenerse de guardar informaci&oacute;n en el disco duro de los &ldquo;Equipos de C&oacute;mputo&rdquo;.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados asumen la responsabilidad total respecto de cualquier reclamaci&oacute;n, queja o demanda en contra de TELMEX y/o INTTELMEX y se obliga a mantenerlo en paz y a salvo de cualquier demanda, querella o acci&oacute;n legal que pueda ser presentada en su contra, relacionada con cualquier queja o reclamaci&oacute;n en este sentido.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados deber&aacute;n reportar inmediatamente las fallas o desperfectos que se presenten en los &ldquo;Equipos de C&oacute;mputo&rdquo; que tengan en pr&eacute;stamo, as&iacute; como cualquier otro detalle que pueda poner en riesgo al equipo, sus componentes, su sistema operativo y/o el software.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados se comprometen &uacute;nicamente a introducir en los &ldquo;Equipo de Computo&rdquo; dispositivos de almacenamiento (discos compactos, memorias USB, discos duros externos, DVD&rsquo;s) libres de virus inform&aacute;ticos y en buen estado de uso, para evitar cualquier da&ntilde;o a los equipos.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados son los &uacute;nicos responsables de los archivos, im&aacute;genes, informaci&oacute;n o cualquier tipo de dato que descarguen y de las consecuencias que ello pueda producir.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados se comprometen a cumplir con las pol&iacute;ticas sobre descarga de documentos, licencias y/o software del programa Telmex Hub, publicadas de tiempo en tiempo en la direcci&oacute;n web: www.telmexhub.mx</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="6">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>USUARIOS DEL SERVICIO.</b></font>&nbsp;<font size="2" style="font-size: 9pt">INTTELMEX a trav&eacute;s de su programa Telmex Hub ofrece sus servicios para el p&uacute;blico en general, siempre que los Usuarios cumplan con los siguientes requisitos y obtengan su registro:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Sean personas mayores de edad.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Est&eacute;n en pleno ejercicio de sus facultades.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Hayan firmado el reglamento</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Cuente con experiencia y/o con conocimientos b&aacute;sicos en el manejo, manipulaci&oacute;n, utilizaci&oacute;n y funcionamiento de los &ldquo;Equipos de Computo&rdquo; y/o las &ldquo;Consolas de Videojuegos&rdquo;.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Registren su entrada y su respectiva salida de las Instalaciones.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 1.27cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.27cm; margin-bottom: 0cm; "><span style="font-size: 9pt; ">Para que un menor de edad pueda; i) acceder a las Instalaciones, y/o ii) utilizar los Servicios que ofrece INTTELMEX a trav&eacute;s de su programa Telmex Hub deber&aacute;:</span></p>
<p align="JUSTIFY" style="margin-left: 1.27cm; margin-bottom: 0cm; "><span style="font-size: 9pt; ">1. Ser mayor de 13 a&ntilde;os de edad.</span></p>
<p align="JUSTIFY" style="margin-left: 1.27cm; margin-bottom: 0cm; "><span style="font-size: 9pt; ">2. a. estar acompa&ntilde;ado en todo momento por el &ldquo;Representante del menor&rdquo;,</span></p>
<p class="rteleft" style=""><span style="font-size: 9pt; ">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;b. contar la a</span><span style="font-size: 9pt; ">utorizaci&oacute;n expresa y por escrito del &ldquo;Representante del menor&rdquo;, para lo cual, el &ldquo;Representante</span></p>
<p style=""><span style="font-size: 9pt; ">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;del menor&rdquo;&nbsp;</span><span style="font-size: 9pt; ">&nbsp;deber&aacute; estar previamente inscrito en el Sistema su programa Telmex Hub como &ldquo;Usuario &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>&nbsp; &nbsp;​ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="font-size: 9pt; ">Registrado&quot;&nbsp;</span></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;c. demostrar con una identificaci&oacute;n con fotograf&iacute;a y el acta de nacimiento, la edad del menor.</font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; ">&nbsp;</p>
<ol start="7">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>PROCESO DE REGISTRO.</b></font>&nbsp;<font size="2" style="font-size: 9pt">Para que un &ldquo;Usuario&rdquo; pueda tener acceso a los servicios dentro de las Instalaciones, es necesario que se inscriba en el Sistema del programa Telmex Hub, para lo cual deber&aacute; concluir el siguiente Proceso de Registro:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El &ldquo;Usuario&rdquo; deber&aacute; presentar la siguiente documentaci&oacute;n en el &aacute;rea de registro que se ubica dentro de las Instalaciones:</font></font></font></p>
				<ol>
					<li>
						<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Identificaci&oacute;n Oficial (preferentemente la credencial para votar con fotograf&iacute;a expedida por el Instituto Federal Electoral).</font></font></font></p>
					</li>
				</ol>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 2.5cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Nota: En caso de que se pretenda autorizar a un menor de edad en el Sistema para la utilizaci&oacute;n de los Servicios, se requiere la acreditaci&oacute;n previa, de la paternidad, tutor&iacute;a o el ejercicio de la patria potestad sobre el menor en cuesti&oacute;n, para lo cual deber&aacute; presentar; Acta de Nacimiento del menor o cualquier otro documento oficial, como lo es el pasaporte vigente.</font></font></font></p>
<ol>
	<li>
		<ol start="2" type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Una vez validada la informaci&oacute;n y la documentaci&oacute;n proporcionada por el Usuario, se le dar&aacute; de alta en el Sistema como Usuario Registrado, con lo que se le proporcionar&aacute; acceso a los Servicios, mediante la creaci&oacute;n de una cuenta de usuario.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El Usuario Registrado es y ser&aacute; el &uacute;nico responsable de la adecuada utilizaci&oacute;n de la cuenta de Usuario que se le asigne.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INTTELMEX a trav&eacute;s de su programa Telmex Hub se reservar&aacute; en todo momento el derecho de denegar el alta en el Sistema, a cualquier Usuario, de manera discrecional.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INTTELMEX a trav&eacute;s de su programa Telmex Hub podr&aacute; dar de baja del Sistema, discrecionalmente y en cualquier momento, a los &ldquo;Usuarios Registrados&rdquo; que infrinjan alguna de las disposiciones del presente Reglamento.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="8">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>HORARIO DE SERVICIO.</b></font>&nbsp;<font size="2" style="font-size: 9pt">El horario regular en el cual INTTELMEX, a trav&eacute;s de su programa Telmex Hub, prestar&aacute; los Servicios a los Usuarios Registrados, es de lunes a viernes de 11:00 hrs. a 20:00 hrs. y s&aacute;bados de 11:00 hrs. a 20:00 hrs.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INTTELMEX, a trav&eacute;s de su programa Telmex Hub, se reservar&aacute; en todo momento el derecho de modificar el horario regular de Servicio, por lo que podr&aacute; establecer discrecionalmente horarios especiales de Servicio que obedezcan a las necesidades de nuevos programas y/o proyectos. Los horarios especiales de servicio pueden consultarse en la direcci&oacute;n web: www.telmexhub.mx</font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Algunos de los Servicios como el pr&eacute;stamo de proyectores, videowalls, consolas de videojuegos, ipads o cualquier otro equipo u aparato propiedad de INTTELMEX podr&aacute; proporcionarse en un horario m&aacute;s restringido y sujeto a la disponibilidad de unidades.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<ol start="9">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>ACCESO A LAS INSTALACIONES.</b></font>&nbsp;<font size="2" style="font-size: 9pt">Tendr&aacute;n acceso a las Instalaciones y por consiguiente a los Servicios proporcionados por INTTELMEX a trav&eacute;s de su programa Telmex Hub los &ldquo;Usuarios Registrados&rdquo; o en su caso los menores de edad que hayan cumplido con los requisitos establecidos por la cl&aacute;usula 6 del presente Reglamento.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los &ldquo;Usuarios Registrados&rdquo; que soliciten accesar a las Instalaciones con Equipo de computo de propiedad particular podr&aacute;n hacerlo siempre que dicho equipo sea registrado previamente a su ingreso.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Es responsabilidad de cada Usuario Registrado el ingreso a las Instalaciones de cualquier Equipo de c&oacute;mputo de propiedad particular, por lo que en este acto libera a TELMEX, INTTELMEX, y a su programa Telmex Hub de cualquier responsabilidad que pueda surgir por cualquier da&ntilde;o, p&eacute;rdida, robo, extrav&iacute;o, deterioro o menoscabo que pudiera sufrir dicho equipo mientras se encuentre en las Instalaciones.</font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="10">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>PROHIBICIONES</b></font><font size="2" style="font-size: 9pt">. los Usuarios Registrados tienen prohibido:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Utilizar las instalaciones con prop&oacute;sitos diferentes para los que fueron creadas.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Comportarse indebidamente dentro de las Instalaciones, como pudiera ser, de forma enunciativa, m&aacute;s no limitativa; la ejecuci&oacute;n individual o colectiva de actos que pudieran da&ntilde;ar las Instalaciones como lo es el vandalismo, el desorden p&uacute;blico, ri&ntilde;as, peleas, discusiones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Consumir alimentos, bebidas y/o fumar dentro de las Instalaciones, salvo en las zonas especialmente designadas y se&ntilde;aladas por el Programa Telmex Hub.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Hacer ruido molesto o excesivo, escuchar m&uacute;sica con un nivel de volumen que distraiga a los dem&aacute;s Usuarios Registrados.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">La utilizaci&oacute;n de altavoces conectados a los &ldquo;Equipos de C&oacute;mputo&rdquo; o a equipos de m&uacute;sica personales.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Sustraer cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;, &ldquo;Consola de Videojuegos&rdquo; o cualquier otro equipo de las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Utilizar los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o &ldquo;Las Consolas de Videojuegos&rdquo; para fines diferentes a los aqu&iacute; establecidos.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Rayar, da&ntilde;ar, maltratar y/o vandalizar los &ldquo;Equipos de C&oacute;mputo&rdquo;, &ldquo;Consolas de Videojuegos&rdquo;, y/o las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Descargar, introducir, dise&ntilde;ar, crear y/o utilizar programas, aplicaciones y/o c&oacute;digos maliciosos y/o cualquier clase de virus que pudieran da&ntilde;ar o afectar en los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o la red de TELMEX y/o de INTTELMEX.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Cualquier comportamiento violento o cualquier forma de agresi&oacute;n f&iacute;sica, verbal, escrita o por cualquier medio hacia otros Usuarios Registrados o hacia el personal que labora dentro de las Instalaciones.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Presentarse en estado de ebriedad o bajo el influjo de drogas enervantes o cualquier otra, que produzca efectos similares.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Ingresar a las &aacute;reas de acceso restringido.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Prestar, arrendar, transmitir o intercambiar las cuentas de acceso al Sistema del programa Telmex Hub.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Realizar y/o ejecutar cualquier clase de ataque u agresi&oacute;n hacia la red, sistemas, servidores y/o p&aacute;ginas o sitios web propiedad de TELMEX y/o INTTELMEX, as&iacute; como la utilizaci&oacute;n de los &ldquo;Equipos de C&oacute;mputo&rdquo; o los Servicios para efectuar cualquier clase de agresiones a redes, sistemas, servidores y/o p&aacute;ginas o sitios web de terceros.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Intente abrir o reparar los &ldquo;Equipos de C&oacute;mputo&rdquo;, las &ldquo;Consolas de Videojuegos&rdquo; o cualquiera de sus accesorios.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Modificar la configuraci&oacute;n del &ldquo;Equipo de C&oacute;mputo&rdquo; que se les asigne, incluyendo en forma enunciativa, m&aacute;s no limitativa: aplicaciones, programas, instalar y/o descargar software malicioso.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 2.5cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Nota: Las pol&iacute;ticas sobre descarga de documentos, licencias y/o programas podr&aacute;n ser consultadas en la pagina oficial del programa Telmex Hub en la siguiente direcci&oacute;n web: www.telmexhub.mx</font></font></font></p>
<ol>
	<li>
		<ol start="17" type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Visitar sitios en Internet con contenido contrario a la moral y las buenas costumbres.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Utilizar el &ldquo;Equipo de C&oacute;mputo&rdquo; para descargar o publicar material o contenido pornogr&aacute;fico, violento o inapropiado.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Utilizar el &ldquo;Equipo de C&oacute;mputo&rdquo; para desarrollar, almacenar, descargar, distribuir o difundir virus inform&aacute;ticos o c&oacute;digos inform&aacute;ticos malignos.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Utilizar los &ldquo;Equipos de C&oacute;mputo&rdquo; para copiar, reproducir, distribuir, aplicar ingenier&iacute;a inversa, descompilar o realizar obras derivativas, de cualquier programa o aplicaci&oacute;n de software que venga incluido en los &ldquo;Equipos de C&oacute;mputo&rdquo;.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="11">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>REPARACI&Oacute;N DE DA&Ntilde;OS.&nbsp;</b></font><font size="2" style="font-size: 9pt">INTTELMEX podr&aacute; cobrar el monto total de la reparaci&oacute;n respectiva a los Usuarios Registrados que:</font></font></font></font></p>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">devuelvan los &ldquo;Equipo de C&oacute;mputo&rdquo; o las &ldquo;Consolas de Videojuegos&rdquo; con se&ntilde;ales evidentes de deterioro f&iacute;sico o da&ntilde;o &ldquo;irreparable&rdquo; (da&ntilde;o de hardware),</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">causen cualquier da&ntilde;o o desperfecto a las Instalaciones,</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Da&ntilde;en de cualquier forma el software precargado en los &ldquo;Equipos de C&oacute;mputo&rdquo;.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Da&ntilde;en, alteren o modifiquen el sistema operativo de los &ldquo;Equipos de C&oacute;mputo&rdquo;.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">En caso de robo, extrav&iacute;o o p&eacute;rdida, total o parcial, del &ldquo;Equipo de C&oacute;mputo&rdquo;, &ldquo;Consola de Videojuegos&rdquo; o de cualquiera de sus accesorios, INTTELMEX podr&aacute; cobrarle al Usuario Registrado responsable, el monto correspondiente del equipo conforme al valor comercial del mismo.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">El Usuario Registrado se obliga a reponer el costo total del &ldquo;Equipo de C&oacute;mputo&rdquo; robado, extraviado o da&ntilde;ado (da&ntilde;o de hardware o software), en un periodo no mayor a 15 d&iacute;as naturales, contados a partir de la fecha en que la Administraci&oacute;n del programa Telmex Hub se percat&oacute; de tal circunstancia o, a trav&eacute;s de la suscripci&oacute;n de un pagar&eacute; por el monto total del equipo a nombre de Tel&eacute;fonos de M&eacute;xico S.A.B. de C.V., quedando &eacute;ste facultado para su ejecuci&oacute;n inmediata, sin necesidad de declaraci&oacute;n judicial.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<ol start="12">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>PROPIEDAD INTELECTUAL.</b></font>&nbsp;<font size="2" style="font-size: 9pt">INTTELMEX a trav&eacute;s de su programa Telmex Hub reconoce que los Usuarios Registrados gozar&aacute;n, en lo que les corresponda, de los derechos otorgados por las leyes vigentes en materia de derechos de autor y propiedad industrial, tanto en la Rep&uacute;blica Mexicana, como en el extranjero.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">INTTELMEX a trav&eacute;s de su programa Telmex Hub reconoce de manera expresa que aquellos trabajos que se originen de la utilizaci&oacute;n de los &ldquo;Equipos de C&oacute;mputo&rdquo; o que se desarrollen por parte de los Usuarios Registrados dentro de las Instalaciones como consecuencia de la utilizaci&oacute;n de los Servicios y que sean susceptibles de protecci&oacute;n intelectual, corresponder&aacute;n al Usuario Registrado haya realizado el trabajo objeto de protecci&oacute;n, d&aacute;ndole el debido reconocimiento a quienes hayan intervenido en la realizaci&oacute;n del mismo, conforme a las disposiciones legales aplicables, y en su caso, el reconocimiento p&uacute;blico a TELMEX, INTTELMEX y al Programa Telmex Hub por el apoyo brindado a trav&eacute;s de los Servicios objeto del presente.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Los Usuarios Registrados se obligan a mantener en paz y a salvo a &ldquo;TELMEX&rdquo; y/o a &ldquo;INTTELMEX&rdquo;, en caso de que cualquier tercero inicie en contra de cualquiera de &eacute;stas alguna denuncia, querella, juicio o procedimiento judicial o administrativo, relacionado de manera directa o indirecta con cualquiera de los contenidos digitales, informaci&oacute;n, proyectos, trabajos, obras, archivos, documentos, etc., que sean cargados, descargados, guardados, utilizados, modificados, alterados, desarrollados, dise&ntilde;ados y/o creados en los &ldquo;Equipos de C&oacute;mputo&rdquo;.</font></font></font></p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Todos aquellos gastos de indemnizaci&oacute;n y sanciones o multas que en forma temporal o definitiva deban ser pagados a cualquier autoridad, despachos de abogados, o a cualquier tercero con motivo de la violaci&oacute;n a los derechos de propiedad industrial o intelectual de terceros, quedar&aacute;n a cargo del Usuario Registrado que los ocasione, dejando a &ldquo;TELMEX&rdquo; y/o a &ldquo;INTTELMEX&rdquo; libre(s) de cualquier responsabilidad.</font></font></font></p>
<p style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="13">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>SANCIONES.&nbsp;</b></font><font size="2" style="font-size: 9pt">Independientemente de lo establecido en el presente Reglamento para la reparaci&oacute;n de los da&ntilde;os, aquel Usuario Autorizado que sea sorprendido extrayendo cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;, &ldquo;Consola de Videojuegos&rdquo; o cualquiera de sus accesorios de las Instalaciones sin contar con la debida autorizaci&oacute;n, o bien, mutilando, marcando, rayando, da&ntilde;ando o alterando las Instalaciones, o haciendo un mal uso de cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;, ser&aacute; turnado a las autoridades correspondientes.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-left: 1.25cm; margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">En caso de p&eacute;rdida o robo del &ldquo;Equipo de C&oacute;mputo&rdquo;, el Usuario Registrado deber&aacute;:</font></font></font></p>
<ol>
	<li>
		<ol type="i">
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Notificar inmediatamente la p&eacute;rdida del &ldquo;Equipo de C&oacute;mputo&rdquo; a la Administraci&oacute;n del programa Telmex Hub.</font></font></font></p>
			</li>
			<li>
				<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">Responder de dicha p&eacute;rdida en los t&eacute;rminos establecidos en apartado de reparaci&oacute;n de da&ntilde;os del presente documento.</font></font></font></p>
			</li>
		</ol>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<ol start="14">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt"><b>OTRO TIPO DE SANCIONES.</b></font></font></font></p>
	</li>
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>.&nbsp;</b></font><font size="2" style="font-size: 9pt">Independientemente de lo establecido en los apartados de Reparaci&oacute;n de Da&ntilde;os y Sanciones, cada vez que un Usuario Registrado incumpla con cualquiera de las disposiciones contenidas en el presente Reglamento, recibir&aacute; un aviso o reporte. La acumulaci&oacute;n de avisos o reportes por parte de los Usuarios Registrados traer&aacute; aparejada las siguientes sanciones:</font></font></font></font></p>
	</li>
</ol>
<ol type="i">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">La acumulaci&oacute;n de dos avisos o reportes en un periodo de una semana: la suspensi&oacute;n temporal del Registro como Usuario, y por consiguiente la prohibici&oacute;n del Ingreso a las Instalaciones hasta por un lapso de un mes.</font></font></font></p>
	</li>
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt">La acumulaci&oacute;n de tres avisos o reportes: la expulsi&oacute;n definitiva del programa Telmex Hub.</font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="16">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>Realizaci&oacute;n de eventos.&nbsp;</b></font><font size="2" style="font-size: 9pt">Las normas, reglas y pol&iacute;ticas aplicables a la organizaci&oacute;n y log&iacute;stica para la realizaci&oacute;n de eventos dentro de las instalaciones del Telmex Hub se deber&aacute; consultar con el personal autorizado y/o en la direcci&oacute;n web: www.telmexhub.mx</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<ol start="17">
	<li>
		<p align="JUSTIFY" style="margin-bottom: 0cm; border: none; padding: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="3"><font size="2" style="font-size: 9pt"><b>CASOS NO PREVISTOS.&nbsp;</b></font><font size="2" style="font-size: 9pt">Los casos no previstos en el presente Reglamento, ser&aacute;n resueltos por el Director del programa Telmex Hub.</font></font></font></font></p>
	</li>
</ol>
<p align="JUSTIFY" style="margin-left: 0.64cm; margin-bottom: 0cm; ">&nbsp;</p>
<p align="JUSTIFY" style="margin-bottom: 0cm; "><font color="#000000"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt"><b>Le&iacute;do que es el presente reglamento, lo firma de conformidad el usuario registrado enunciado al calce, en la Ciudad de M&eacute;xico a los ______ d&iacute;as del mes de _________ del a&ntilde;o 201___.</b></font></font></font></p>
<p align="JUSTIFY" style="margin-bottom: 0cm; ">&nbsp;</p>
<table border="1" bordercolor="#000000" cellpadding="7" cellspacing="0" width="719">
	<colgroup>
		<col width="258" />
		<col width="21" />
		<col width="396" />
	</colgroup>
	<tbody>
		<tr valign="TOP">
			<td width="258">
				<p align="JUSTIFY">&nbsp;</p>
			</td>
			<td width="21">
				<p align="JUSTIFY">&nbsp;</p>
			</td>
			<td width="396">
				<p align="JUSTIFY">&nbsp;</p>
			</td>
		</tr>
		<tr valign="TOP">
			<td width="258">
				<p align="CENTER"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt"><b>nombre del usuario</b></font></font></p>
			</td>
			<td width="21">
				<p align="CENTER">&nbsp;</p>
			</td>
			<td width="396">
				<p align="CENTER"><font face="Arial, Helvetica, Nimbus Sans L, sans-serif"><font size="2" style="font-size: 9pt"><b>FIRMA DE CONFORMIDAD</b></font></font></p>
			</td>
		</tr>
	</tbody>
</table>
<p><br />
	<br />
	&nbsp;</p>




	</article>
</section>