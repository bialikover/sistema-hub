<section id="blog-content">
	<div id="blog">
		<h1>Blog</h1>
	</div>	
	<article class="entry-single">
		<div class="main-image">
			<img src="<?php echo base_url('assets/posts/'.$post->image_url)?>" alt="<?php echo $post->name;?>" title="<?php echo $post->name;?>" width="1024" height="250"/>
		</div>
		<section class="description">
			<h1><?php echo $post->name;?></h1>
			<h5><?php echo $post->category;?></h5>
			<date><?php echo date('F j, Y', $post->published_on);?></date>
			
			<h4><?php echo $post->excerpt;?></h4>
		</section>
		<section class="entry-content">
			<p><?php echo $post->description;?></p>
		</section>
		<section class="tags">
			<?php foreach ($post->tags as $tag):?>
				<?php echo $tag['tag_name'];?>
			<?php endforeach;?>
		</section>
	</article>	
</section>
<div class="clearfix"></div>

<!--<?php echo var_dump($post);?> -->