<section id="blog-content">
	<div id="blog">
		<h1>Blog</h1>
	</div>
	<?php //echo var_dump($posts);?>
	<?php foreach ($posts as $post):?>
	<article class="entry">
		<a href="<?php echo base_url('index.php/blog/'.$post->slug);?>">
		<img src="<?php echo base_url('assets/posts/'.$post->image_url)?>" alt="<?php echo $post->name;?>" title="<?php echo $post->name;?>"/>
		<section class="description">
			<h2><?php echo $post->name;?></h2>
			<date><?php echo date('F j, Y', $post->published_on);?></date>
			<p><?php echo $post->excerpt;?></p>
		</section>
		</a>
	</article>
	<?php endforeach;?>
</section>
<div class="clearfix"></div>