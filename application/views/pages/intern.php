
<section id ="terms">
	<article class="brief">
		<h1 style="color:#000;text-align:center">Reglamento Interno</h1>		
		<p>&nbsp;</p>
<p>TELMEX-HUB</p>
<p align="center"><strong>REGLAMENTO COMUNIDADES</strong></p>
<p>&nbsp;</p>
<p class="rtejustify" style="">El presente documento contiene las bases conforme las cuales INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), a trav&eacute;s de su programa Telmex Hub, proporcionar&aacute; a las comunidades que accedan a las salas de coworking y usen las instalaciones y servicios que a continuaci&oacute;n se describen.</p>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="">
		<strong>DEFINICIONES.</strong> Para los efectos del presente Reglamento se entiende por:
		<ol>
			<li style="">
				Programa &ldquo;Telmex Hub&rdquo;: es un proyecto que proporciona a sus usuarios, de forma gratuita, infraestructura tecnol&oacute;gica de &uacute;ltima generaci&oacute;n con el objetivo de fomentar la colaboraci&oacute;n, la innovaci&oacute;n, as&iacute; como la creaci&oacute;n y desarrollo de toda clase de proyectos.</li>
			<li style="">
				&ldquo;Consolas de Videojuegos&rdquo;: significa el equipo electr&oacute;nico de entretenimiento, propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), y que a trav&eacute;s de su programa Telmex Hub, concede su uso temporal dentro de las instalaciones, a los usuarios registrados.</li>
			<li style="">
				&ldquo;Equipo de C&oacute;mputo&rdquo;: Significa; i) el equipo de c&oacute;mputo port&aacute;til (Laptop y/o Netbook), ii) el equipo de computo de escritorio, y/o iii) cualquier otro aparato electr&oacute;nico, cableado y material did&aacute;ctico propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX), que a trav&eacute;s de su programa Telmex Hub, conceda su uso temporal a los Usuarios Registrados.</li>
			<li style="">
				Equipo de C&oacute;mputo de Propiedad Particular: Significa cualquier equipo de c&oacute;mputo y/o electr&oacute;nico propiedad de los Usuarios Registrados, (como pudiera ser de forma enunciativa, m&aacute;s no limitativa; laptops, netbooks, ipads, ipods, c&aacute;maras fotogr&aacute;ficas, tel&eacute;fonos celulares,&nbsp; etc&hellip;) que sea ingresado por dichos Usuarios Registrados a las Instalaciones para utilizar cualquiera de los Servicios prestados mediante el programa Telmex Hub.</li>
			<li style="">
				&ldquo;Instalaciones&rdquo;: Significan todas las facilidades, mobiliario y dem&aacute;s equipamiento propiedad de INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C. (INTTELMEX) que se encuentren dentro del inmueble ubicado en la calle de Isabel la Cat&oacute;lica No. 51, Colonia Centro Hist&oacute;rico, CP. 06002, Delegaci&oacute;n Cuauhtemoc, M&eacute;xico, Distrito Federal.</li>
			<li style="">
				&ldquo;Inttelmex&rdquo;: Significa el INSTITUTO TECNOL&Oacute;GICO DE TEL&Eacute;FONOS DE M&Eacute;XICO, S.C.</li>
			<li style="">
				&ldquo;Representante del menor&rdquo;: Significa, en t&eacute;rminos del C&oacute;digo Civil para el Distrito Federal, cualquiera de los padres de los menores de edad que ingresen a las Instalaciones, su tutor o aquella persona quien ejerza la patria potestad sobre aquellos.</li>
			<li style="">
				&ldquo;Software&rdquo;: Significa el conjunto de programas, instrucciones y reglas inform&aacute;ticas que permiten a los&nbsp; Usuarios Registrados ejecutar distintas tareas en cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;.</li>
			<li style="">
				&ldquo;Telmex&rdquo;: Tel&eacute;fonos de M&eacute;xico, S.A.B. de C.V.</li>
			<li style="">
				&ldquo;Usuario&rdquo;: Significa cualquier persona f&iacute;sica, mayor de edad, que se encuentre en pleno goce y ejercicio de las facultades que le concede la Legislaci&oacute;n Mexicana vigente.</li>
			<li style="">
				&ldquo;Usuario Registrado&rdquo;: Significa aquel Usuario que ha completado su inscripci&oacute;n&nbsp;&nbsp; l Sistema del programa Telmex Hub.</li>
			<li style="">
				&ldquo;Comunidades&rdquo; Conjunto o asociaci&oacute;n de personas que comparten intereses u objetivos comunes.</li>
		</ol>
	</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="2">
		<strong>OBJETO.</strong> La finalidad del presente Reglamento es establecer los t&eacute;rminos y las condiciones aplicables a los Servicios proporcionados por INTTELMEX mediante el programa Telmex Hub a las Comunidades.</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="3">
		<strong>SERVICIOS.</strong> INTTELMEX, a trav&eacute;s de su programa Telmex Hub, proporcionar&aacute; de forma gratuita, a las comunidades que as&iacute; lo soliciten, dentro del horario de servicio y sujeto a los t&eacute;rminos y condiciones aqu&iacute; establecidos, los siguientes servicios (en lo sucesivo, los &ldquo;Servicios&rdquo;):
		<ol>
			<li style="">
				Pr&eacute;stamo de uso temporal, dentro de las Instalaciones, del &ldquo;Equipo de Computo&rdquo; con acceso a Internet de alta velocidad.</li>
			<li style="">
				Pr&eacute;stamo de uso temporal, dentro de las Instalaciones, de las &ldquo;Consolas de Videojuegos&rdquo;.</li>
			<li style="">
				Acceso a Internet inal&aacute;mbrico de alta velocidad dentro de las Instalaciones.</li>
			<li style="">
				Acceso a Internet al&aacute;mbrico mediante conexi&oacute;n v&iacute;a cable de Ethernet</li>
			<li style="">
				Uso de las instalaciones para organizar talleres, conferencias, charlas, encuentros, presentaciones y reuniones de trabajo.</li>
			<li style="">
				Acceso y uso de las salas de coworking.</li>
		</ol>
	</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<p class="rtejustify" style="">La prestaci&oacute;n de los servicios por parte de INTTELMEX estar&aacute; sujeta en todo momento a la disponibilidad que haya en las Instalaciones.</p>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="4">
		<strong>CONDICIONES APLICABLES A LA PRESTACI&Oacute;N DE LOS SERVICIOS</strong></li>
</ol>
<p class="rtejustify" style="">i.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Todo Usuario Registrado que acuda a las Instalaciones podr&aacute; hacer uso de los Servicios s&oacute;lo si se encuentra registrado en el sistema y ha firmado el reglamento.</p>
<p class="rtejustify" style="">ii.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; INTTELMEX proporcionar&aacute; a los Usuarios Registrados que as&iacute; lo soliciten, un &ldquo;Equipo de C&oacute;mputo&rdquo;, para que &eacute;ste sea utilizado dentro de las Instalaciones, conforme a los horarios de servicio y las pol&iacute;ticas de uso establecidas por el Programa Telmex Hub.</p>
<p class="rtejustify" style="">iii.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; El Usuario Registrado deber&aacute; verificar el estado que guarda el &ldquo;Equipo de C&oacute;mputo&rdquo; al momento en que el mismo le sea entregado, toda vez que &eacute;ste ser&aacute; acreedor a las sanciones que se detallan m&aacute;s adelante en caso de encontrarse en el &ldquo;Equipo de Computo&rdquo; se&ntilde;ales de deterioro.</p>
<p class="rtejustify" style="">iv.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Los Usuarios Registrados s&oacute;lo podr&aacute; solicitar el pr&eacute;stamo de un &ldquo;Equipo de C&oacute;mputo&rdquo; o una &ldquo;Consola de Videojuegos&rdquo; por visita a las Instalaciones.</p>
<p class="rtejustify" style="">v.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; El pr&eacute;stamo de uso de los &ldquo;Equipos de C&oacute;mputo&rdquo; y de las &ldquo;Consolas de Videojuegos&rdquo; estar&aacute; sujeto a disponibilidad de unidades.</p>
<p class="rtejustify" style="">vi.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; El Usuario deber&aacute; devolver el &ldquo;Equipo de C&oacute;mputo&rdquo; en el mismo estado en que fue proporcionado, 30 minutos antes del cierre de las Instalaciones.&nbsp;</p>
<p class="rtejustify" style="">vii.&nbsp;&nbsp;&nbsp; El personal del Programa Telmex Hub levantar&aacute; un inventario al momento de la devoluci&oacute;n de los &ldquo;Equipos de C&oacute;mputo&rdquo; y de las &ldquo;Consolas de Videojuegos&rdquo;, el cual contendr&aacute; la descripci&oacute;n del estado general de &eacute;ste y, en su caso, el detalle de los accesorios del mismo.</p>
<p class="rtejustify" style="">viii.&nbsp;&nbsp; Por ning&uacute;n motivo se podr&aacute; prestar m&aacute;s de un &ldquo;Equipo de C&oacute;mputo&rdquo; o &ldquo;Consola de Videojuegos&rdquo; a un s&oacute;lo Usuario Registrado de forma simult&aacute;nea.</p>
<p class="rtejustify" style="">ix.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Los Usuarios Registrados ser&aacute;n en todo momento los &uacute;nicos responsables del &ldquo;Equipo de Computo&rdquo; o &ldquo;Consola de Videojuegos&rdquo; que INTTELMEX les entregue, incluso ser&aacute;n responsables por el uso que hagan terceras personas de dichos equipos dentro de las Instalaciones.</p>
<p class="rtejustify" style="">x.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Los Usuarios Registrados ser&aacute;n en todo momento, los &uacute;nicos responsables de los contenidos digitales, informaci&oacute;n, proyectos, trabajos, obras, archivos, documentos, etc., que sean cargados, descargados, guardados, utilizados, modificados, alterados, desarrollados, dise&ntilde;ados, creados, o a los que tengan acceso mediante los &ldquo;Equipo de C&oacute;mputo&rdquo; y/o los Servicios, as&iacute; como, de las actividades, cursos y/o talleres organizadas, creados y/o desarrollados por los propios Usuarios Registrados, por lo que se obligan a mantener en paz y a salvo a &ldquo;TELMEX&rdquo; y/o a &ldquo;INTTELMEX&rdquo;, en caso de que cualquier tercero y/o autorizado inicie en contra de cualquiera de &eacute;stas &uacute;ltimas alguna denuncia, querella, juicio o procedimiento judicial o administrativo, relacionado de manera directa o indirecta con todo lo anterior.</p>
<p class="rtejustify" style="">xi.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Por ning&uacute;n motivo se autorizar&aacute; a los Usuarios Registrados el acceso a las Instalaciones si se encuentran bajo los efectos de alg&uacute;n estupefaciente, droga, estimulante o cualquier sustancia similar, o con aliento alcoh&oacute;lico.</p>
<p class="rtejustify" style="">xii.&nbsp;&nbsp;&nbsp; Las comunidades que utilicen las salas de coworking deber&aacute;n entregar un reporte mensual a la direcci&oacute;n de Telmex Hub, en donde&nbsp; describan&nbsp; las actividades que realizan y los logros alcanzados.</p>
<p class="rtejustify" style="">xiii.&nbsp;&nbsp; Las comunidades que utilicen las salas de coworking deber&aacute;n entregar la lista de software descargado en el equipo de c&oacute;mputo de la sala de coworking a su disposici&oacute;n, a la direcci&oacute;n de Telmex Hub.</p>
<p class="rtejustify" style="">xiv.&nbsp; Las comunidades se har&aacute;n responsables del equipo de c&oacute;mputo de la sala de coworking a su disposici&oacute;n.</p>
<p class="rtejustify" style="">xv.&nbsp;&nbsp;&nbsp; La comunidad que tenga dos faltas en un mes se le retirar&aacute; el derecho exclusivo a la misma.</p>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="5">
		<strong>ACCESO A LAS SALAS DE COWORKING.</strong> Tendr&aacute;n acceso a las salas y por consiguiente a los Servicios proporcionados por INTTELMEX a trav&eacute;s de su programa Telmex Hub las &ldquo;Comunidades&rdquo; que hayan cumplido con los requisitos establecidos por la direcci&oacute;n de Telmex Hub.</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="6">
		<strong>PROHIBICIONES</strong>. los Usuarios Registrados tienen prohibido:
		<ol>
			<li style="">
				Utilizar las salas con prop&oacute;sitos diferentes para los que fueron creadas.</li>
			<li style="">
				Comportarse inadecuadamente dentro de las salas de coworking, como pudiera ser, de forma enunciativa, m&aacute;s no limitativa; la ejecuci&oacute;n individual o colectiva de actos que pudieran da&ntilde;ar las Instalaciones como lo es el vandalismo, el desorden p&uacute;blico, ri&ntilde;as, peleas, discusiones.</li>
			<li style="">
				Consumir alimentos, bebidas y/o fumar dentro de las salas de coworking.</li>
			<li style="">
				Hacer ruido molesto o excesivo, escuchar m&uacute;sica con un nivel de volumen que distraiga a los dem&aacute;s Usuarios Registrados.</li>
			<li style="">
				La utilizaci&oacute;n de altavoces conectados a los &ldquo;Equipos de C&oacute;mputo&rdquo; o a equipos de m&uacute;sica personales.</li>
			<li style="">
				Sustraer cualquier &ldquo;Equipo de C&oacute;mputo&rdquo;, &ldquo;Consola de Videojuegos&rdquo; o cualquier otro equipo de las Instalaciones.</li>
			<li style="">
				Utilizar los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o &ldquo;Las Consolas de Videojuegos&rdquo; para fines diferentes a los aqu&iacute; establecidos.</li>
			<li style="">
				Rayar, da&ntilde;ar, maltratar y/o vandalizar los &ldquo;Equipos de C&oacute;mputo&rdquo;, &ldquo;Consolas de Videojuegos&rdquo;, y/o las Instalaciones.</li>
			<li style="">
				Descargar, introducir, dise&ntilde;ar, crear y/o utilizar programas, aplicaciones y/o c&oacute;digos maliciosos y/o cualquier clase de virus que pudieran da&ntilde;ar o afectar en los &ldquo;Equipos de C&oacute;mputo&rdquo; y/o la red de TELMEX y/o de INTTELMEX.</li>
			<li style="">
				Cualquier comportamiento violento o cualquier forma de agresi&oacute;n f&iacute;sica, verbal, escrita o por cualquier medio hacia otros Usuarios Registrados o hacia el personal que labora dentro de las Instalaciones.</li>
			<li style="">
				Presentarse en estado de ebriedad o bajo el influjo de drogas enervantes o cualquier otra, que produzca efectos similares.</li>
		</ol>
	</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<ol>
	<li class="rtejustify" style="" value="7">
		<strong>CASOS NO PREVISTOS. </strong>Los casos no previstos en el presente Reglamento, ser&aacute;n resueltos por el Director del programa Telmex Hub.</li>
</ol>
<p class="rtejustify" style="">&nbsp;</p>
<p class="rtejustify" style=""><strong>Le&iacute;do que es el presente reglamento, lo firma de conformidad el usuario registrado enunciado al calce, en la Ciudad de M&eacute;xico a los ______ d&iacute;as del mes de ________________________ del a&ntilde;o 201___.</strong></p>
<p class="rtejustify" style="">&nbsp;</p>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<p class="rtejustify" style="">&nbsp;</p>
			</td>
			<td>
				<p class="rtejustify" style="">&nbsp;</p>
			</td>
			<td>
				<p class="rtejustify" style="">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td>
				<p class="rtecenter"><strong>Nombre del responsable de la comunidad</strong></p>
			</td>
			<td>
				<p class="rtecenter">&nbsp;</p>
			</td>
			<td>
				<p class="rtecenter"><strong>FIRMA DE CONFORMIDAD</strong></p>
			</td>
		</tr>
	</tbody>
</table>
<p>&nbsp;</p>
	</article>
</section>