<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h3>Eventos del día</h3>
</div>
<div class="modal-body">	
	<?php foreach ($events as $event):?>
	<div class="main-info">
		<div class="image-holder">
			<img src="<?php echo base_url('assets/events/'.$event->image_url)?>" class="polaroid" width="150" height="150"/>
		</div>
		<div class="info-holder">
			<h4><?php echo $event->name;?></h4>
			<div id="event-date">
				<p>
					<date class="start"> <?php echo date("l, d F H:i",$event->date_start);?> Hrs.</date>
				</p>
			</div>
			<div id ="event-meta">
				<h5>Tipo de evento: <?php echo $event->type;?></h5>
				<h5>Lugar: Sala <?php echo $event->workspace;?> del Hub</h5>
			</div>
		</div>
	</div>
	<div class="content">		
    	<p><?php echo $event->description;?></p>
    	<h6>Imparte: <?php echo $event->coach;?></h6>
    </div>
    <div class="rsvps">
        <?php if($this->ion_auth->logged_in()):?>
    	   <p class="rsvp <?php echo is_attending($this->session->userdata('user_id'), $event->id)?'cancel':'';?>" id="rsvp" data-eid="<?php echo $event->id;?>" onclick="attend(<?php echo $event->id;?>)">rsvp</p>
        <?php else:?>
            <p class="rsvp" id="rsvp" data-eid="<?php echo $event->id;?>" onclick="attend(<?php echo $event->id;?>)">rsvp</p>
        <?php endif;?>
    	<?php if(isset($event->rsvps)):?>
    	
    		<div class="rsvpsimages">
    			<?php foreach ($event->rsvps as $rsvp):?>
    				<a href="<?php echo base_url('index.php/usuario').'/'.$rsvp->uid?>">
                        <img alt="<?php echo $rsvp->name ?>" title="<?php echo $rsvp->name ?>" src="<?php echo $rsvp->image_url;?>" width="45" height="45" />
                    </a>
    			<?php endforeach;?>
    		</div>
    		<?php if( count($event->rsvps) > 10 ):?>
    			<div class="totalrsvps"><?php echo count($event->rsvps) - 10 ;?> </div>
    		<?php endif;?>
    	<?php endif;?>
    </div>
    <div class="clear"></div>
	</hr>
    <?php endforeach;?>

</div>
<div class="modal-footer">
</div>
