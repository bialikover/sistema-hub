
<section id ="about">
	<article class="brief">
		<h2>Acerca de TelmexHub</h2>
		<h4>TelmexHub es un punto de encuentro que genera conocimiento a partir de la participación activa de la comunidad.

Es un lugar físico y virtual para jóvenes, dónde la colaboración es el principal valor; ofrecemos talleres, cursos, pláticas, conferencias que la misma comunidad de TelmexHub promueve.</h4>
		<h4>En TelmexHub podrás encontrar gamers, diseñadores, programadores, estudiantes, publicistas, editores, periodistas, y medios sociales que se reúnen para ampliar sus proyectos compartiendo el aprendizaje.</h4>
		<br/>		
		<br/>		
		<h2>Actividades dentro de TelmexHub</h2>
		<h4>Todos los días sesiones de trabajo en colaboración, foros, conferencias, talleres, cursos, pláticas, encuentros de comunidades, gaming, lanzamiento de productos y todo lo que tú propongas. Mantente al tanto en www.telmexhub.com/calendario.</h4>
		<br/>		
		<br/>		
		<h2>Infraestructura</h2>
		<ul>
		<li><h4>Equipo de Computo: Laptops( Toshiba, Dell, Macbook Pro), iMac, ipad</h4></li>
		<li><h4>Sistemas operativos: Linux, Windows 7 y Snow Leopard.</h4></li>
		<li><h4>Conectividad: Acceso a banda ancha de 10Gb vía Ethernet y WiFi.</h4>		</li>
		<li><h4>Capacidad: 300 usuarios simultáneos, hasta150 conectados vía Ethernet.</h></li>
		</ul>
	</article>
</section>
