<?php $seleccionado = array();?>
<h1>Editar usuario</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('users/update');?>

<input type="hidden" name="user_id" value="<?php echo $user->uid; ?>"/> 

<div class="row-fluid"> 
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Nombre de usuario:</label>
            
            <div class="controls">
                <input type="text" name="username" value="<?php echo $user->name; ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Email:</label>
            
            <div class="controls">
                <input type="email" name="email" value="<?php echo $user->mail; ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Contraseña:</label>
            
            <div class="controls">
                <input type="password" name="password" value="" size="70" />
            </div>
        </div>
    </div>
</div>    
<div class="row-fluid"> 
    <h2>Activar/Bloquear usuario:</h2>
    <div class="span12">
        <div class="control-group">            
            <input type="checkbox" name="active" value="1" <?php echo $user->active?"checked":"";?>> 
            Deseo que el usuario este activo<br>                    
        </div>
    </div>    
</div>
<div class="row-fluid"> 
    <h2>Asignar a grupos</h2>
    <?php foreach($user->roles as $role):?>    
        <?php $seleccionado[] = $role->name;?>
        <div class="span2">
        <div class="control-group">            
            <input type="checkbox" name="group[]" value="<?php echo $role->rid;?>" checked>  <?php echo $role->name?><br>
            <p>"<?php echo $role->description?>"</p>            
        </div>
        </div>    
    <?php endforeach;?>
    <?php foreach($roles as $role):?>
    <?php if(!in_array($role->name, $seleccionado)):?>
        <div class="span2">
        <div class="control-group">            
            <input type="checkbox" name="group[]" value="<?php echo $role->rid;?>">  <?php echo $role->name?><br>
            <p>"<?php echo $role->description?>"</p>            
        </div>
        </div>
    <?php endif;?>
    <?php endforeach;?>
</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Editar usuario');?>
    <?php echo anchor('users/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>