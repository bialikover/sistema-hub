<div class="fixed">
<div id="infoMessage"><?php echo $message;?></div>
<div class="row-fluid">
  <div class="span6 offset3">
    <div class="auth-box">
    <h1>Registrate en TelmexHub</h1>  


<?php echo form_open("auth/signup");?>
    <input type="text" name="username" id="username" placeholder="Nombre de usuario"/>
    <input type="text" name="email" id="email" placeholder="Correo electrónico"/>
    <input type="password" name="password" id="password" placeholder="Contraseña" />    
  <div id="actions">
    <div id="remember" class="span6">
    <p>
      <?php echo form_checkbox('accept', '1', FALSE, 'id="remember"');?>
      <label for="remember">Acepto los terminos de servicio:</label>
    </p>
    </div>
    <div id="submit" class="span6">
      <?php echo form_submit('submit', 'Crear cuenta');?>
    </div>
  </div>
  <div class="facebook-login">    
    <a href= "<?php echo base_url('index.php/api/session/facebook'); ?>"><p>Regístrate con Facebook</p></a>    
  </div>
  <div class="actionlinks">
    <a href= "<?php echo base_url('index.php/auth/login'); ?>">¿Ya estas registrado? Inicia sesión</a>    
  </div>
        
<?php echo form_close();?>

  
  </div>
</div>
</div>
</div>

<script>
$('#username').tooltip({'trigger':'focus', 'title': 'Se permite utilizar espacios; los signos de puntuación no están permitidos a excepción de puntos, guiones y guiones bajos'});
$('#password').tooltip({'trigger':'focus', 'title': 'Password tooltip'});
$('#password').tooltip({'trigger':'focus', 'title': 'Password tooltip'});
</script>
