<?php if ($this->session->flashdata('success')):?>
         <div class="alert alert-success">           
             <h4>Super bien!</h4>
           <?php echo $this->session->flashdata('success');?>
         </div>
<?php endif; ?>

<h1>usuarios</h1>
<?php echo form_open('users/search', array('class'=>"navbar-search pull-left"));?>
  <input type="text" class="search-query" name="criteria" placeholder="Buscar usuarios">
<?php echo form_close();?>
<?php echo anchor("users/create", "Crear un user", array('class' => 'btn btn-primary pull-right'));?>
<?php if($users):?>
<table class="table table-hover">
<thead>
    <tr>
      <th>uid</th>
      <th>Nombre de usuario: </th>
      <th>Email</th>      
      <th>Fecha de Registro</th>      
      <th>Ultimo inicio de sesión</th>      
      <th>¿Esta Activo?</th>
      <th>Rol de usuario</th>
      <th>Acciones</th>            
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $user) :?>
      <tr>
        <td><?php echo $user->uid;?></td>
        <td><?php echo $user->name;?></td>
        <td><?php echo $user->mail;?></td>
        <td><?php echo date("d-M-Y H:i", $user->created_on);?></td>      
		    <td><?php echo date("d-M-Y H:i", $user->last_login);?></td>
		    <td><?php echo $user->active?"si":"no";?></td>		    
		    <td>
          <ul>
            <?php foreach($user->roles as $role):?>
            <li>
            <?php echo $role->name;?>
            </li>
            <?php endforeach;?>
          </ul>
        </td>
        <td>          
          <?php echo anchor("users/edit/".$user->uid, "Editar", array('class' => 'btn btn-mini'));?>
          <?php echo anchor("users/destroy/".$user->uid, "Borrar", array('class' => 'btn btn-mini btn-danger'));?>          
        </td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php endif;?>