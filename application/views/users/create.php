<h1>Nuevo usuario</h1><hr>

<?php if (isset($error)):?>
    <div class="alert alert-notice">           
        <h4>Muy mal!</h4>
        <?php echo $error;?>
     </div>    
<?php endif;?>
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('users/save');?>

<div class="row-fluid"> 
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Nombre de usuario:</label>
            
            <div class="controls">
                <input type="text" name="username" value="<?php echo set_value('username'); ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Email:</label>
            
            <div class="controls">
                <input type="email" name="email" value="<?php echo set_value('email'); ?>" size="70" />
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">     
            <label class="control-label">Contraseña:</label>
            
            <div class="controls">
                <input type="password" name="password" value="<?php echo set_value('password'); ?>" size="70" />
            </div>
        </div>
    </div>
</div>    

<div class="row-fluid"> 
    <h2>Asignar a grupos</h2>
    <?php foreach($roles as $role):?>
    <div class="span2">
        <div class="control-group">            
            <input type="checkbox" name="group[]" value="<?php echo $role->rid;?>">  <?php echo $role->name?><br>
            <p>"<?php echo $role->description?>"</p>            
        </div>
    </div>
    <?php endforeach;?>
</div>
<div class="form-actions">
    <div class="pull-right">
    <?php echo form_submit(array('class' => 'btn btn-primary'), 'Crear usuario');?>
    <?php echo anchor('users/index', 'Cancelar', array('class' => 'btn btn-danger'));?>
    </div>
</div>
<?php echo form_close();?>