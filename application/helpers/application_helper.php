<?php 

/* Acepta un arrego de un número indefinido de elementos. Si no se pasan argumentos, sólo se muestra TelmexHub. */
/* Ejemplo: pagetitle(array('Perfumes', 'Florecitas Estrella')) devuelve 'TelmexHub :: Eventos :: Mama dig' */
function pagetitle($breadcrumb = array()) {
        $pagetitle = 'TelmexHub';
        $separator = ' | ';
        
        // Si no es arreglo, forzar que sea arreglo
        if ( !is_array($breadcrumb) )
                $breadcrumb = array($breadcrumb);
        
        // Validar que haya elementos, sino fallback a sólo título
        if ( count($breadcrumb) < 1 )
                return $pagetitle;
        
        // Finalmente concatenar elementos y devolver título
        $pagetitle .= ' ' . $separator . ' ' . $breadcrumb[0];
        unset($breadcrumb[0]);
        
        foreach ( $breadcrumb as $i )
                $pagetitle .= ' ' . $separator . ' ' . $i;
        
        return $pagetitle;
}

function current_user(){      
        $CI =& get_instance();  
        $user = $CI->session->userdata('name');     

        if($user){
          return substr($user,0,10);
        }
        else{
          return false;
        }
}
function is_attending($user_id, $event){
        $CI =& get_instance();  
        $CI->load->model('rsvp');
        return $CI->rsvp->is_attending($user_id, $event);
}

function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
}

function user_image(){
  $CI =& get_instance();   
  $CI->load->model('profile');
  $user_id = $CI->session->userdata('user_id');
  $profile = $CI->profile->find_by_user_id($user_id);  
  if(!empty($profile)){
    return $profile->picture_url;     
  }
  else{
    return "http://placekitten.com/25/25";
  }






}