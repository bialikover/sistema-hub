<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profiles extends MY_Controller {

	var $before_filter = array();
	function __construct()
	{		
		parent::__construct();       
		$this->load->model('profile');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user',
    		'except' => array('index', 'show'),
  		);
  		$this->before_filter[] = array(
    		'action' => '_has_profile',
    		'only' => array('save', 'create'),
  		);
  		$this->before_filter[] = array(
    		'action' => '_has_privilegies',
    		'only' => array('admin_create', 'admin_edit', 'admin_update'),
  		);
  		$this->load->spark('oauth2/0.4.0');  		
		$this->load->model("facebook");
	}

	public function index()
	{
	}

	public function show()
	{	
		$user_id = $this->uri->segment(2);
		$data['profile']  = $this->profile->find_by_user_id($user_id);		
		if($data['profile']){
			//$data['activity'] = $this->
			$data['pagetitle'] = $data['profile']->name;		
			$yield = $this->load->view('profiles/show', $data, true);
        	$this->load->view("layouts/application", array('yield' => $yield));	
		}		
        else{
        	redirect('404');
        }
		
	}

	public function create()
	{			
		$user_id = $this->session->userdata('user_id');
		$user = $this->user->find($user_id);
		if($user->fbid){
			$data['fbInfo'] = $this->facebook->getfbInfo($user->uid);
		}	
		$data['pagetitle'] = 'Completa tu perfil';		
		$yield = $this->load->view('profiles/create_fb', $data, true);
        $this->load->view("layouts/application", array('yield' => $yield));	
	}



	public function admin_create()
	{			
		$user_id = $this->uri->segment(3);
		$user = $this->user->find($user_id);	
		$data['pagetitle'] = 'Crear perfil';		
		$yield = $this->load->view('profiles/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}


	public function admin_edit()
	{	
		$user_id = $this->session->userdata('user_id');
		$profile = $this->profile->find_by_user_id($user_id);
		if(!empty($profile)){
			$data['profile'] = $profile;
			$data['pagetitle'] = 'Editar perfil';			
			$yield = $this->load->view('profiles/edit', $data, true);
        	$this->load->view("layouts/application", array('yield' => $yield));	
		}
		else{
			$this->session->set_flashdata('success', "Parece que no tienes perfil");
			redirect("profiles");
		}
		
	}



	public function edit()
	{	
		$user_id = $this->session->userdata('user_id');
		$profile = $this->profile->find_by_user_id($user_id);
		if(!empty($profile)){
			$data['profile'] = $profile;
			$data['pagetitle'] = 'Editar perfil';			
			$yield = $this->load->view('profiles/edit', $data, true);
        	$this->load->view("layouts/application", array('yield' => $yield));	
		}
		else{
			$this->session->set_flashdata('success', "Parece que no tienes perfil");
			redirect("profiles");
		}
		
	}
	//commit the new object
	public function save()
	{
		$params = $this->input->post();	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
		$this->form_validation->set_rules('middle_name', 'Apellido Materno', 'required');
		$this->form_validation->set_rules('last_name', 'Apellido Paterno', 'required');
		$this->form_validation->set_rules('birthday', 'Fecha de Nacimiento', 'required');
		$this->form_validation->set_rules('gender', 'Genero', 'required');
		$this->form_validation->set_rules('education', 'Nivel de estudios', 'required');
		$this->form_validation->set_rules('telephone', "Telefono", "numeric");
        $this->form_validation->set_rules('address', 	"Dirección");
        $this->form_validation->set_rules('city', 	"Ciudad");
        $this->form_validation->set_rules('country', 	"Pais");
        $this->form_validation->set_rules('skill', 	"Especialidad");
        $this->form_validation->set_rules('interested_in', 	"Intereses");
        $this->form_validation->set_rules('likes', 'Likes');
        $this->form_validation->set_rules('thumbnail', 'Thumbnail');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('first_name',		$this->input->post("first_name"));
            $this->session->set_flashdata('middle_name',	$this->input->post("middle_name"));
            $this->session->set_flashdata('last_name', 	$this->input->post("last_name"));
            $this->session->set_flashdata('birthday',$this->input->post("birthday"));
            $this->session->set_flashdata('gender', 	$this->input->post("gender"));
            $this->session->set_flashdata('education', 		$this->input->post("education"));
            $this->session->set_flashdata('skill', 	$this->input->post("skill"));
            $this->session->set_flashdata('telephone', 	$this->input->post("telephone"));
            $this->session->set_flashdata('address', 	$this->input->post("address"));
            $this->session->set_flashdata('city', 	$this->input->post("city"));
            $this->session->set_flashdata('country', 	$this->input->post("country"));
            $this->session->set_flashdata('interested_in', 	$this->input->post("interested_in"));            
            $this->session->set_flashdata('likes', $this->input->post("likes"));
            $this->session->set_flashdata('thumbnail', $this->input->post("thumbnail"));            
            $data['pagetitle'] = 'Completa tu perfil';			
			$yield = $this->load->view('profiles/create_fb', $data, true);
        	$this->load->view("layouts/application", array('yield' => $yield));				
		}		
		else{	

			if($params['thumbnail']){
				$params['picture_url'] = $params['thumbnail'];
				unset($params['thumbnail']);
				$params['uid'] = $this->session->userdata('user_id');
            	$profile = $this->profile->create($params);
				if(!empty($profile)){
					$this->session->set_flashdata('success', "completaste tu perfil");

					// i need to redirect it to the show event  with userdata session.
					redirect("events");				
				}
				else{
					var_dump($profile);
					die("noooooo!!");
				}
			}
			else{
			$image = $this->_upload_image($this->session->userdata('user_id'));
			if(isset($image['success'])){
            	$params['picture_url'] = $image['success']['file_name'];
            	$params['uid'] = $this->session->userdata('user_id');
            	$profile = $this->profile->create($params);
				if(!empty($profile)){
					$this->session->set_flashdata('success', "completaste tu perfil");

					// i need to redirect it to the show event  with userdata session.
					redirect("events");				
				}
				else{
					var_dump($profile);
					die("noooooo!!");
				}                
            }
            else{
				$this->session->set_flashdata('first_name',		$this->input->post("first_name"));
            	$this->session->set_flashdata('middle_name',	$this->input->post("middle_name"));
            	$this->session->set_flashdata('last_name', 	$this->input->post("last_name"));
            	$this->session->set_flashdata('birthday',$this->input->post("birthday"));
            	$this->session->set_flashdata('gender', 	$this->input->post("gender"));
            	$this->session->set_flashdata('education', 		$this->input->post("education"));
            	$this->session->set_flashdata('skill', 	$this->input->post("skill"));
            	$this->session->set_flashdata('telephone', 	$this->input->post("telephone"));
            	$this->session->set_flashdata('address', 	$this->input->post("address"));
            	$this->session->set_flashdata('city', 	$this->input->post("city"));
            	$this->session->set_flashdata('country', 	$this->input->post("country"));            	
            	$this->session->set_flashdata('interested_in', 	$this->input->post("interested_in"));
            	$this->session->set_flashdata('likes', 	$this->input->post("likes"));
            	$this->session->set_flashdata('thumbnail', 	$this->input->post("thumbnail"));            
            	$data['error']= $image["error"];
            	$data['pagetitle'] = 'Completa tu perfil';			
				$yield = $this->load->view('profiles/create_fb', $data, true);
        		$this->load->view("layouts/application", array('yield' => $yield));	
            }
            }			
			
		}	
	}


	public function update()
	{
		$user_id = $this->session->userdata('user_id');
		$params = $this->input->post();		
		$image = $this->_upload_image($user_id);

		//print_r($image);
		//die();
		
		if(isset($image['success'])){
        	$params['image_url'] = $image['success']['file_name'];         	         		
		}
		else if($image['error']==="<p>You did not select a file to upload.</p>"){	
		}
		else{
			$this->session->set_flashdata('error', $image["error"]);			
			redirect("profiles/edit");				
		}
		$profile = $this->profile->find_by_user_id($user_id);
		$params['profile_id'] = $profile->id;
		$profile = $this->profile->edit($params);
		$this->session->set_flashdata('success', "se actualizó tu perfil");
		redirect("events");	
	}

	public function destroy()
	{
	
	}

	private function _upload_image($user_id){
		$file_path = "./assets/users/pics/$user_id";
		if(!file_exists($file_path)){         
          $file_path = $this->_create_file_path($user_id);
        }
    	$config['upload_path'] = $file_path;
    	$config['allowed_types'] = 'gif|jpg|png';
    	$config['max_size'] = '1000';
    	$config['max_width']  = '1024';
    	$config['max_height']  = '768';
    	$this->load->library('upload', $config);
	  	if ( ! $this->upload->do_upload()){
	  		$result['error']= $this->upload->display_errors();
	        return $result;
	    }
	    else{
	    	$result['success'] = $this->upload->data();
	        return $result;
	    }
	}

	private function _create_file_path($user_id){        
        $path = "./assets/users/pics/".$user_id;
        mkdir($path, 0775);
        return $path;
    }


    protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

    protected function _has_profile(){    	
    	$user_id = $this->session->userdata('user_id');
    	$profile = $this->profile->find_by_user_id($user_id);
    	if (!empty($profile))
		{
			$this->session->set_flashdata("message", "ya tienes perfil");
  			redirect('events');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}
}

/* End of file profiles.php */
/* Location: ./application/controllers/profiles.php */