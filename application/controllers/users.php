<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->model('user');
		$this->load->model('role');
		$this->load->model('profile');		
		$this->before_filter[] = array(
    		'action' => '_authenticated_user'    		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies',
    		'except' => array('change_password')
  		);
	}

	public function index()
	{
		$users = $this->user->all();	
		$data['pagetitle'] = 'users';
      	$data['users'] = $users;
		$yield = $this->load->view('users/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo usuario';
		$data['roles'] = $this->role->all();
		$yield = $this->load->view('users/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{

		$user_id = $this->uri->segment(3);
		$user = $this->user->find($user_id);
		if(!empty($user)){
			$data['user'] = $user;
			$data['pagetitle'] = 'Editar user';
			$data['roles'] = $this->role->all();			
			$yield = $this->load->view('users/edit', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));	
		}
		else{
			$this->session->set_flashdata('success', "Ese user no existe");
			redirect("users");
		}				
	}
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Nombre de usuario', 'required|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]|valid_email');		
		$this->form_validation->set_rules('password', 'Contraseña', 'required');		
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('username',		$this->input->post("username"));
            $this->session->set_flashdata('email',	$this->input->post("email"));
            $this->session->set_flashdata('password', 	$this->input->post("password"));
            $data['pagetitle'] = 'Nuevo usuario';			
            $data['roles'] = $this->role->all();
			$yield = $this->load->view('users/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		
		else{	
			$username = $params['username'];
			$email = $params['email'];
			$password = $params['password'];
			$groups = $params['group'];
			if($this->ion_auth->register($username, $password, $email, $additional = "", $groups)){
				$this->session->set_flashdata('success', "se creó el usurio");
				redirect("users");
			}
			else{
            	$data['error']= "no se pudo crear el usuario";
            	$this->session->set_flashdata('username',		$this->input->post("username"));
            	$this->session->set_flashdata('email',	$this->input->post("email"));
            	$this->session->set_flashdata('password', 	$this->input->post("password"));
            	$data['pagetitle'] = 'Nuevo usuario';			
            	$data['roles'] = $this->role->all();
				$yield = $this->load->view('users/create', $data, true);
        		$this->load->view("layouts/backend", array('yield' => $yield));				
			}
		}	
	}

	public function update()
	{
		$params = $this->input->post();
		$user = $this->user->edit($params);
		$this->session->set_flashdata('success', "se actualizó el user");
		redirect("users");		
		      		
	}

	public function destroy()
	{
		$user_id = $this->uri->segment(3);
		$user = $this->user->delete($user_id);
		$this->session->set_flashdata('success', "Se ha borrado el user");
			redirect("users");

	}

	public function search(){
		$criteria = $this->input->post("criteria");
		$users = $this->user->search($criteria);
		if(!empty($users)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de usuarios';
      		$data['users'] = $users;
			$yield = $this->load->view('users/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("users");
		}

	}

	public function change_password(){		
		$params = $this->input->post();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');		
		$this->form_validation->set_rules('password', 'Contraseña', 'required');

		if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('password', 	$this->input->post("password"));							
			$this->load->view('users/edit_password');
		}
		else{	
			$this->user->edit_password($params);
			redirect("/");
		}
        
	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}
	
}

/* End of file users.php */
/* Location: ./application/controllers/users.php */