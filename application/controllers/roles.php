<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class roles extends MY_Controller {

	var $before_filter = array();
	function __construct()
	{		
		parent::__construct();       
		$this->load->model('role');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user'    		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies'
  		);

	}

	public function index()
	{
		$roles = $this->role->all();
		$data['pagetitle'] = 'roles de usuario';
      	$data['roles'] = $roles;
		$yield = $this->load->view('roles/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo rol de usuario';		
		$yield = $this->load->view('roles/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{	
		$role_id = $this->uri->segment(3);
		$data['role'] = $this->role->find($role_id);
		$data['pagetitle'] = 'Editar rol de usuario';			
		$yield = $this->load->view('roles/edit', $data, true);
       	$this->load->view("layouts/backend", array('yield' => $yield));	
	}
		
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripción');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));
            $this->session->set_flashdata('description',	$this->input->post("description"));            
            $data['pagetitle'] = 'Editar rol';			
			$yield = $this->load->view('roles/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{	
			$role = $this->role->create($params);
			if(!empty($role)){
				$this->session->set_flashdata('success', "se creó el rol de usuario");
				redirect("roles");				
			}
			else{
				var_dump($role);
				die("noooooo!!");
			}    			
		}	
	}


	public function update()
	{		
		$params = $this->input->post();		
		$role = $this->role->edit($params);
		$this->session->set_flashdata('success', "se actualizó el rol de usuario");
		redirect("roles");	
	}

	public function destroy()
	{
		$role_id = $this->uri->segment(3);
		$role = $this->role->delete($role_id);
		$this->session->set_flashdata('success', "Se ha borrado el rol de usuario");
		redirect("roles");
	}
	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}

	


  
}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */