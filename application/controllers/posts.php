<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();		
		$this->load->model('post');
		$this->load->model('category');
		$this->load->model('tag');
		$this->load->model('profile');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user', 
    		'except' => array('blog', 'lookup_by_hyphenated_name'),   		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies',
    		'except' => array('blog', 'lookup_by_hyphenated_name'),
  		);
	}


	public function index()
	{
		$posts = $this->post->all();
		$data['pagetitle'] = 'Blog';
      	$data['posts'] = $posts;      	
		$yield = $this->load->view('posts/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function blog(){
		$posts = $this->post->all();
		$data['pagetitle'] = 'Blog';
      	$data['posts'] = $posts;      	
		$yield = $this->load->view('pages/blog/blog', $data, true);
        $this->load->view("layouts/application", array('yield' => $yield));	
	}

	public function lookup_by_hyphenated_name(){
		$postslug = $this->uri->segment(2);
		$post = $this->post->find_by_slug($postslug);
		$data['pagetitle'] = 'Blog::'.$post->name;
      	$data['post'] = $post;      	
		$yield = $this->load->view('pages/blog/blog-single', $data, true);
        $this->load->view("layouts/application", array('yield' => $yield));	

	}

	public function show()
	{	
		$post_id = $this->uri->segment(3);	
		$post = $this->post->find($post_id);
		$data['post'] = $post;
		$data['title'] = $post->name;
		$yield = $this->load->view('posts/show', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo post';
		$data['categories']= $this->category->all();
		$data['profiles'] = $this->profile->prepare_all();
		$data['tags'] = $this->tag->all_for_post();
		$yield = $this->load->view('posts/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{
		$post_id = $this->uri->segment(3);
		$post = $this->post->find($post_id);
		if(!empty($post)){
			$data['post'] = $post;			
			$data['pagetitle'] = 'Editar post';
			$data['categories']= $this->category->all();
			$data['profiles'] = $this->profile->prepare_all();
			$data['tags'] = $this->tag->all_for_post();
			$yield = $this->load->view('posts/edit', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));	

		}
		else{
			$this->session->set_flashdata('success', "Ese post no existe");
			redirect("posts");
		}
		
		
	
	}
	//commit the new object
	public function save()
	{
		$params = $this->input->post();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('profile', 'Publicado por', 'required');		
		$this->form_validation->set_rules('excerpt', 'Extraxto', 'required');		
		$this->form_validation->set_rules('description', 'Descripción', 'required');
		$this->form_validation->set_rules('category_id', 'Categoria de post', 'required');
		$this->form_validation->set_rules('tags', 'Etiquetas');	
		$this->form_validation->set_rules('published_on', 'Mostrar con fecha de publicacion del dia', 'required');		
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));			
			$this->session->set_flashdata('profile', $this->input->post("profile"));
			$this->session->set_flashdata('excerpt', $this->input->post("excerpt"));				
			$this->session->set_flashdata('description', $this->input->post("description"));
			$this->session->set_flashdata('category_id', $this->input->post("category_id"));
			$this->session->set_flashdata('tags',  $this->input->post("tags"));
			$this->session->set_flashdata('published_on', $this->input->post("published_on"));

            $data['pagetitle'] = 'Nuevo post';
			$data['categories']= $this->category->all();
			$data['profiles'] = $this->profile->prepare_all();
			$data['tags'] = $this->tag->all_for_post();
			$yield = $this->load->view('posts/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{				
			$image = $this->_upload_image();
			if(isset($image['success'])){
            	$params['image_url'] = $image['success']['file_name'];            	
            	$params['slug'] = strtolower( preg_replace("![^a-z0-9]+!i", "-", $params['name']));
            	$params['profile_id'] = $this->_get_only_profile_id($params['profile']);
            	$post = $this->post->create($params);
				if(!empty($post)){
					$this->session->set_flashdata('success', "se creó el post");
					redirect("posts");				
				}
				else{
					var_dump($post);
					die("noooooo!!");
				}                
            }
            else{
				$this->session->set_flashdata('name',		$this->input->post("name"));			
				$this->session->set_flashdata('profile', $this->input->post("profile"));		
				$this->session->set_flashdata('excerpt', $this->input->post("excerpt"));		
				$this->session->set_flashdata('description', $this->input->post("description"));
				$this->session->set_flashdata('category_id', $this->input->post("category_id"));
				$this->session->set_flashdata('tags',  $this->input->post("tags"));
				$this->session->set_flashdata('published_on',  $this->input->post("published_on"));

            	$data['error']= $image["error"];
            	$data['pagetitle'] = 'Nuevo post';
				$data['categories']= $this->category->all();
				$data['profiles'] = $this->profile->prepare_all();
				$data['tags'] = $this->tag->all_for_post();
				$yield = $this->load->view('posts/create', $data, true);
        		$this->load->view("layouts/backend", array('yield' => $yield));	
            }			
			
		}	
	}

	public function update()
	{
		$params = $this->input->post();		
		$image = $this->_upload_image();

		//print_r($image);
		//die();
		
		if(isset($image['success'])){
        	$params['image_url'] = $image['success']['file_name'];         	         		
		}
		else if($image['error']==="<p>You did not select a file to upload.</p>"){	
		}
		else{
			$this->session->set_flashdata('error', $image["error"]);			
			redirect("posts/edit/".$params['post_id']);				
		}
		$post = $this->post->edit($params);
		$this->session->set_flashdata('success', "se actualizó el post");
		redirect("posts");		
		      		
	}

	public function destroy()
	{
		$post_id = $this->uri->segment(3);
		$post = $this->post->delete($post_id);
		$this->session->set_flashdata('success', "Se ha borrado el post");
			redirect("posts");

	}

	public function search(){
		$criteria = $this->input->post("criteria");
		$posts = $this->post->search($criteria);
		if(!empty($posts)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de posts';
      		$data['posts'] = $posts;
			$yield = $this->load->view('posts/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("posts");
		}

	}

	private function _get_only_profile_id($profile){
		$profile_raw = explode("-", $profile);
		$profile_id = end($profile_raw);
		return $profile_id;
	}

	private function _upload_image(){
		$file_path = "./assets/posts";
    	$config['upload_path'] = $file_path;
    	$config['allowed_types'] = 'gif|jpg|png|jpeg';
    	$config['max_size'] = '1000';
    	$config['max_width']  = '1024';
    	$config['max_height']  = '768';
    	$this->load->library('upload', $config);    	    	
	  	if ( ! $this->upload->do_upload()){
	  		$result['error']= $this->upload->display_errors();
	        return $result;
	    }
	    else{
	    	$result['success'] = $this->upload->data();
	        return $result;
	    }
	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}

}

/* End of file posts.php */
/* Location: ./application/controllers/posts.php */