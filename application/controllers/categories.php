<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MY_Controller {

	var $before_filter = array();
	function __construct()
	{		
		parent::__construct();       
		$this->load->model('category');
  		$this->before_filter[] = array(
    		'action' => '_authenticated_user'
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies'
  		);

	}

	public function index()
	{
		$categories = $this->category->all();
		$data['pagetitle'] = 'categorias';
      	$data['categories'] = $categories;
		$yield = $this->load->view('categories/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nueva Categoria';		
		$yield = $this->load->view('categories/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{	
		$category_id = $this->uri->segment(3);
		$data['category'] = $this->category->find($category_id);
		$data['pagetitle'] = 'Editar categoria';			
		$yield = $this->load->view('categories/edit', $data, true);
       	$this->load->view("layouts/backend", array('yield' => $yield));	
	}
		
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripción');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));
            $this->session->set_flashdata('description',	$this->input->post("description"));            
            $data['pagetitle'] = 'Editar categoria';			
			$yield = $this->load->view('categories/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{	
			$category = $this->category->create($params);
			if(!empty($category)){
				$this->session->set_flashdata('success', "se creó la categoria");
				redirect("categories");				
			}
			else{
				var_dump($category);
				die("noooooo!!");
			}    			
		}	
	}


	public function update()
	{		
		$params = $this->input->post();		
		$category = $this->category->edit($params);
		$this->session->set_flashdata('success', "se actualizó la categoria");
		redirect("categories");	
	}

	public function destroy()
	{
		$category_id = $this->uri->segment(3);
		$category = $this->category->delete($category_id);
		$this->session->set_flashdata('success', "Se ha borrado la categoria");
		redirect("categories");
	}


	public function search(){
		$criteria = $this->input->post("criteria");
		$categories = $this->category->search($criteria);
		if(!empty($categories)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de categorias';
      		$data['categories'] = $categories;
			$yield = $this->load->view('categories/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("categories");
		}

	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}


	


  
}

/* End of file profiles.php */
/* Location: ./application/controllers/profiles.php */