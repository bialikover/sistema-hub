<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rsvps extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();
  		$this->before_filter[] = array(
    		'action' => '_has_event'    		
  		);
  		$this->load->model('rsvp');
		$this->load->model('profile');
		$this->load->model('user');
	}
	public function update()
	{	

		$event_id = $this->input->post('event_id');
		$user_id = $this->session->userdata('user_id');
		if(!empty($user_id)){
			$profile = $this->profile->find_by_user_id($user_id);
			if(!empty($profile)){			
				$rsvp = $this->rsvp->save($profile->id, $event_id);
				$rsvp->profile_picture_url = $profile->picture_url;
				$user = $this->user->find($user_id);
				$rsvp->username = $user->name;
				$rsvp = json_encode($rsvp);
				echo $rsvp;			
			}
			else{
				//redirect to create profile
				echo '{Status:"you need to complete your profile"}';				
			}
		}
		else{
			echo '{Status:"You need to signin"}';
		}
	}

	protected function _has_event(){
		$event_id = $this->input->post("event_id");
		if(empty($event_id)){
			echo '{Status:"The event does not exist"}';
		}
	}

}

/* End of file rsvps.php */
/* Location: ./application/controllers/rsvps.php */