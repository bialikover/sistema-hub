<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends MY_Controller {

	var $before_filter = array();
	function __construct()
	{		
		parent::__construct();       
		$this->load->model('tag');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user'    		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies'
  		);

	}

	public function index()
	{
		$tags = $this->tag->all();
		$data['pagetitle'] = 'etiquetas';
      	$data['tags'] = $tags;
		$yield = $this->load->view('tags/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nueva etiqueta';		
		$yield = $this->load->view('tags/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{	
		$tag_id = $this->uri->segment(3);
		$data['tag'] = $this->tag->find($tag_id);
		$data['pagetitle'] = 'Editar etiqueta';			
		$yield = $this->load->view('tags/edit', $data, true);
       	$this->load->view("layouts/backend", array('yield' => $yield));	
	}
		
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');		
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));            
            $data['pagetitle'] = 'Editar etiqueta';			
			$yield = $this->load->view('tags/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{	
			$tag = $this->tag->create($params);
			if(!empty($tag)){
				$this->session->set_flashdata('success', "se creó la etiqueta");
				redirect("tags");				
			}
			else{
				var_dump($tag);
				die("noooooo!!");
			}    			
		}	
	}


	public function update()
	{		
		$params = $this->input->post();		
		$tag = $this->tag->edit($params);
		$this->session->set_flashdata('success', "se actualizó la etiqueta");
		redirect("tags");	
	}

	public function destroy()
	{
		$tag_id = $this->uri->segment(3);
		$tag = $this->tag->delete($tag_id);
		$this->session->set_flashdata('success', "Se ha borrado la etiqueta");
		redirect("tags");
	}

	public function search(){
		$tags = $this->input->post("criteria");
		$tags = $this->tag->search($tags);
		if(!empty($tags)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de eventos';
      		$data['tags'] = $tags;
			$yield = $this->load->view('tags/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("tags");
		}

	}


	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}
	


  
}

/* End of file profiles.php */
/* Location: ./application/controllers/profiles.php */