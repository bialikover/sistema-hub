<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Types extends MY_Controller {

	var $before_filter = array();
	function __construct()
	{		
		parent::__construct();       
		$this->load->model('type');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user'    		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies'
  		);
	}

	public function index()
	{
		$types = $this->type->all();
		$data['pagetitle'] = 'tipo de eventos';
      	$data['types'] = $types;
		$yield = $this->load->view('types/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo tipo de evento';		
		$yield = $this->load->view('types/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{	
		$type_id = $this->uri->segment(3);
		$data['type'] = $this->type->find($type_id);
		$data['pagetitle'] = 'Editar tipo de evento';			
		$yield = $this->load->view('types/edit', $data, true);
       	$this->load->view("layouts/backend", array('yield' => $yield));	
	}
		
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();		
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripción');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));
            $this->session->set_flashdata('description',	$this->input->post("description"));            
            $data['pagetitle'] = 'Editar tipo de evento';			
			$yield = $this->load->view('types/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{	
			$type = $this->type->create($params);
			if(!empty($type)){
				$this->session->set_flashdata('success', "se creó el tipo de evento");
				redirect("types");				
			}
			else{
				var_dump($type);
				die("noooooo!!");
			}    			
		}	
	}


	public function update()
	{		
		$params = $this->input->post();		
		$type = $this->type->edit($params);
		$this->session->set_flashdata('success', "se actualizó el tipo de evento");
		redirect("types");	
	}

	public function destroy()
	{
		$type_id = $this->uri->segment(3);
		$type = $this->type->delete($type_id);
		$this->session->set_flashdata('success', "Se ha borrado el tipo de evento");
		redirect("types");
	}


	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}

	


  
}

/* End of file profiles.php */
/* Location: ./application/controllers/profiles.php */