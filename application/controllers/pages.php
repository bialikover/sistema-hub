<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {


	function __construct()
	{
		
		parent::__construct();
		//Load libraries && helpers		
		//->load->library('form_validation');
		//->load->helper('url');
		//->load->database();
		$this->load->model('profile');
		$this->load->model('banner');

	}
	public function index()
	{		
		if ($this->ion_auth->logged_in())
		{
  			
				$user_id = $this->session->userdata('user_id');
				$has_profile = $this->profile->find_by_user_id($user_id);
				$session_url = $this->session->userdata('session_url');			
				if(empty($has_profile)){
						redirect("profiles/create");
				}								
			
		}
		$data['title'] = "Telmex Hub";
		$data['banners'] = $this->banner->active();
		$yield = $this->load->view('pages/index', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}

	public function tv (){
		$data['title'] = "Tv";
		$yield = $this->load->view('pages/tv', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}
	public function place (){
		$data['title'] = "Ubicación";
		$yield = $this->load->view('pages/place', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}
	public function about (){
		$data['title'] = "Acerca de TelmexHub";
		$yield = $this->load->view('pages/about', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}

	public function community (){
		$data['title'] = "Reglamento para comunidades";
		$yield = $this->load->view('pages/community', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}
	public function intern (){
		$data['title'] = "Reglamento Interno";
		$yield = $this->load->view('pages/intern', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */