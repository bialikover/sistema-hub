<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();
		//Load libraries && helpers		
		//->load->library('form_validation');
		//->load->helper('url');
		//->load->database();
		$this->load->model('event');
		$this->load->model('rsvp');
		$this->load->model('type');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user',
    		'except' => array('calendar', 'show_single', 'onday')
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies',
    		'except' => array('calendar', 'show_single', 'onday')
  		);
	}

	public function index()
	{
		$events = $this->event->all();
		$data['pagetitle'] = 'Eventos';
      	$data['events'] = $events;
		$yield = $this->load->view('events/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function calendar(){

		$prefs = array (
               'start_day'    => 'monday',
               'month_type'   => 'long',
               'day_type'     => 'long',
               'show_next_prev'  => TRUE,
               'next_prev_url'   => base_url().'index.php/events/calendar/'
             );
		$prefs['template'] = '
		{table_open}<table class="calendar">{/table_open}
    	{week_day_cell}<th class="day_header">{week_day}</th>{/week_day_cell}
    	{cal_cell_content}<span class="day_listing">{day}</span><div class="event-single"><a href="#Modal"  data-toggle="modal">{content}</a></div>{/cal_cell_content}
    	{cal_cell_content_today}<div class="today"><span class="day_listing">{day}</span><div class="event-single"><a href="#Modal"  data-toggle="modal">{content}</a></div></div>{/cal_cell_content_today}
    	{cal_cell_no_content}<span class="day_listing">{day}</span>&nbsp;{/cal_cell_no_content}
    	{cal_cell_no_content_today}<div class="today"><span class="day_listing">{day}</span></div>{/cal_cell_no_content_today}
		'; 
		$this->load->library('calendar', $prefs);
		if($this->uri->segment(3) && $this->uri->segment(4)) {
			$year = $this->uri->segment(3);
			$month = $this->uri->segment(4);
			$events = $this->event->find_by_year_and_month($year, $month);
			$data['calendar'] = $this->calendar->generate($year, $month, $events);
		}
		else{
			$events = $this->event->find_this_month(time());	
			$data['calendar'] = $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4), $events);
		}
		
		
		$yield= $this->load->view('pages/calendar/calendar', $data, true);
		$this->load->view("layouts/application", array('yield' => $yield));	
	}

	public function onday(){
		$day = $this->uri->segment(3);
		$data['events'] = $this->event->find_by_day($day);	
		return  $this->load->view('pages/calendar/onday', $data);

	}


	public function show_single(){

	}

	public function show()
	{	
		$event_id = $this->uri->segment(3);	
		$event = $this->event->find($event_id);
		$data['event'] = $event;
		$data['title'] = $event->name;
		$yield = $this->load->view('events/show', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo evento';
		$data['types']= $this->type->all();
		$yield = $this->load->view('events/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{
		$event_id = $this->uri->segment(3);
		$event = $this->event->find($event_id);
		if(!empty($event)){
			$data['event'] = $event;
			$data['pagetitle'] = 'Editar evento';
			$data['types']= $this->type->all();
			$yield = $this->load->view('events/edit', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));	

		}
		else{
			$this->session->set_flashdata('success', "Ese evento no existe");
			redirect("events");
		}
		
		
	
	}
	//commit the new object
	public function save()
	{
		$params = $this->input->post();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('date_start', 'Fecha de Inicio', 'required');
		$this->form_validation->set_rules('date_end', 'Fecha de Termino', 'required');
		$this->form_validation->set_rules('description', 'Descripción', 'required');
		$this->form_validation->set_rules('type_id', 'Tipo de evento', 'required');
		$this->form_validation->set_rules('coach', 'Instructor', 'required');
		$this->form_validation->set_rules('workspace', 'Espacio de trabajo', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));
            $this->session->set_flashdata('date_start',	$this->input->post("date_start"));
            $this->session->set_flashdata('date_end', 	$this->input->post("date_end"));
            $this->session->set_flashdata('description',$this->input->post("description"));
            $this->session->set_flashdata('type_id', 	$this->input->post("type_id"));
            $this->session->set_flashdata('coach', 		$this->input->post("coach"));
            $this->session->set_flashdata('workspace', 	$this->input->post("workspace"));
            $data['pagetitle'] = 'Nuevo evento';
			$data['types']= $this->type->all();
			$yield = $this->load->view('events/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		else{	
			$image = $this->_upload_image();
			if(isset($image['success'])){
            	$params['image_url'] = $image['success']['file_name'];
            	$event = $this->event->create($params);
				if(!empty($event)){
					$this->session->set_flashdata('success', "se creó el evento");
					redirect("events");				
				}
				else{
					var_dump($event);
					die("noooooo!!");
				}                
            }
            else{
				$this->session->set_flashdata('name',		$this->input->post("name"));
            	$this->session->set_flashdata('date_start',	$this->input->post("date_start"));
            	$this->session->set_flashdata('date_end', 	$this->input->post("date_end"));
            	$this->session->set_flashdata('description',$this->input->post("description"));
            	$this->session->set_flashdata('type_id', 	$this->input->post("type_id"));
            	$this->session->set_flashdata('coach', 		$this->input->post("coach"));
            	$this->session->set_flashdata('workspace', 	$this->input->post("workspace"));

            	$data['error']= $image["error"];
            	$data['pagetitle'] = 'Nuevo evento';
				$data['types']= $this->type->all();
				$yield = $this->load->view('events/create', $data, true);
        		$this->load->view("layouts/backend", array('yield' => $yield));	
            }			
			
		}	
	}

	public function update()
	{
		$params = $this->input->post();		
		$image = $this->_upload_image();

		//print_r($image);
		//die();
		
		if(isset($image['success'])){
        	$params['image_url'] = $image['success']['file_name'];         	         		
		}
		else if($image['error']==="<p>You did not select a file to upload.</p>"){	
		}
		else{
			$this->session->set_flashdata('error', $image["error"]);			
			redirect("events/edit/".$params['event_id']);				
		}
		$event = $this->event->edit($params);
		$this->session->set_flashdata('success', "se actualizó el evento");
		redirect("events");		
		      		
	}

	public function destroy()
	{
		$event_id = $this->uri->segment(3);
		$event = $this->event->delete($event_id);
		$this->session->set_flashdata('success', "Se ha borrado el evento");
			redirect("events");

	}

	public function search(){
		$criteria = $this->input->post("criteria");
		$events = $this->event->search($criteria);
		if(!empty($events)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de eventos';
      		$data['events'] = $events;
			$yield = $this->load->view('events/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("events");
		}

	}


	public function rsvps(){
		$event_id = $this->uri->segment(3);
		$rsvps = $this->rsvp->find_by_event($event_id);
		var_dump($rsvps);
	}

	


	private function _upload_image(){
		$file_path = "./assets/events";
    	$config['upload_path'] = $file_path;
    	$config['allowed_types'] = 'gif|jpg|png';
    	$config['max_size'] = '1000';
    	$config['max_width']  = '1024';
    	$config['max_height']  = '768';
    	$this->load->library('upload', $config);
	  	if ( ! $this->upload->do_upload()){
	  		$result['error']= $this->upload->display_errors();
	        return $result;
	    }
	    else{
	    	$result['success'] = $this->upload->data();
	        return $result;
	    }
	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}

}

/* End of file events.php */
/* Location: ./application/controllers/events.php */