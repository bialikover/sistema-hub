<?php
class Api extends CI_Controller {

    function __construct()
    {
        
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('session');
        $this->load->model('user');
        $this->load->model('role');
        $this->load->model('profile');      
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->spark('oauth2/0.4.0');
    }

    public function session($provider) {        
       //si el proveedor pasado es foursquare
        if($provider == 'facebook')
        {
            $provider = $this -> oauth2 -> provider($provider, array(
            'id' => '311397385570136',
            'secret' => 'af0798ea531efcf3c2e06126b71af6e0'));
           //si el proveedor pasado es google
        }
        if (!$this -> input -> get('code')) {
            // By sending no options it'll come back here
            $url = $provider -> authorize();
 
            redirect($url);
        } else {
            try {
                // Have a go at creating an access token from the code
                $token = $provider -> access($this->input->get('code'));
 
                // Use this object to try and get some user details (username, full name, etc)
                $user = $provider->get_user_info($token); 

                $this->create_or_login($user, $token);
            } catch (OAuth2_Exception $e) {
                show_error('That didnt work: ' . $e);
            }
        }
    }


    private function create_or_login($fbuser, $token)
    {           
        $user = $this->user->find_by_email_or_username($fbuser['email'], $fbuser['nickname']);
        if(empty($user)){
            
            
            /*echo "Tengo que crear el usuario";
            var_dump($fbuser);            
            die();*/
            $user_id = $this->ion_auth->fbregister($fbuser, $token);
            $user = $this->user->find($user_id);                
            $this->ion_auth->fblogin($user);
            redirect("profiles/change_password");

        }
        else{
            //if it has already an account...
            if($user->fbid == null || $user->fbid == 0){                
                $data = array('fbid' => $fbuser['uid'],
                              'access_token'=> $token->access_token);
                $this->ion_auth->update($user->uid, $data);
                $user = $this->user->find($user->uid);
                //login user.
                if($this->ion_auth->fblogin($user)){
                    //redirect($this->session->userdata("redirect"));
                    if(strlen($user->pass) < 40){
                        redirect("profiles/change_password");
                    }                
                    redirect("/");
                }
                else{
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
                }
            }
            //si ya tiene cuenta con fb..
            else{ 
                if($this->ion_auth->fblogin($user)){
                    //redirect($this->session->userdata("redirect"));
                    redirect("/");
                }
                else{
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
                }
            }
        }
    }        
}
/*
*fin del controlador auth2.php*/