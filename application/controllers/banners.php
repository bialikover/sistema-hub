<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class banners extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();
		$this->load->model('banner');
		$this->load->model('profile');
		$this->before_filter[] = array(
    		'action' => '_authenticated_user'    		
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies'
  		);
	}

	public function index()
	{
		$banners = $this->banner->all();
		$data['pagetitle'] = 'banners';
      	$data['banners'] = $banners;
		$yield = $this->load->view('banners/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	public function create()
	{
		$data['pagetitle'] = 'Nuevo banner';
		$yield = $this->load->view('banners/create', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));	
	}

	public function edit()
	{
		$banner_id = $this->uri->segment(3);
		$banner = $this->banner->find($banner_id);
		if(!empty($banner)){
			$data['banner'] = $banner;
			$data['pagetitle'] = 'Editar banner';			
			$yield = $this->load->view('banners/edit', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));	
		}
		else{
			$this->session->set_flashdata('success', "Ese banner no existe");
			redirect("banners");
		}				
	}
	
	//commit the new object
	public function save()
	{
		$params = $this->input->post();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripcion', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');		
		$this->form_validation->set_rules('active', 'Activo');		
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('name',		$this->input->post("name"));
			$this->session->set_flashdata('description',		$this->input->post("description"));
            $this->session->set_flashdata('link',	$this->input->post("link"));
            $this->session->set_flashdata('active', 	$this->input->post("active"));
            $data['pagetitle'] = 'Nuevo banner';			
			$yield = $this->load->view('banners/create', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));				
		}		
		
		else{	
			$image = $this->_upload_image();
			if(isset($image['success'])){
            	$params['image_url'] = $image['success']['file_name'];
				$profile = $this->profile->find_by_user_id($this->session->userdata("user_id"));
            	$params['profile_id'] = $profile->id;
            	$banner = $this->banner->create($params);
				if(!empty($banner)){
					$this->session->set_flashdata('success', "se creó el banner");
					redirect("banners");				
				}
				else{
					var_dump($banner);
					die("noooooo!!");
				}                
            }
            else{
				$this->session->set_flashdata('name',		$this->input->post("name"));
				$this->session->set_flashdata('description',		$this->input->post("description"));
            	$this->session->set_flashdata('link',	$this->input->post("link"));
            	$this->session->set_flashdata('active', 	$this->input->post("active"));
            	$data['error']= $image["error"];
            	$data['pagetitle'] = 'Nuevo bannero';				
				$yield = $this->load->view('banners/create', $data, true);
        		$this->load->view("layouts/backend", array('yield' => $yield));	
            }			
			
		}	
	}

	public function update()
	{
		$params = $this->input->post();		
		$image = $this->_upload_image();

		//print_r($image);
		//die();
		
		if(isset($image['success'])){
        	$params['image_url'] = $image['success']['file_name'];         	         		
		}
		else if($image['error']==="<p>You did not select a file to upload.</p>"){	
		}
		else{
			$this->session->set_flashdata('error', $image["error"]);			
			redirect("banners/edit/".$params['banner_id']);				
		}
		$banner = $this->banner->edit($params);
		$this->session->set_flashdata('success', "se actualizó el banner");
		redirect("banners");		
		      		
	}

	public function destroy()
	{
		$banner_id = $this->uri->segment(3);
		$banner = $this->banner->delete($banner_id);
		$this->session->set_flashdata('success', "Se ha borrado el banner");
			redirect("banners");

	}

	public function search(){
		$criteria = $this->input->post("criteria");
		$banners = $this->banner->search($criteria);
		if(!empty($banners)){
			$this->session->set_flashdata('success', "Estos fueron los resultados de la busqueda");
			$data['pagetitle'] = 'Busqueda de banners';
      		$data['banners'] = $banners;
			$yield = $this->load->view('banners/index', $data, true);
        	$this->load->view("layouts/backend", array('yield' => $yield));
		}
		else{
			$this->session->set_flashdata('success', "No se encontraron resultados");
			redirect("banners");
		}

	}

	private function _upload_image(){
		$file_path = "./assets/banners";
    	$config['upload_path'] = $file_path;
    	$config['allowed_types'] = 'gif|jpg|png';
    	$config['max_size'] = '1000';
    	$config['max_width']  = '1024';
    	$config['max_height']  = '768';
    	$this->load->library('upload', $config);
	  	if ( ! $this->upload->do_upload()){
	  		$result['error']= $this->upload->display_errors();
	        return $result;
	    }
	    else{
	    	$result['success'] = $this->upload->data();
	        return $result;
	    }
	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}

}

/* End of file banners.php */
/* Location: ./application/controllers/banners.php */