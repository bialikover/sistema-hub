<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		
		parent::__construct();
		$this->before_filter[] = array(
    		'action' => '_authenticated_user',
    		'only' => array('index'),
  		);		
		$this->before_filter[] = array(
    		'action' => '_has_privilegies',
    		'only' => array('index'),
  		);
	}

	public function index()
	{	

		$data['pagetitle'] = 'Panel';      	
		$yield = $this->load->view('dashboard/index', $data, true);
        $this->load->view("layouts/backend", array('yield' => $yield));
	}

	protected function _authenticated_user(){
    	if (!$this->ion_auth->logged_in())
		{
  			//redirect them to the login page  			
  			redirect('auth/login');
 		}
    }

	protected function _has_privilegies(){
		$group = array('admin', 'team');
		if (!$this->ion_auth->in_group($group)){			
			redirect("/");
		}		
	}	
}

/* End of file model_name.php */
/* Location: ./application/controllers/model_name.php */