/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:	
	
	config.language = 'es';	 
	config.toolbar = 'blog';
	config.toolbar_blog =
    [
        ['Source'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','SelectAll','RemoveFormat'],
        '/',
        ['Bold','Italic','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink','Anchor'],
        ['Image','Table','SpecialChar'],
        '/',
        ['Styles','Format','FontSize'],
        ['TextColor'],
        ['Maximize', 'ShowBlocks']
    ];
    config.toolbar = 'event';
    config.toolbar_event =
    [
    	['Source'],
    	['Cut','Copy','Paste','PasteText','PasteFromWord'],
    	['Undo','Redo','-','SelectAll','RemoveFormat'],
    	['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ];
     
    autoGrow_bottomSpace = 300;
    config.autoGrow_minHeight = 300;
    config.autoGrow_maxHeight = 300;
    
	// config.uiColor = '#AADC6E';
};
