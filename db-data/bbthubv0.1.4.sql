-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2013 at 04:48 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bbthub`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_banners_profiles1_idx` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `profile_id`, `name`, `image_url`, `link`, `active`) VALUES
(1, 2, 'Las mejores vestidas según facebook', '2013goldenglobes_annehathaway001.jpg', 'http://mashable.com/2013/02/25/oscars-best-dressed-facebook/', 1),
(2, 2, 'prueba 2', 'codeigniter-lg.gif', 'http://ellislab.com/codeigniter/user-guide/database/active_record.html#update', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(140) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Tecnología', 'Publicaciones relacionadas con tecnología y dispositivos');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` int(11) NOT NULL,
  `date_end` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `coach` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `workspace` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_events_types1_idx` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `date_start`, `date_end`, `type_id`, `description`, `coach`, `workspace`, `image_url`, `slug`) VALUES
(1, 'Evento de prueba', 2013, 2013, 2, '<p>Un evento mas para probar que ya funcione...</p>\n<p>Un evento mas para probar que ya funcione...</p>\n', 'Rubén Guadalquivir', '2', 'POW-WOW-Art-Event-in-Hong-Kong-06.jpg', NULL),
(2, 'No xss', 2013, 2013, 1, '<p>&lt;script&gt;alert(&quot;xss&quot;);&lt;/script&gt;</p>', 'Prueba', '1', 'IMG_20130213_183954.jpg', NULL),
(3, 'Evento html', 2013, 2013, 3, '	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;"><b>Samples</b><br />\n	Use when creating examples of document designs for clients, students. [including Web pages]\n	<p style="font-family: inherit; font-style: inherit; margin-top: 1.5em; margin-bottom: 1.5em; padding: 0px; text-decoration: inherit;"> </p>\n	</li>\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;"><b>Type Specimen Sheets</b><br />\n	Use when creating type specimen sheets or examples from which you or clients choose fonts for a project.</li>\n</ul>\n\n<p><span style="font-family: Verdana; font-size: 12px; line-height: 18px;">You could cut and paste the first paragraph of this article over and over, or download one of the Lorem ipsum text files I created. The first is simply 3 variations of the same paragraph repeated over and over. The second is 10 unique paragraphs of varying length. Right click (PC) or click/hold (Mac) on the link then save the plain text file to your hard drive. Or click on the link and choose "Save" in your browser. See the links at the end of this article for more variations on the lorem ipsum placeholder text.</span></p>\n\n<ul style="font-family: Verdana; font-size: 12px; margin-top: 1.5em; margin-bottom: 1.5em; margin-left: 0px; text-decoration: inherit; position: relative; z-index: 0; line-height: 18px;">\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;">[<a href="http://desktoppub.about.com/library/weekly/lorem.txt" style="font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;">lorem.txt</a> - 40+ evenly sized paragraphs of placeholder text]</li>\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;">[<a href="http://desktoppub.about.com/library/weekly/lorem2.txt" style="font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;">lorem2.txt</a> - 10 paragraphs of mixed length placeholder text]</li>\n</ul>\n" class="textarea span12" rows="12" id="description" placeholder="Enter text ...">', 'Elizabeth ', '4', 'IMG_20130209_213131.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ip_address` (`ip_address`),
  KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `image_url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entries_categories1_idx` (`category_id`),
  KEY `fk_entries_profiles1_idx` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `profile_id`, `name`, `description`, `category_id`, `published`, `image_url`, `slug`, `published_on`) VALUES
(1, 2, 'David es gay', '<header class="article-header" style="color: rgb(85, 85, 85); font-size: 13px; line-height: 19px;">\n<div class="article-info" style="max-width: 625px; margin: 28px 28px 28px auto; -webkit-transition: margin 0.2s; width: auto;"><a class="byline" href="http://mashable.com/people/anniecolbert/" style="color: rgb(0, 0, 0); font-weight: bold; line-height: normal; font-family: Arial, sans-serif; font-size: 1.3rem; display: inline;">Annie Colbert</a><time class="date" datetime="Mon, 25 Feb 2013 22:50:49 +0000" style="font-size: 1.3rem; color: rgb(138, 140, 142); display: inline;">3 hours ago</time></div>\n</header>\n\n<section class="article-content" style="font-size: 1.5rem; max-width: 625px; margin-left: auto; margin-right: 28px; -webkit-transition: margin 0.2s; margin-top: 1em; width: auto; color: rgb(85, 85, 85);">\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">As the classic song so accurately goes, the Internet is&nbsp;<a href="http://www.youtube.com/watch?v=zi8VTeDHjcM" style="color: rgb(12, 116, 166);" target="_blank">made of cats</a>.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">This obsession with meow-mixing everything on the web extends to&nbsp;<a href="http://mashable.com/category/tumblr/" style="color: rgb(12, 116, 166);">Tumblr</a>, where clever folks have created entire pages dedicated to famous people&#39;s&nbsp;<a href="http://mashable.com/category/cats/" style="color: rgb(12, 116, 166);">feline</a>&nbsp;doppelg&auml;ngers. Ron Swanson, David Bowie and Skrillex all star in Tumblr pages with their kitty twins. When was the last time you saw those three names in a sentence together? Send a thank you note to the Internet.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">&nbsp;</p>\n\n<div class="see-also"><strong>SEE ALSO:&nbsp;<a data-crackerjax="#post-slider" href="http://mashable.com/2012/07/02/best-cat-memes-ever/" style="color: rgb(12, 116, 166);">15 Best Cat Memes Ever [MEOW]</a></strong></div>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">&nbsp;</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">Prance through the gallery above for nine of the finest &quot;Cats That Look Like ...&quot; Tumblr pages.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;"><em>Homepage image courtesy of Flickr,&nbsp;<a href="http://www.flickr.com/photos/meaganjean/" style="color: rgb(12, 116, 166);" target="_blank">meaganjean</a></em></p>\n</section>\n\n<footer class="article-topics" style="max-width: 625px; margin-left: auto; margin-right: 28px; -webkit-transition: margin 0.2s; font-size: 1.1rem; text-transform: uppercase; font-weight: bold; width: auto; color: rgb(85, 85, 85);">TOPICS:&nbsp;<a href="http://mashable.com/category/cats/" style="color: rgb(105, 160, 59); font-weight: normal;">CATS</a>,&nbsp;<a href="http://mashable.com/category/cute-animals/" style="color: rgb(105, 160, 59); font-weight: normal;">CUTE ANIMALS</a>,&nbsp;<a href="http://mashable.com/category/gallery/" style="color: rgb(105, 160, 59); font-weight: normal;">GALLERY</a>,&nbsp;<a href="http://mashable.com/category/pics/" style="color: rgb(105, 160, 59); font-weight: normal;">PICS</a>,&nbsp;<a href="http://mashable.com/category/tumblr/" style="color: rgb(105, 160, 59); font-weight: normal;">TUMBLR</a>,&nbsp;<a href="http://mashable.com/category/watercooler/" style="color: rgb(105, 160, 59); font-weight: normal;">WATERCOOLER</a></footer>\n', 1, 1, 'cat-that-looks-like-drake.jpg', 'david-es-gay', '2013-02-26 00:48:51'),
(3, 2, 'Prueba con tags', '<div style="color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; font-size: medium; line-height: normal; width: 684px; margin: auto;">\n<h2 style="margin-top: 0px; margin-bottom: 0px; margin-left: 85px;">&iquest; Qu&eacute; es un tag?</h2>\n\n<p style="margin-left: 85px;">Una introducci&oacute;n breve a los tags - o etiquetas - que conforman el lenguaje HTML.</p>\n</div>\n\n<div id="article_text" style="clear: left; width: 684px; margin: 1em auto; color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; line-height: normal;">\n<div id="article_tools" style="margin-left: 1.5em; margin-bottom: 1em; width: 171px; float: right; background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-btm.gif); background-position: 0px 100%; background-repeat: no-repeat no-repeat;">\n<div class="hdr" style="background-color: rgb(137, 160, 44); background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-top.gif); background-repeat: no-repeat no-repeat;">\n<h5 style="margin: 0px 10px; font-weight: normal; color: rgb(255, 255, 255); padding-top: 2px;">herramientas<br />\ndel art&iacute;culo</h5>\n</div>\n\n<div class="bdy" style="margin-bottom: 5px; background-color: rgb(236, 236, 236); background-image: url(http://web.uservers.net/ayuda/__web-partes/fondo-arttools-mid.gif); background-repeat: no-repeat no-repeat;">\n<ul style="margin-bottom: 0px; margin-left: 10px; padding-top: 25px; padding-bottom: 0.5em; list-style: none;">\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://web.uservers.net/ayuda/soluciones/paginas-y-html/-que-es-un-tag_Mjg.html?print" style="color: rgb(72, 72, 72);">versi&oacute;n para imprimir</a></li>\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://usvx.net/doc/SPKB-028" style="color: rgb(72, 72, 72);">URL corto al art&iacute;culo</a></li>\n</ul>\n</div>\n</div>\n\n<h2 class="udoc_title" style="font-size: 17px;">&nbsp;</h2>\n\n<p>HTML es un lenguaje para escribir p&aacute;ginas de hipertexto definido por tags.</p>\n\n<p>Los tags, a veces llamados &quot;etiquetas&quot; en espa&ntilde;ol, son los &quot;comandos&quot; que los programas navegadores leen e interpretan para armar y dar forma a las p&aacute;ginas de Internet.</p>\n\n<p>Todos los tags de HTML empiezan con el caracter &lt; y terminan con el caracter &gt;. Dentro de ellos va toda la informaci&oacute;n del tag, empezando por el nombre. Por ejemplo, el tag HTML (que probablemente sea el m&aacute;s b&aacute;sico de todos) se escribe de la siguiente manera:</p>\n<code class="udoc_programlisting">&lt;html&gt;</code>\n\n<p>Adem&aacute;s del nombre, dentro de &lt; y &gt; el tag puede recibir o requerir ciertos argumentos. Los argumentos brindan informaci&oacute;n acerca de la instancia del tag. Estos generalmente se escriben de la forma:</p>\n<code class="udoc_programlisting">&lt;nombreTag argumento1=&quot;valor1&quot; argumento2=&quot;valor2&quot; ...&gt;</code>\n\n<p>Por ejemplo, el tag&nbsp;<code class="udoc_programlisting">&lt;FONT&gt;</code>, que controla la tipograf&iacute;a o tipo de letra de las p&aacute;ginas, lleva los siguientes argumentos:</p>\n<code class="udoc_programlisting">&lt;FONT FACE=&quot;Arial&quot; SIZE=&quot;2&quot; COLOR=&quot;RED&quot;&gt;</code>\n\n<p>No hay necesidad de decirlo, pero FACE determin</p>\n</div>\n', 1, 1, 'Adding_Tags1.png', 'prueba-con-tags', '2013-02-26 00:00:26');

-- --------------------------------------------------------

--
-- Table structure for table `posts_tags`
--

CREATE TABLE `posts_tags` (
  `id` int(45) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entries_tags_entries1_idx` (`post_id`),
  KEY `fk_entries_tags_tags1_idx` (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 6),
(2, 3, 2),
(3, 3, 5),
(4, 3, 6),
(5, 3, 7),
(6, 3, 8),
(7, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` mediumint(8) unsigned NOT NULL,
  `first_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture_url` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `likes` text COLLATE utf8_unicode_ci,
  `interested_in` text COLLATE utf8_unicode_ci,
  `education` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skill` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_users1_idx` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `uid`, `first_name`, `middle_name`, `last_name`, `birthday`, `gender`, `picture_url`, `likes`, `interested_in`, `education`, `skill`, `telephone`, `address`, `city`, `country`) VALUES
(2, 2, 'Adán', 'González', 'Galván', '0000-00-00 00:00:00', 'female', 'me1.jpg', NULL, 'Ios, RoR', 'Universidad', 'Desarrollador web', '53259996', 'Armeria #36', 'México', 'México');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'team', 'TelmexHub Team'),
(3, 'hubbers', 'The Hubbers'),
(4, 'Auditoria', 'Un usuario para auditoria');

-- --------------------------------------------------------

--
-- Table structure for table `rsvps`
--

CREATE TABLE `rsvps` (
  `id` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `event_id` int(10) unsigned NOT NULL,
  `profile_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `attended` tinyint(1) DEFAULT NULL,
  `registered_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rsvps_events1_idx` (`event_id`),
  KEY `fk_rsvps_profiles1_idx` (`profile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'javascript'),
(2, 'erlang'),
(4, 'lol'),
(5, 'league of legends'),
(6, 'mashable'),
(7, 'nettuts+'),
(8, 'telmex'),
(9, 'telmexhub');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(140) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `description`) VALUES
(1, 'charla', 'Una charla'),
(2, 'congreso', NULL),
(3, 'taller', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `fbid` int(11) DEFAULT NULL,
  `access_token` varchar(200) DEFAULT NULL,
  `ip_address` varbinary(16) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fbid`, `access_token`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES
(1, NULL, NULL, NULL, 'administrator', '59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4', '9462e8eee0', 'admin@admin.com', NULL, NULL, NULL, NULL, 1268889823, 1268889823, 1, 'Admin', 'istrator'),
(2, NULL, NULL, NULL, 'bialikover', '69395a93cbe8a534cf10d06f376860622d951536', NULL, 'bialikoer@gmail.com', NULL, NULL, NULL, NULL, 1361470394, 1361935485, 1, NULL, NULL),
(3, NULL, NULL, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 'herball', 'cd86380ad0ad72c5daf6a94adf881c1cd13d090e', NULL, 'jose@telmexhub.org', '727d1d7b81832d985420e76faa43c68c971463ab', NULL, NULL, NULL, 1361906557, 1361906557, 0, NULL, NULL),
(4, NULL, NULL, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 'fernando', '2b584a03615de7500ebcdb1179eb967434993148', NULL, 'fernando@telmexhub.org', '587a7207cc55602c1376714dc141d48e164bb345', NULL, NULL, NULL, 1361908192, 1361908192, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE `users_roles` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `rid` mediumint(8) unsigned NOT NULL,
  `uid` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_roles_role1_idx` (`rid`),
  KEY `fk_users_roles_users1_idx` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `rid`, `uid`) VALUES
(1, 1, 1),
(4, 3, 3),
(5, 2, 4),
(6, 3, 4),
(31, 2, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `fk_banners_profiles1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `fk_events_types1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_entries_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entries_profiles1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD CONSTRAINT `fk_entries_tags_entries1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entries_tags_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `fk_profiles_users1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rsvps`
--
ALTER TABLE `rsvps`
  ADD CONSTRAINT `fk_rsvps_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rsvps_profiles1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `fk_users_roles_role1` FOREIGN KEY (`rid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_roles_users1` FOREIGN KEY (`uid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
