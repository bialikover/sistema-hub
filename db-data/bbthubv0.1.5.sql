SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `bbthub` ;
CREATE SCHEMA IF NOT EXISTS `bbthub` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `bbthub` ;

-- -----------------------------------------------------
-- Table `bbthub`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`users` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`users` (
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fbid` VARCHAR(100) NULL DEFAULT NULL ,
  `access_token` VARCHAR(200) NULL DEFAULT NULL ,
  `ip_address` VARCHAR(20) NULL DEFAULT NULL ,
  `name` VARCHAR(100) NOT NULL ,
  `pass` VARCHAR(40) NULL DEFAULT NULL ,
  `salt` VARCHAR(40) NULL DEFAULT NULL ,
  `mail` VARCHAR(100) NOT NULL ,
  `activation_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_time` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `remember_code` VARCHAR(40) NULL DEFAULT NULL ,
  `created_on` INT(11) UNSIGNED NOT NULL ,
  `last_login` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
  `first_name` VARCHAR(50) NULL DEFAULT NULL ,
  `last_name` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`uid`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`profiles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`profiles` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`profiles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `first_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `middle_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `last_name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `birthday` INT(11) NULL DEFAULT NULL ,
  `gender` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `picture_url` VARCHAR(450) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `likes` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `interested_in` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `education` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `skill` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `telephone` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `address` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `city` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `country` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_profiles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_profiles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`uid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`banners`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`banners` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`banners` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `profile_id` INT(11) NOT NULL ,
  `name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `image_url` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `link` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `active` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_banners_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_banners_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`categories` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` VARCHAR(140) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`types` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` VARCHAR(140) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`events` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`events` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `date_start` INT(11) NOT NULL ,
  `date_end` INT(11) NOT NULL ,
  `type_id` INT(11) NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `coach` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `workspace` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `image_url` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `slug` VARCHAR(60) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_events_types1_idx` (`type_id` ASC) ,
  CONSTRAINT `fk_events_types1`
    FOREIGN KEY (`type_id` )
    REFERENCES `bbthub`.`types` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`login_attempts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`login_attempts` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`login_attempts` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ip_address` VARBINARY(16) NOT NULL ,
  `login` VARCHAR(100) NOT NULL ,
  `time` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ip_address` (`ip_address` ASC) ,
  INDEX `login` (`login` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`migrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`migrations` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`migrations` (
  `version` INT(3) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`posts` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`posts` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `profile_id` INT(11) NOT NULL ,
  `name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `excerpt` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  `category_id` INT(11) NOT NULL ,
  `published` TINYINT(1) NOT NULL DEFAULT '0' ,
  `image_url` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `slug` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
  `published_on` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_categories1_idx` (`category_id` ASC) ,
  INDEX `fk_entries_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_entries_categories1`
    FOREIGN KEY (`category_id` )
    REFERENCES `bbthub`.`categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`tags` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`tags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`posts_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`posts_tags` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`posts_tags` (
  `id` INT(45) NOT NULL AUTO_INCREMENT ,
  `post_id` INT(10) UNSIGNED NOT NULL ,
  `tag_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_tags_entries1_idx` (`post_id` ASC) ,
  INDEX `fk_entries_tags_tags1_idx` (`tag_id` ASC) ,
  CONSTRAINT `fk_entries_tags_entries1`
    FOREIGN KEY (`post_id` )
    REFERENCES `bbthub`.`posts` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_tags_tags1`
    FOREIGN KEY (`tag_id` )
    REFERENCES `bbthub`.`tags` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`role` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`role` (
  `rid` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(20) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`rid`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`rsvps`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`rsvps` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`rsvps` (
  `id` INT(99) NOT NULL AUTO_INCREMENT,
  `event_id` INT(10) UNSIGNED NOT NULL ,
  `profile_id` INT(11) NOT NULL ,
  `status` TINYINT(1) NULL DEFAULT NULL ,
  `attended` TINYINT(1) NULL DEFAULT NULL ,
  `registered_time` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_rsvps_events1_idx` (`event_id` ASC) ,
  INDEX `fk_rsvps_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_rsvps_events1`
    FOREIGN KEY (`event_id` )
    REFERENCES `bbthub`.`events` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rsvps_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `bbthub`.`users_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`users_roles` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`users_roles` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_users_roles_role1_idx` (`rid` ASC) ,
  INDEX `fk_users_roles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_users_roles_role1`
    FOREIGN KEY (`rid` )
    REFERENCES `bbthub`.`role` (`rid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`uid` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

USE `bbthub` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
