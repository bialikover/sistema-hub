SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `bbthub` ;
CREATE SCHEMA IF NOT EXISTS `bbthub` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `bbthub` ;

-- -----------------------------------------------------
-- Table `bbthub`.`login_attempts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`login_attempts` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`login_attempts` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ip_address` VARBINARY(16) NOT NULL ,
  `login` VARCHAR(100) NOT NULL ,
  `time` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ip_address` (`ip_address` ASC) ,
  INDEX `login` (`login` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`migrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`migrations` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`migrations` (
  `version` INT(3) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`role` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`role` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(20) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`users` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`users` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `fbid` INT NULL ,
  `access_token` VARCHAR(200) NULL ,
  `ip_address` VARBINARY(16) NULL ,
  `username` VARCHAR(100) NOT NULL ,
  `password` VARCHAR(40) NULL ,
  `salt` VARCHAR(40) NULL DEFAULT NULL ,
  `email` VARCHAR(100) NOT NULL ,
  `activation_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_time` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `remember_code` VARCHAR(40) NULL DEFAULT NULL ,
  `created_on` INT(11) UNSIGNED NOT NULL ,
  `last_login` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
  `first_name` VARCHAR(50) NULL DEFAULT NULL ,
  `last_name` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`users_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`users_roles` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`users_roles` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_users_roles_role1_idx` (`rid` ASC) ,
  INDEX `fk_users_roles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_users_roles_role1`
    FOREIGN KEY (`rid` )
    REFERENCES `bbthub`.`role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`types` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`types` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(140) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`events` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`events` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  `date_start` INT NOT NULL ,
  `date_end` INT NOT NULL ,
  `type_id` INT NOT NULL ,
  `description` TEXT NOT NULL ,
  `coach` VARCHAR(100) NOT NULL ,
  `workspace` VARCHAR(45) NULL ,
  `image_url` VARCHAR(500) NULL ,
  `slug` VARCHAR(60) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_events_types1_idx` (`type_id` ASC) ,
  CONSTRAINT `fk_events_types1`
    FOREIGN KEY (`type_id` )
    REFERENCES `bbthub`.`types` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`profiles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`profiles` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`profiles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `first_name` VARCHAR(45) NULL ,
  `middle_name` VARCHAR(45) NULL ,
  `last_name` VARCHAR(45) NULL ,
  `birthday` DATETIME NULL ,
  `gender` VARCHAR(45) NULL ,
  `picture_url` VARCHAR(45) NULL ,
  `likes` TEXT NULL ,
  `interested_in` TEXT NULL ,
  `education` VARCHAR(45) NULL ,
  `skill` VARCHAR(45) NULL ,
  `telephone` VARCHAR(45) NULL ,
  `address` VARCHAR(45) NULL ,
  `city` VARCHAR(45) NULL ,
  `country` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_profiles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_profiles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`rsvps`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`rsvps` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`rsvps` (
  `id` VARCHAR(45) NOT NULL ,
  `event_id` INT UNSIGNED NOT NULL ,
  `profile_id` INT NOT NULL ,
  `status` TINYINT(1) NULL ,
  `attended` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_rsvps_events1_idx` (`event_id` ASC) ,
  INDEX `fk_rsvps_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_rsvps_events1`
    FOREIGN KEY (`event_id` )
    REFERENCES `bbthub`.`events` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rsvps_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`categories` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`categories` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(140) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`entries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`entries` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`entries` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `profile_id` INT NOT NULL ,
  `name` VARCHAR(100) NOT NULL ,
  `description` TEXT NOT NULL ,
  `category_id` INT NOT NULL ,
  `published` TINYINT(1) NOT NULL DEFAULT 0 ,
  `image_url` VARCHAR(500) NULL ,
  `slug` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_categories1_idx` (`category_id` ASC) ,
  INDEX `fk_entries_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_entries_categories1`
    FOREIGN KEY (`category_id` )
    REFERENCES `bbthub`.`categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`tags` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`tags` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`entries_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`entries_tags` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`entries_tags` (
  `id` VARCHAR(45) NOT NULL ,
  `entries_id` INT UNSIGNED NOT NULL ,
  `tags_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_tags_entries1_idx` (`entries_id` ASC) ,
  INDEX `fk_entries_tags_tags1_idx` (`tags_id` ASC) ,
  CONSTRAINT `fk_entries_tags_entries1`
    FOREIGN KEY (`entries_id` )
    REFERENCES `bbthub`.`entries` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_tags_tags1`
    FOREIGN KEY (`tags_id` )
    REFERENCES `bbthub`.`tags` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`banners`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`banners` ;

CREATE  TABLE IF NOT EXISTS `bbthub`.`banners` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `profile_id` INT NOT NULL ,
  `name` VARCHAR(100) NULL ,
  `image_url` VARCHAR(500) NOT NULL ,
  `link` VARCHAR(500) NULL ,
  `active` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_banners_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_banners_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `bbthub` ;

-- -----------------------------------------------------
-- Placeholder table for view `bbthub`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbthub`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `bbthub`.`view1`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `bbthub`.`view1` ;
DROP TABLE IF EXISTS `bbthub`.`view1`;
USE `bbthub`;
;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bbthub`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `bbthub`;
INSERT INTO `bbthub`.`role` (`id`, `name`, `description`) VALUES (1, 'admin', 'Administrator');
INSERT INTO `bbthub`.`role` (`id`, `name`, `description`) VALUES (2, 'team', 'TelmexHub Team');
INSERT INTO `bbthub`.`role` (`id`, `name`, `description`) VALUES (3, 'hubbers', 'The Hubbers');

COMMIT;

-- -----------------------------------------------------
-- Data for table `bbthub`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `bbthub`;
INSERT INTO `bbthub`.`users` (`id`, `fbid`, `access_token`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES (1, NULL, NULL, NULL, 'administrator', '59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4', '9462e8eee0', 'admin@admin.com', NULL, NULL, NULL, NULL, 1268889823, 1268889823, 1, 'Admin', 'istrator');

COMMIT;

-- -----------------------------------------------------
-- Data for table `bbthub`.`users_roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `bbthub`;
INSERT INTO `bbthub`.`users_roles` (`id`, `rid`, `uid`) VALUES (1, 1, 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `bbthub`.`types`
-- -----------------------------------------------------
START TRANSACTION;
USE `bbthub`;
INSERT INTO `bbthub`.`types` (`id`, `name`, `description`) VALUES (1, 'charla', NULL);
INSERT INTO `bbthub`.`types` (`id`, `name`, `description`) VALUES (2, 'congreso', NULL);
INSERT INTO `bbthub`.`types` (`id`, `name`, `description`) VALUES (3, 'taller', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `bbthub`.`events`
-- -----------------------------------------------------
START TRANSACTION;
USE `bbthub`;
INSERT INTO `bbthub`.`events` (`id`, `name`, `date_start`, `date_end`, `type_id`, `description`, `coach`, `workspace`, `image_url`, `slug`) VALUES (1, 'Evento de prueba', '2013-02-18 16:00:29', '2013-02-18 17:00:29', 2, '<p>Un evento mas para probar que ya funcione...</p>\n<p>Un evento mas para probar que ya funcione...</p>\n', 'Rubén Guadalquivir', '2', 'POW-WOW-Art-Event-in-Hong-Kong-06.jpg', NULL);
INSERT INTO `bbthub`.`events` (`id`, `name`, `date_start`, `date_end`, `type_id`, `description`, `coach`, `workspace`, `image_url`, `slug`) VALUES (2, 'No xss', '2013-02-19 19:28:15', '2013-02-19 20:00:15', 1, '<p>&lt;script&gt;alert(&quot;xss&quot;);&lt;/script&gt;</p>', 'Prueba', '1', 'IMG_20130213_183954.jpg', NULL);
INSERT INTO `bbthub`.`events` (`id`, `name`, `date_start`, `date_end`, `type_id`, `description`, `coach`, `workspace`, `image_url`, `slug`) VALUES (3, 'Evento html', '2013-02-15 02:00:22', '2013-02-15 06:00:22', 3, '	<li style=\"font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;\"><b>Samples</b><br />\n	Use when creating examples of document designs for clients, students. [including Web pages]\n	<p style=\"font-family: inherit; font-style: inherit; margin-top: 1.5em; margin-bottom: 1.5em; padding: 0px; text-decoration: inherit;\"> </p>\n	</li>\n	<li style=\"font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;\"><b>Type Specimen Sheets</b><br />\n	Use when creating type specimen sheets or examples from which you or clients choose fonts for a project.</li>\n</ul>\n\n<p><span style=\"font-family: Verdana; font-size: 12px; line-height: 18px;\">You could cut and paste the first paragraph of this article over and over, or download one of the Lorem ipsum text files I created. The first is simply 3 variations of the same paragraph repeated over and over. The second is 10 unique paragraphs of varying length. Right click (PC) or click/hold (Mac) on the link then save the plain text file to your hard drive. Or click on the link and choose \"Save\" in your browser. See the links at the end of this article for more variations on the lorem ipsum placeholder text.</span></p>\n\n<ul style=\"font-family: Verdana; font-size: 12px; margin-top: 1.5em; margin-bottom: 1.5em; margin-left: 0px; text-decoration: inherit; position: relative; z-index: 0; line-height: 18px;\">\n	<li style=\"font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;\">[<a href=\"http://desktoppub.about.com/library/weekly/lorem.txt\" style=\"font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;\">lorem.txt</a> - 40+ evenly sized paragraphs of placeholder text]</li>\n	<li style=\"font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;\">[<a href=\"http://desktoppub.about.com/library/weekly/lorem2.txt\" style=\"font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;\">lorem2.txt</a> - 10 paragraphs of mixed length placeholder text]</li>\n</ul>\n\" class=\"textarea span12\" rows=\"12\" id=\"description\" placeholder=\"Enter text ...\">', 'Elizabeth ', '4', 'IMG_20130209_213131.jpg', NULL);

COMMIT;
