--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'javascript'),
(2, 'erlang'),
(4, 'lol'),
(5, 'league of legends'),
(6, 'mashable'),
(7, 'nettuts+'),
(8, 'telmex'),
(9, 'telmexhub');

-- --------------------------------------------------------


--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Tecnología', 'Publicaciones relacionadas con tecnología y dispositivos');

-- --------------------------------------------------------

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `description`) VALUES
(1, 'charla', 'Una charla'),
(2, 'congreso', NULL),
(3, 'taller', NULL);

-- --------------------------------------------------------

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `fbid`, `access_token`, `ip_address`, `name`, `pass`, `salt`, `mail`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES
(1, NULL, NULL, NULL, 'administrator', '59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4', '9462e8eee0', 'admin@admin.com', NULL, NULL, NULL, NULL, 1268889823, 1268889823, 1, 'Admin', 'istrator'),
(2, NULL, NULL, NULL, 'bialikover', '70e57a4d218cad50caf1d872ace5d48dc23310c0', NULL, 'bialikoer@gmail.com', NULL, NULL, NULL, NULL, 1361470394, 1361837581, 1, NULL, NULL);

-- --------------------------------------------------------
--
-- Dumping data for table `role`
--

INSERT INTO `role` (`rid`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'team', 'TelmexHub Team'),
(3, 'hubbers', 'The Hubbers'),
(4, 'authenticated user','Usuario autenticado');

-- --------------------------------------------------------


--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`id`, `rid`, `uid`) VALUES
(1, 1, 1),
(3, 4, 2);



--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `uid`, `first_name`, `middle_name`, `last_name`, `birthday`, `gender`, `picture_url`, `likes`, `interested_in`, `education`, `skill`, `telephone`, `address`, `city`, `country`) VALUES
(2, 2, 'Adán', 'González', 'Galván', '0000-00-00 00:00:00', 'male', 'me1.jpg', NULL, 'Ios, RoR', 'Universidad', 'Desarrollador web', '53259996', 'Armeria #36', 'México', 'México');

-- --------------------------------------------------------



--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `profile_id`, `name`, `image_url`, `link`, `active`) VALUES
(1, 2, 'Las mejores vestidas según facebook', '2013goldenglobes_annehathaway001.jpg', 'http://mashable.com/2013/02/25/oscars-best-dressed-facebook/', 1),
(2, 2, 'prueba 2', 'codeigniter-lg.gif', 'http://ellislab.com/codeigniter/user-guide/database/active_record.html#update', 1);

-- --------------------------------------------------------

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `date_start`, `date_end`, `type_id`, `description`, `coach`, `workspace`, `image_url`, `slug`) VALUES
(1, 'Evento de prueba', 2013, 2013, 2, '<p>Un evento mas para probar que ya funcione...</p>\n<p>Un evento mas para probar que ya funcione...</p>\n', 'Rubén Guadalquivir', '2', 'POW-WOW-Art-Event-in-Hong-Kong-06.jpg', NULL),
(2, 'No xss', 2013, 2013, 1, '<p>&lt;script&gt;alert(&quot;xss&quot;);&lt;/script&gt;</p>', 'Prueba', '1', 'IMG_20130213_183954.jpg', NULL),
(3, 'Evento html', 2013, 2013, 3, '	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;"><b>Samples</b><br />\n	Use when creating examples of document designs for clients, students. [including Web pages]\n	<p style="font-family: inherit; font-style: inherit; margin-top: 1.5em; margin-bottom: 1.5em; padding: 0px; text-decoration: inherit;"> </p>\n	</li>\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;"><b>Type Specimen Sheets</b><br />\n	Use when creating type specimen sheets or examples from which you or clients choose fonts for a project.</li>\n</ul>\n\n<p><span style="font-family: Verdana; font-size: 12px; line-height: 18px;">You could cut and paste the first paragraph of this article over and over, or download one of the Lorem ipsum text files I created. The first is simply 3 variations of the same paragraph repeated over and over. The second is 10 unique paragraphs of varying length. Right click (PC) or click/hold (Mac) on the link then save the plain text file to your hard drive. Or click on the link and choose "Save" in your browser. See the links at the end of this article for more variations on the lorem ipsum placeholder text.</span></p>\n\n<ul style="font-family: Verdana; font-size: 12px; margin-top: 1.5em; margin-bottom: 1.5em; margin-left: 0px; text-decoration: inherit; position: relative; z-index: 0; line-height: 18px;">\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;">[<a href="http://desktoppub.about.com/library/weekly/lorem.txt" style="font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;">lorem.txt</a> - 40+ evenly sized paragraphs of placeholder text]</li>\n	<li style="font-family: inherit; font-style: inherit; margin: 0px 0px 0px 18px; padding: 0px; text-decoration: inherit; list-style-type: disc;">[<a href="http://desktoppub.about.com/library/weekly/lorem2.txt" style="font-family: inherit; font-style: inherit; margin: 0px; padding: 0px; text-decoration: underline; color: rgb(51, 102, 204); cursor: pointer;">lorem2.txt</a> - 10 paragraphs of mixed length placeholder text]</li>\n</ul>\n" class="textarea span12" rows="12" id="description" placeholder="Enter text ...">', 'Elizabeth ', '4', 'IMG_20130209_213131.jpg', NULL);

-- --------------------------------------------------------


--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `profile_id`, `name`, `description`, `category_id`, `published`, `image_url`, `slug`, `published_on`) VALUES
(1, 2, 'David es gay', '<header class="article-header" style="color: rgb(85, 85, 85); font-size: 13px; line-height: 19px;">\n<div class="article-info" style="max-width: 625px; margin: 28px 28px 28px auto; -webkit-transition: margin 0.2s; width: auto;"><a class="byline" href="http://mashable.com/people/anniecolbert/" style="color: rgb(0, 0, 0); font-weight: bold; line-height: normal; font-family: Arial, sans-serif; font-size: 1.3rem; display: inline;">Annie Colbert</a><time class="date" datetime="Mon, 25 Feb 2013 22:50:49 +0000" style="font-size: 1.3rem; color: rgb(138, 140, 142); display: inline;">3 hours ago</time></div>\n</header>\n\n<section class="article-content" style="font-size: 1.5rem; max-width: 625px; margin-left: auto; margin-right: 28px; -webkit-transition: margin 0.2s; margin-top: 1em; width: auto; color: rgb(85, 85, 85);">\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">As the classic song so accurately goes, the Internet is&nbsp;<a href="http://www.youtube.com/watch?v=zi8VTeDHjcM" style="color: rgb(12, 116, 166);" target="_blank">made of cats</a>.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">This obsession with meow-mixing everything on the web extends to&nbsp;<a href="http://mashable.com/category/tumblr/" style="color: rgb(12, 116, 166);">Tumblr</a>, where clever folks have created entire pages dedicated to famous people&#39;s&nbsp;<a href="http://mashable.com/category/cats/" style="color: rgb(12, 116, 166);">feline</a>&nbsp;doppelg&auml;ngers. Ron Swanson, David Bowie and Skrillex all star in Tumblr pages with their kitty twins. When was the last time you saw those three names in a sentence together? Send a thank you note to the Internet.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">&nbsp;</p>\n\n<div class="see-also"><strong>SEE ALSO:&nbsp;<a data-crackerjax="#post-slider" href="http://mashable.com/2012/07/02/best-cat-memes-ever/" style="color: rgb(12, 116, 166);">15 Best Cat Memes Ever [MEOW]</a></strong></div>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">&nbsp;</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;">Prance through the gallery above for nine of the finest &quot;Cats That Look Like ...&quot; Tumblr pages.</p>\n\n<p style="margin-top: 0.75em; margin-bottom: 1.5em;"><em>Homepage image courtesy of Flickr,&nbsp;<a href="http://www.flickr.com/photos/meaganjean/" style="color: rgb(12, 116, 166);" target="_blank">meaganjean</a></em></p>\n</section>\n\n<footer class="article-topics" style="max-width: 625px; margin-left: auto; margin-right: 28px; -webkit-transition: margin 0.2s; font-size: 1.1rem; text-transform: uppercase; font-weight: bold; width: auto; color: rgb(85, 85, 85);">TOPICS:&nbsp;<a href="http://mashable.com/category/cats/" style="color: rgb(105, 160, 59); font-weight: normal;">CATS</a>,&nbsp;<a href="http://mashable.com/category/cute-animals/" style="color: rgb(105, 160, 59); font-weight: normal;">CUTE ANIMALS</a>,&nbsp;<a href="http://mashable.com/category/gallery/" style="color: rgb(105, 160, 59); font-weight: normal;">GALLERY</a>,&nbsp;<a href="http://mashable.com/category/pics/" style="color: rgb(105, 160, 59); font-weight: normal;">PICS</a>,&nbsp;<a href="http://mashable.com/category/tumblr/" style="color: rgb(105, 160, 59); font-weight: normal;">TUMBLR</a>,&nbsp;<a href="http://mashable.com/category/watercooler/" style="color: rgb(105, 160, 59); font-weight: normal;">WATERCOOLER</a></footer>\n', 1, 1, 'cat-that-looks-like-drake.jpg', 'david-es-gay', '2013-02-26 00:48:51'),
(2, 2, 'Prueba con tags', '<div style="color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; font-size: medium; line-height: normal; width: 684px; margin: auto;">\n<h2 style="margin-top: 0px; margin-bottom: 0px; margin-left: 85px;">&iquest; Qu&eacute; es un tag?</h2>\n\n<p style="margin-left: 85px;">Una introducci&oacute;n breve a los tags - o etiquetas - que conforman el lenguaje HTML.</p>\n</div>\n\n<div id="article_text" style="clear: left; width: 684px; margin: 1em auto; color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; line-height: normal;">\n<div id="article_tools" style="margin-left: 1.5em; margin-bottom: 1em; width: 171px; float: right; background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-btm.gif); background-position: 0px 100%; background-repeat: no-repeat no-repeat;">\n<div class="hdr" style="background-color: rgb(137, 160, 44); background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-top.gif); background-repeat: no-repeat no-repeat;">\n<h5 style="margin: 0px 10px; font-weight: normal; color: rgb(255, 255, 255); padding-top: 2px;">herramientas<br />\ndel art&iacute;culo</h5>\n</div>\n\n<div class="bdy" style="margin-bottom: 5px; background-color: rgb(236, 236, 236); background-image: url(http://web.uservers.net/ayuda/__web-partes/fondo-arttools-mid.gif); background-repeat: no-repeat no-repeat;">\n<ul style="margin-bottom: 0px; margin-left: 10px; padding-top: 25px; padding-bottom: 0.5em; list-style: none;">\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://web.uservers.net/ayuda/soluciones/paginas-y-html/-que-es-un-tag_Mjg.html?print" style="color: rgb(72, 72, 72);">versi&oacute;n para imprimir</a></li>\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://usvx.net/doc/SPKB-028" style="color: rgb(72, 72, 72);">URL corto al art&iacute;culo</a></li>\n</ul>\n</div>\n</div>\n\n<h2 class="udoc_title" style="font-size: 17px;">&nbsp;</h2>\n\n<p>HTML es un lenguaje para escribir p&aacute;ginas de hipertexto definido por tags.</p>\n\n<p>Los tags, a veces llamados &quot;etiquetas&quot; en espa&ntilde;ol, son los &quot;comandos&quot; que los programas navegadores leen e interpretan para armar y dar forma a las p&aacute;ginas de Internet.</p>\n\n<p>Todos los tags de HTML empiezan con el caracter &lt; y terminan con el caracter &gt;. Dentro de ellos va toda la informaci&oacute;n del tag, empezando por el nombre. Por ejemplo, el tag HTML (que probablemente sea el m&aacute;s b&aacute;sico de todos) se escribe de la siguiente manera:</p>\n<code class="udoc_programlisting">&lt;html&gt;</code>\n\n<p>Adem&aacute;s del nombre, dentro de &lt; y &gt; el tag puede recibir o requerir ciertos argumentos. Los argumentos brindan informaci&oacute;n acerca de la instancia del tag. Estos generalmente se escriben de la forma:</p>\n<code class="udoc_programlisting">&lt;nombreTag argumento1=&quot;valor1&quot; argumento2=&quot;valor2&quot; ...&gt;</code>\n\n<p>Por ejemplo, el tag&nbsp;<code class="udoc_programlisting">&lt;FONT&gt;</code>, que controla la tipograf&iacute;a o tipo de letra de las p&aacute;ginas, lleva los siguientes argumentos:</p>\n<code class="udoc_programlisting">&lt;FONT FACE=&quot;Arial&quot; SIZE=&quot;2&quot; COLOR=&quot;RED&quot;&gt;</code>\n\n<p>No hay necesidad de decirlo, pero FACE determin</p>\n</div>\n', 1, 1, 'Adding_Tags.png', 'prueba-con-tags', '2013-02-26 00:00:26'),
(3, 2, 'Prueba con tags', '<div style="color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; font-size: medium; line-height: normal; width: 684px; margin: auto;">\n<h2 style="margin-top: 0px; margin-bottom: 0px; margin-left: 85px;">&iquest; Qu&eacute; es un tag?</h2>\n\n<p style="margin-left: 85px;">Una introducci&oacute;n breve a los tags - o etiquetas - que conforman el lenguaje HTML.</p>\n</div>\n\n<div id="article_text" style="clear: left; width: 684px; margin: 1em auto; color: rgb(0, 0, 0); font-family: ''trebuchet ms'', verdana, helvetica, arial; line-height: normal;">\n<div id="article_tools" style="margin-left: 1.5em; margin-bottom: 1em; width: 171px; float: right; background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-btm.gif); background-position: 0px 100%; background-repeat: no-repeat no-repeat;">\n<div class="hdr" style="background-color: rgb(137, 160, 44); background-image: url(http://web.uservers.net/ayuda/__web-partes/cap-arttools-top.gif); background-repeat: no-repeat no-repeat;">\n<h5 style="margin: 0px 10px; font-weight: normal; color: rgb(255, 255, 255); padding-top: 2px;">herramientas<br />\ndel art&iacute;culo</h5>\n</div>\n\n<div class="bdy" style="margin-bottom: 5px; background-color: rgb(236, 236, 236); background-image: url(http://web.uservers.net/ayuda/__web-partes/fondo-arttools-mid.gif); background-repeat: no-repeat no-repeat;">\n<ul style="margin-bottom: 0px; margin-left: 10px; padding-top: 25px; padding-bottom: 0.5em; list-style: none;">\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://web.uservers.net/ayuda/soluciones/paginas-y-html/-que-es-un-tag_Mjg.html?print" style="color: rgb(72, 72, 72);">versi&oacute;n para imprimir</a></li>\n	<li style="color: rgb(72, 72, 72); margin-bottom: 0.5em;"><a href="http://usvx.net/doc/SPKB-028" style="color: rgb(72, 72, 72);">URL corto al art&iacute;culo</a></li>\n</ul>\n</div>\n</div>\n\n<h2 class="udoc_title" style="font-size: 17px;">&nbsp;</h2>\n\n<p>HTML es un lenguaje para escribir p&aacute;ginas de hipertexto definido por tags.</p>\n\n<p>Los tags, a veces llamados &quot;etiquetas&quot; en espa&ntilde;ol, son los &quot;comandos&quot; que los programas navegadores leen e interpretan para armar y dar forma a las p&aacute;ginas de Internet.</p>\n\n<p>Todos los tags de HTML empiezan con el caracter &lt; y terminan con el caracter &gt;. Dentro de ellos va toda la informaci&oacute;n del tag, empezando por el nombre. Por ejemplo, el tag HTML (que probablemente sea el m&aacute;s b&aacute;sico de todos) se escribe de la siguiente manera:</p>\n<code class="udoc_programlisting">&lt;html&gt;</code>\n\n<p>Adem&aacute;s del nombre, dentro de &lt; y &gt; el tag puede recibir o requerir ciertos argumentos. Los argumentos brindan informaci&oacute;n acerca de la instancia del tag. Estos generalmente se escriben de la forma:</p>\n<code class="udoc_programlisting">&lt;nombreTag argumento1=&quot;valor1&quot; argumento2=&quot;valor2&quot; ...&gt;</code>\n\n<p>Por ejemplo, el tag&nbsp;<code class="udoc_programlisting">&lt;FONT&gt;</code>, que controla la tipograf&iacute;a o tipo de letra de las p&aacute;ginas, lleva los siguientes argumentos:</p>\n<code class="udoc_programlisting">&lt;FONT FACE=&quot;Arial&quot; SIZE=&quot;2&quot; COLOR=&quot;RED&quot;&gt;</code>\n\n<p>No hay necesidad de decirlo, pero FACE determin</p>\n</div>\n', 1, 1, 'Adding_Tags1.png', 'prueba-con-tags', '2013-02-26 00:00:26');

-- --------------------------------------------------------

--
-- Dumping data for table `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(1, 1, 6),
(2, 3, 2),
(3, 3, 5),
(4, 3, 6),
(5, 3, 7),
(6, 3, 8),
(7, 3, 9);

-- --------------------------------------------------------

