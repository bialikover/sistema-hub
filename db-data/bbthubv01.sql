SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `bbthub` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `bbthub` ;

-- -----------------------------------------------------
-- Table `bbthub`.`login_attempts`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`login_attempts` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ip_address` VARBINARY(16) NOT NULL ,
  `login` VARCHAR(100) NOT NULL ,
  `time` INT(11) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ip_address` (`ip_address` ASC) ,
  INDEX `login` (`login` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`migrations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`migrations` (
  `version` INT(3) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`role` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(20) NOT NULL ,
  `description` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`users` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ip_address` VARBINARY(16) NOT NULL ,
  `username` VARCHAR(100) NOT NULL ,
  `password` VARCHAR(40) NOT NULL ,
  `salt` VARCHAR(40) NULL DEFAULT NULL ,
  `email` VARCHAR(100) NOT NULL ,
  `activation_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_code` VARCHAR(40) NULL DEFAULT NULL ,
  `forgotten_password_time` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `remember_code` VARCHAR(40) NULL DEFAULT NULL ,
  `created_on` INT(11) UNSIGNED NOT NULL ,
  `last_login` INT(11) UNSIGNED NULL DEFAULT NULL ,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL ,
  `first_name` VARCHAR(50) NULL DEFAULT NULL ,
  `last_name` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`users_roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`users_roles` (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_users_roles_role1_idx` (`rid` ASC) ,
  INDEX `fk_users_roles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_users_roles_role1`
    FOREIGN KEY (`rid` )
    REFERENCES `bbthub`.`role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_roles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bbthub`.`types`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`types` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(140) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`events`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`events` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  `date_start` INT NOT NULL ,
  `date_end` INT NOT NULL ,
  `type_id` INT NOT NULL ,
  `description` TEXT NOT NULL ,
  `coach` VARCHAR(100) NOT NULL ,
  `workspace` VARCHAR(45) NULL ,
  `image_url` VARCHAR(500) NULL ,
  `slug` VARCHAR(60) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_events_types1_idx` (`type_id` ASC) ,
  CONSTRAINT `fk_events_types1`
    FOREIGN KEY (`type_id` )
    REFERENCES `bbthub`.`types` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`profiles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`profiles` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `uid` MEDIUMINT(8) UNSIGNED NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `midname` VARCHAR(45) NULL ,
  `lastname` VARCHAR(45) NULL ,
  `birthday` INT NULL ,
  `gender` VARCHAR(45) NULL ,
  `pictures` VARCHAR(45) NULL ,
  `likes` TEXT NULL ,
  `education` VARCHAR(45) NULL ,
  `work` VARCHAR(45) NULL ,
  `telephone` VARCHAR(45) NULL ,
  `address` VARCHAR(45) NULL ,
  `city` VARCHAR(45) NULL ,
  `country` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_profiles_users1_idx` (`uid` ASC) ,
  CONSTRAINT `fk_profiles_users1`
    FOREIGN KEY (`uid` )
    REFERENCES `bbthub`.`users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`rsvps`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`rsvps` (
  `id` VARCHAR(45) NOT NULL ,
  `event_id` INT UNSIGNED NOT NULL ,
  `profile_id` INT NOT NULL ,
  `status` TINYINT(1) NULL ,
  `attended` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_rsvps_events1_idx` (`event_id` ASC) ,
  INDEX `fk_rsvps_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_rsvps_events1`
    FOREIGN KEY (`event_id` )
    REFERENCES `bbthub`.`events` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rsvps_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`categories`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`categories` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `description` VARCHAR(140) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`entries`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`entries` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `profile_id` INT NOT NULL ,
  `name` VARCHAR(100) NOT NULL ,
  `description` TEXT NOT NULL ,
  `category_id` INT NOT NULL ,
  `published` TINYINT(1) NOT NULL DEFAULT 0 ,
  `image_url` VARCHAR(500) NULL ,
  `slug` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_categories1_idx` (`category_id` ASC) ,
  INDEX `fk_entries_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_entries_categories1`
    FOREIGN KEY (`category_id` )
    REFERENCES `bbthub`.`categories` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`tags` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`entries_tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`entries_tags` (
  `id` VARCHAR(45) NOT NULL ,
  `entries_id` INT UNSIGNED NOT NULL ,
  `tags_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_entries_tags_entries1_idx` (`entries_id` ASC) ,
  INDEX `fk_entries_tags_tags1_idx` (`tags_id` ASC) ,
  CONSTRAINT `fk_entries_tags_entries1`
    FOREIGN KEY (`entries_id` )
    REFERENCES `bbthub`.`entries` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entries_tags_tags1`
    FOREIGN KEY (`tags_id` )
    REFERENCES `bbthub`.`tags` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bbthub`.`banners`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bbthub`.`banners` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `profile_id` INT NOT NULL ,
  `name` VARCHAR(100) NULL ,
  `image_url` VARCHAR(500) NOT NULL ,
  `link` VARCHAR(500) NULL ,
  `active` TINYINT(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_banners_profiles1_idx` (`profile_id` ASC) ,
  CONSTRAINT `fk_banners_profiles1`
    FOREIGN KEY (`profile_id` )
    REFERENCES `bbthub`.`profiles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `bbthub` ;

-- -----------------------------------------------------
-- Placeholder table for view `bbthub`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bbthub`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `bbthub`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bbthub`.`view1`;
USE `bbthub`;
;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
