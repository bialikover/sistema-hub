<div class="fixed">
<div id="infoMessage"><?php echo $message;?></div>
<div class="row-fluid">
  <div class="span6 offset3">
    <div class="auth-box">
    <h1>Cambia tu contraseña</h1>  
<?php echo form_open('auth/reset_password/' . $code);?>
    <?php echo form_input($new_password);?>
    <?php echo form_input($new_password_confirm);?>    
    <?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>
  <div id="actions">
    <div id="submit-forget" class="span6">
      <?php echo form_submit('submit', 'Cambiar');?>
    </div>
  </div>        
<?php echo form_close();?>

  
  </div>
</div>
</div>
</div>

<script>
$('#new').tooltip({'trigger':'focus', 'title': 'Minimo 8 caracteres'});
</script>