<div class="fixed">
<div id="infoMessage"><?php echo $message;?></div>
<div class="row-fluid">
  <div class="span6 offset3">
    <div class="auth-box">
    <h1>Inicia sesión</h1>  


<?php echo form_open("auth/login");?>
    <input type="text" name="identity" id ="identity"placeholder="Correo electrónico"/>
    <input type="password" name="password" placeholder="Contraseña"/>
    

  <div id="actions">
    <div id="remember" class="span6">
      <p>
      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
      <label for="remember">Mantenerme conectado:</label>
      </p>
    </div>
    <div id="submit" class="span6">
      <?php echo form_submit('submit', 'Entrar');?>
    </div>
  </div>
  <div class="facebook-login">    
    <a href= "<?php echo base_url('index.php/api/session/facebook'); ?>"><p>Regístrate con Facebook</p></a>    
  </div>
  <div class="actionlinks">
    <a href= "<?php echo base_url('index.php/auth/signup'); ?>">Crear una cuenta</a>
    <a href="forgot_password">¿Olvidaste tu contraseña?</a>    
  </div>
        
<?php echo form_close();?>

  
  </div>
</div>
</div>
</div>
<script>
$('#identity').tooltip({'trigger':'focus', 'title': 'tu email con el que te registraste, o si fue con fb el mail de fb'});
</script>