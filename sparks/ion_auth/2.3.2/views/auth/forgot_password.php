<div class="fixed">
<div id="infoMessage"><?php echo $message;?></div>
<div class="row-fluid">
  <div class="span6 offset3">
    <div class="auth-box">
    <h1>¿Olvidaste tu contraseña?</h1>  

<p> Porfavor ingresa tu correo electrónico para que te enviemos un correo y recuperes tu contraseña</p>
<?php echo form_open("auth/forgot_password");?>    
    <input type="text" name="email" id="email" placeholder="Correo electrónico"/>    
  <div id="actions">
    <div id="submit-forget" class="span6">
      <?php echo form_submit('submit', 'Recuperar');?>
    </div>
  </div>        
<?php echo form_close();?>

  
  </div>
</div>
</div>
</div>

<script>
$('#email').tooltip({'trigger':'focus', 'title': 'tu email con el que te registraste, o si fue con fb el mail de fb'});
</script>